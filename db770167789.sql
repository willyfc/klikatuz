-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: db770167789.hosting-data.io
-- Tiempo de generación: 05-04-2019 a las 13:37:39
-- Versión del servidor: 5.5.60-0+deb7u1-log
-- Versión de PHP: 7.0.33-0+deb9u3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db770167789`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_additional_excercise`
--

CREATE TABLE `tbl_additional_excercise` (
  `additional_excercise_id` int(11) NOT NULL,
  `additional_excercise_name` text NOT NULL,
  `position_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_additional_excercise_detail`
--

CREATE TABLE `tbl_additional_excercise_detail` (
  `additional_excercise_detail_id` int(11) NOT NULL,
  `additional_excercise_content` text NOT NULL,
  `additional_excercise_time` varchar(20) NOT NULL,
  `additional_excercise_max_time` varchar(20) NOT NULL,
  `additional_excercise_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_additional_excercise_group`
--

CREATE TABLE `tbl_additional_excercise_group` (
  `aditional_excercise_group_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `aditional_excercise_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_additional_excercise_group_student`
--

CREATE TABLE `tbl_additional_excercise_group_student` (
  `aditional_excercise_group` int(11) NOT NULL,
  `aditional_excercise_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_center`
--

CREATE TABLE `tbl_center` (
  `center_id` int(11) NOT NULL,
  `center_name` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `manager_id` int(11) NOT NULL COMMENT 'Viene desde la tabla usuario y se obtiene con la sesión'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_center`
--

INSERT INTO `tbl_center` (`center_id`, `center_name`, `manager_id`) VALUES
(1, 'La Salle', 7),
(2, 'Centro 2', 8),
(3, 'Centro 3', 6),
(4, 'Centro 4', 9),
(5, 'Leka Enea Ikastola', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_center_professor`
--

CREATE TABLE `tbl_center_professor` (
  `user_id` int(11) NOT NULL,
  `center_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_center_professor`
--

INSERT INTO `tbl_center_professor` (`user_id`, `center_id`) VALUES
(10, 1),
(11, 1),
(10, 2),
(10, 3),
(12, 3),
(12, 4),
(13, 1),
(12, 1),
(14, 1),
(14, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_center_student`
--

CREATE TABLE `tbl_center_student` (
  `user_id` int(11) NOT NULL,
  `center_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_center_student`
--

INSERT INTO `tbl_center_student` (`user_id`, `center_id`) VALUES
(15, 1),
(16, 1),
(19, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_dictation`
--

CREATE TABLE `tbl_dictation` (
  `dictation_id` int(11) NOT NULL,
  `dictation_name` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  `position_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_dictation_sub`
--

CREATE TABLE `tbl_dictation_sub` (
  `dictation_sub_id` int(11) NOT NULL,
  `dictation_sub_name` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `dictation_sub_content` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `dictation_sub_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `dictation_sub_max_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `position_order` int(11) NOT NULL,
  `dictation_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_excercise`
--

CREATE TABLE `tbl_excercise` (
  `excercise_id` int(11) NOT NULL,
  `excercise_name` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `position_order` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_excercise`
--

INSERT INTO `tbl_excercise` (`excercise_id`, `excercise_name`, `lesson_id`, `position_order`) VALUES
(1, 'JF', 3, 1),
(2, 'JF', 6, 2),
(3, 'JFKD - motza', 8, 3),
(4, 'KD', 7, 4),
(5, 'JFKD nahastuta hirunaka', 9, 5),
(6, 'Ejercicio_prueba', 7, 6),
(7, 'Ejercicio_prueba2', 9, 7),
(8, 'JFKD - corto', 11, 8),
(9, 'KD', 10, 9),
(10, 'JFKD mezclado a cuatro', 12, 10),
(11, 'JF nahastuta hirunaka', 6, 0),
(12, 'JF nahastuta launaka', 6, 0),
(13, 'JF mezclado series de tres', 3, 0),
(14, 'jffj', 1, 0),
(15, '1º ejer', 1, 0),
(16, 'jf', 7, 0),
(17, 'KD nahastuta hirunaka', 7, 0),
(18, 'KD nahastuta launaka', 7, 0),
(19, 'JFKD nahastuta launaka', 9, 0),
(20, 'LS', 20, 0),
(21, 'LS hirunaka nahastuta', 20, 0),
(22, 'LS launaka nahastuta', 20, 0),
(23, 'SDF-LKJ', 20, 0),
(24, 'SDFJKL hirunaka nahastuta', 20, 0),
(25, 'SDFJKL launaka nahastuta	', 20, 0),
(26, 'ÑA', 21, 0),
(27, 'ÑA  hirunaka nahastuta', 21, 0),
(28, 'ÑA launaka nahastuta', 21, 0),
(29, 'Hasierakoak hirunaka nahastuta', 23, 0),
(30, 'Hasierakoak launaka nahastuta', 23, 0),
(31, 'Hasierako teklekin hitzak', 23, 0),
(32, 'GH ', 26, 0),
(33, 'GH hirunaka nahastuta', 26, 0),
(34, 'GH launaka nahastuta', 26, 0),
(35, 'Erdiko lerrokoak hirunaka', 26, 0),
(36, 'Erdiko lerrokoak launaka', 26, 0),
(37, 'Erdiko lerrokoekin hitzak', 26, 0),
(38, 'JFKD - luzea', 8, 0),
(39, 'LSÑA - motza', 22, 0),
(40, 'LSÑA - luzea', 22, 0),
(41, 'LSÑA hirunaka nahastuta', 22, 0),
(42, 'LSÑA launaka nahastuta', 22, 0),
(43, 'JHJ - motza', 24, 0),
(44, 'JHJ - luzea', 24, 0),
(45, 'Hasierakoak + H - hirunaka nahastuta', 24, 0),
(46, 'Hasierakoak + H - launaka nahastuta', 24, 0),
(47, 'FGF - motza', 25, 0),
(48, 'FGF - luzea', 25, 0),
(49, 'Hasierakoak + G - hirunaka nahastuta', 25, 0),
(50, 'Hasierakoak + G - launaka nahastuta', 25, 0),
(51, 'bera', 1, 0),
(52, 'posición base palabras', 41, 0),
(53, 'KIK motza', 27, 0),
(54, 'KIK luzea', 27, 0),
(55, 'Erdikoak + i hirunaka nahastuta', 27, 0),
(56, 'Erdikoak + i launaka nahastuta', 27, 0),
(57, 'Erdikoak + i-rekin hitzak', 27, 0),
(58, 'DED Motza', 28, 0),
(59, 'DED Luzea', 28, 0),
(60, 'Erdikoak + e hirunaka nahastuta', 28, 0),
(61, 'Erdikoak + e launaka nahastuta', 28, 0),
(62, 'Erdikoak + e-rekin hitzak', 28, 0),
(63, 'fila posición base', 41, 0),
(64, 'oinarrizko posizioa', 2, 0),
(65, 'Erdikoak + e + i hirunaka nahastuta', 29, 0),
(66, 'Erdikoak + e + i launaka nahastuta', 29, 0),
(67, 'Erdikoak + e + i -kin hitzak', 29, 0),
(68, 'Erdikoak + e + i -kin esaldiak', 29, 0),
(69, 'JUJ - luzea', 30, 0),
(70, 'U-rekin hitzak', 30, 0),
(71, 'U-rekin esaldiak', 30, 0),
(72, 'FRF Luzea', 31, 0),
(73, 'R-rekin hitzak', 31, 0),
(74, 'R-rekin esaldiak', 31, 0),
(75, 'Hitzak U eta R-rekin', 32, 0),
(76, 'Esaldiak U eta R-rekin', 32, 0),
(77, 'LOL', 33, 0),
(78, 'SWS', 34, 0),
(79, 'LOSW launaka', 35, 0),
(80, 'L eta O launaka nahastuta', 33, 0),
(81, 'O-rekin hitzak', 33, 0),
(82, 'O-rekin esaldiak', 33, 0),
(83, 's eta w hirunaka', 34, 0),
(84, 's eta w launaka', 34, 0),
(85, 'w-rekin hitzak', 34, 0),
(86, 'LOSW hirunaka', 35, 0),
(87, 'Hitzak O eta W-rekin', 35, 0),
(88, 'Esaldiak O eta W-rekin', 35, 0),
(89, 'ÑPÑ', 36, 0),
(90, 'Ñ eta P launaka', 36, 0),
(91, 'P-rekin hitzak', 36, 0),
(92, 'P-rekin esaldiak', 36, 0),
(93, 'AQA', 37, 0),
(94, 'A eta Q launaka', 37, 0),
(95, 'Q-rekin hitzak', 37, 0),
(96, 'Q-rekin esaldiak', 37, 0),
(97, 'Hitzak P eta Q-rekin', 38, 0),
(98, 'P eta Q-rekin esaldiak', 38, 0),
(99, 'A, P, Ñ eta Q hirunaka', 38, 0),
(100, 'A, P, Ñ eta Q launaka', 38, 0),
(101, 'JYJ', 45, 0),
(102, 'J eta Y launaka', 45, 0),
(103, 'Y-rekin hitzak', 45, 0),
(104, 'Y-rekin esaldiak', 45, 0),
(105, 'FTF', 46, 0),
(106, 'F eta T launaka', 46, 0),
(107, 'T-rekin hitzak', 46, 0),
(108, 'T-rekin esaldiak', 46, 0),
(109, 'J, Y, F eta T launaka', 47, 0),
(110, 'T eta Y-rekin hitzak', 47, 0),
(111, 'Y eta T-rekin esaldiak', 47, 0),
(112, 'AZA', 48, 0),
(113, 'A eta Z launaka', 48, 0),
(114, 'Z-rekin hitzak', 48, 0),
(115, 'Z-rekin esaldiak', 48, 0),
(116, 'SXS', 49, 0),
(117, 'S eta X launaka', 49, 0),
(118, 'X-ekin hitzak', 49, 0),
(119, 'X-ekin esaldiak', 49, 0),
(120, 'Z eta X launaka', 50, 0),
(121, 'Hitzak Z eta X-rekin', 50, 0),
(122, 'Esaldiak Z eta X-rekin ', 50, 0),
(123, 'DCD', 51, 0),
(124, 'D eta C launaka nahastuta', 51, 0),
(125, 'Hitzak C-rekin', 51, 0),
(126, 'Esaldiak C-rekin', 51, 0),
(127, 'FVF', 52, 0),
(128, 'F eta V launaka', 52, 0),
(129, 'V-rekin hitzak', 52, 0),
(130, 'V-rekin esaldiak', 52, 0),
(131, 'FBF', 53, 0),
(132, 'F eta B launaka nahastuta', 53, 0),
(133, 'B-rekin hitzak', 53, 0),
(134, 'B-rekin esaldiak', 53, 0),
(135, 'D, F, C, V eta B launaka', 54, 0),
(136, 'Hitzak C, V eta B-rekin', 54, 0),
(137, 'Esaldiak C, V eta B-rekin', 54, 0),
(138, 'JMJ', 77, 0),
(139, 'J eta M launaka nahastuta', 77, 0),
(140, 'M-rekin hitzak', 77, 0),
(141, 'M-rekin esaldiak', 77, 0),
(142, 'JNJ', 78, 0),
(143, 'J eta N launaka', 78, 0),
(144, 'N-rekin hitzak', 78, 0),
(145, 'Esaldiak N-rekin', 78, 0),
(146, 'J, M eta N launaka nahastuta', 79, 0),
(147, 'M eta N -rekin hitzak', 79, 0),
(148, 'M eta N-rekin esaldiak', 79, 0),
(149, 'Negua Joan da ta - 1', 88, 0),
(150, 'Negua Joan da ta - 2', 88, 0),
(151, 'Negua Joan da ta - 3', 88, 0),
(152, 'Negua Joan da ta - 4', 88, 0),
(153, 'Ezker Maiuskula ', 84, 0),
(154, 'Ezker Maiuskula - Izenak', 84, 0),
(155, 'Eskuin Maiuskula', 83, 0),
(156, 'Eskuin Maiuskula - Izenak', 83, 0),
(157, 'Aizkorri', 89, 0),
(158, 'Futbol partida', 84, 0),
(159, 'Koma lantzeko k,k', 85, 0),
(160, 'Zenbakiak komarekin', 85, 0),
(161, 'Puntu eta koma lantzeko', 85, 0),
(162, 'Hilabeteak', 85, 0),
(163, 'Astoa ikusi nuen - 1', 88, 0),
(164, 'Astoa ikusi nuen - 2', 88, 0),
(165, 'Astoa ikusi nuen - 3', 88, 0),
(166, 'Astoa ikusi nuen - 4', 88, 0),
(167, 'Astoa ikusi nuen - 5', 88, 0),
(168, 'Astoa ikusi nuen - 6', 88, 0),
(169, 'Puntua lantzeko', 86, 0),
(170, 'Herriak', 86, 0),
(171, 'Bi puntuak lantzeko', 86, 0),
(172, 'Antonimoak', 86, 0),
(173, 'Erosketa zerrenda', 86, 0),
(174, 'Animaliak', 86, 0),
(175, 'Gidoia lantzeko', 87, 0),
(176, 'Azpiko gidoia lantzeko', 87, 0),
(177, 'Hitz konposatuak', 87, 0),
(178, 'Haizea', 89, 0),
(179, 'Timber', 89, 0),
(180, 'Lagrimas desordenadas', 87, 0),
(181, 'Hey Brother', 89, 0),
(182, 'Maritxu nora zoaz', 87, 0),
(183, 'Waka-Waka', 89, 0),
(184, 'Palabras con acento', 88, 0),
(185, 'Palabras con diéresis', 88, 0),
(186, 'El pingüino', 89, 0),
(187, 'El cumpleaños de mi tía', 88, 0),
(188, 'Mis vacaciones', 88, 0),
(189, 'El cole', 88, 0),
(190, 'Zenbakiak 1-5', 89, 0),
(191, 'Ikurrak', 89, 0),
(192, 'Euskaraz', 4, 0),
(193, 'Erderaz', 4, 0),
(194, 'froga!!', 1, 0),
(195, 'FINAL!!!!!', 91, 0),
(196, 'JYJ', 39, 0),
(197, 'HAY', 40, 0),
(198, 'FTF', 41, 0),
(199, 'FUTURA', 41, 0),
(200, 'KIK', 42, 0),
(201, 'DED', 43, 0),
(202, 'HIJA', 55, 0),
(203, 'GAJE', 44, 0),
(204, 'LOL', 56, 0),
(205, 'HOLA', 56, 0),
(206, 'sws largo', 56, 0),
(207, 'FNF FVF', 56, 0),
(208, 'MALVA', 56, 0),
(209, 'Primeras + H a cuatro', 17, 0),
(210, 'JUNTA', 56, 0),
(211, 'FBF', 56, 0),
(212, 'FABRIL', 56, 0),
(213, 'NUBE', 57, 0),
(214, 'DCD', 57, 0),
(215, 'CIUDAD', 57, 0),
(216, 'JACA', 57, 0),
(217, 'KYK', 57, 0),
(218, 'CURA', 57, 0),
(219, 'JHJ', 13, 0),
(220, 'LSÑA - largo', 15, 0),
(221, 'Primera linea mezcladas a tres', 16, 0),
(222, 'Primeras + H a tres', 17, 0),
(223, 'Primeras + G a cuatro', 18, 0),
(224, 'Línea Central a tres', 19, 0),
(225, 'LUGAR', 39, 0),
(226, 'BERROBI', 0, 0),
(227, 'KD mezclado-series de tres', 10, 0),
(228, 'KD variado (una y dos letras mezclado)', 10, 0),
(229, 'ls', 13, 0),
(230, 'ls', 13, 0),
(231, 'ÑA mezclado a tres', 14, 0),
(232, 'EJERCICIO7', 15, 0),
(233, 'Palabras con la primera linea', 16, 0),
(234, 'Todas mezcladas', 16, 0),
(235, 'EJERCICIO 9', 17, 0),
(236, 'Primeras + G a tres', 18, 0),
(237, 'ñpñ-aqa', 60, 0),
(238, 'jf series de tres ', 3, 0),
(239, 'JF mezclado series de cuatro', 3, 0),
(240, 'lección 2.3', 10, 0),
(241, 'KD mezclado- series de cuatro', 10, 0),
(242, 'errepasoa: jfkd', 9, 0),
(243, 'Primera + H', 19, 0),
(244, 'EJERCICIO 12', 39, 0),
(245, 'Praktika ÑA nahastuta', 0, 0),
(246, 'Palabras con primera línea', 19, 0),
(247, 'EJERCICIO 13', 40, 0),
(248, 'EJERCICIO 15', 42, 0),
(249, 'PRACTICA 15', 42, 0),
(250, 'EJERCICIO 16', 43, 0),
(251, 'palabras con r', 43, 0),
(252, 'practicar la r', 43, 0),
(253, 'jjf kkd', 11, 0),
(254, 'ffj ddk', 11, 0),
(255, 'JFKD mezclado a tres', 12, 0),
(256, 'JFKD Mezcla 2', 12, 0),
(257, 'Ariketak: jfkdls nahastuta', 20, 0),
(258, 'ariketa: jfkdls', 20, 0),
(259, 'repaso', 41, 0),
(260, 'el cubo 2', 0, 0),
(261, 'palabras u+r', 44, 0),
(262, 'EJERCICIO 18', 55, 0),
(263, 'practicar o', 55, 0),
(264, 'EJERCICIO 19', 56, 0),
(265, 'Ariketak:jkl', 20, 0),
(266, 'Ariketa:fds', 20, 0),
(267, 'SDFJKL nahastuta', 20, 0),
(268, 'ls 1.2', 13, 0),
(269, 'LSL', 13, 0),
(270, 'jfkdlsña nahastuta', 24, 0),
(271, 'Hasierakoak nahastuta + gh', 25, 0),
(272, 'EJERCICIO 20', 57, 0),
(273, 'practicar ñp', 58, 0),
(274, 'practicar q', 59, 0),
(275, 'errepasoa jfkdlsña', 23, 0),
(276, 'jfkdls nahastuta', 20, 0),
(277, 'palabras p+q', 60, 0),
(278, 'palabras y', 61, 0),
(279, 'EJERCICIO25', 62, 0),
(280, 'palabras t II', 62, 0),
(281, 'virginia', 89, 0),
(282, 'Denak nahastuta', 23, 0),
(283, 'Xalbadorren heriotzean', 4, 0),
(284, 'Amaren bularra', 89, 0),
(285, 'Izarren hautsa', 89, 0),
(286, 'Intxaurra', 89, 0),
(287, 'Txano Gorritxo', 4, 0),
(288, 'palabras q', 59, 0),
(289, 'practicar y', 61, 0),
(290, 'palabras t', 62, 0),
(291, 'EJERCICIO26', 63, 0),
(292, 'Hasierakoak + h nahastuta', 24, 0),
(293, 'EJERCICIO27', 68, 0),
(294, 'frase z', 68, 0),
(295, 'hasierakoak + h', 24, 0),
(296, 'Hiru txerritxoak', 89, 0),
(297, 'Tomatea', 4, 0),
(298, 'Walt Disney', 4, 0),
(299, 'hasierakoak', 26, 0),
(300, 'palabras x', 69, 0),
(301, 'erdiko lerroa+ i hitzak', 27, 0),
(302, 'p', 0, 0),
(303, 'erdiko lerroa+i', 27, 0),
(304, 'EJERCICIO 29', 70, 0),
(305, 'EJERCICIO30', 71, 0),
(306, 'EJERCICIO31', 72, 0),
(307, 'EJERCICIO32', 76, 0),
(308, 'l', 0, 0),
(309, 'olimp', 28, 0),
(310, 'joko', 28, 0),
(311, 'Mekanografia', 89, 0),
(312, 'Donostia', 89, 0),
(313, 'EJERCICIO 33', 73, 0),
(314, 'EJERCICIO 34', 80, 0),
(315, 'EJERCICIO 35', 81, 0),
(316, 'Nikola Tesla', 89, 0),
(317, 'Kafka', 89, 0),
(318, 'Gernikako arbola', 89, 0),
(319, 'Stephen Hawking', 4, 0),
(320, 'EJERCICIO 36', 82, 0),
(321, 'EJERCICIO37', 90, 0),
(322, 'Lyrics to Hey Brother', 89, 0),
(323, 'Let It Go Lyrics', 89, 0),
(324, 'Muñagorriko eskola!!!', 0, 0),
(325, 'Muñagorriko eskola!!!', 0, 0),
(326, 'Muñagorriko eskola!!!', 79, 0),
(327, 'EURRE!!!', 79, 0),
(328, 'Thomas Alva Edison', 4, 0),
(329, 'Chandelier', 89, 0),
(330, 'Furra Furra', 89, 0),
(331, 'Maritxu Nora Zoaz', 87, 0),
(332, 'EJERCICIO38', 91, 0),
(333, 'errepasoa', 32, 0),
(334, 'Marxismoa', 89, 0),
(335, 'Leninismoa', 89, 0),
(336, 'Komunismoa', 89, 0),
(337, 'EJERCICIO 39', 92, 0),
(338, 'EJERCICIO40', 93, 0),
(339, 'EJERCICIO39++', 92, 0),
(340, 'Errepasoa', 32, 0),
(341, 'Happy', 89, 0),
(342, 'l.25V.1', 62, 0),
(343, 'l.25V.2', 62, 0),
(344, 'l.25V.3', 62, 0),
(345, 'l.25V.4', 62, 0),
(346, 'l.25V.5', 62, 0),
(347, 'l.25V.6', 62, 0),
(348, 'l.25V.7', 62, 0),
(349, 'l.25V.8', 62, 0),
(350, 'l.25V.9', 62, 0),
(351, 'l.25V.12', 62, 0),
(352, 'l.25V.11', 62, 0),
(353, 'l.25V.10', 62, 0),
(354, 'l.25TEXTO', 62, 0),
(355, 'hasierakoak errepasoa', 23, 0),
(356, '20.irak errepasoa', 35, 0),
(357, 'West End', 4, 0),
(358, 'East End', 89, 0),
(359, 'Taxiak', 4, 0),
(360, 'Zapatos de Tacón', 4, 0),
(361, 'l.26V.1', 63, 0),
(362, 'l.26V.2', 63, 0),
(363, 'l.26V.3', 63, 0),
(364, 'l.26V.4', 63, 0),
(365, 'l.26V.5', 63, 0),
(366, 'l.26V.6', 63, 0),
(367, 'l.26V.7', 63, 0),
(368, 'l.26V.8', 63, 0),
(369, 'l.26V.9', 63, 0),
(370, 'l.26V.10', 63, 0),
(371, 'l.26V.11', 63, 0),
(372, 'l.26V.12', 63, 0),
(373, 'l.26V.13', 63, 0),
(374, 'l.26TEXTO', 63, 0),
(375, 'errepaso 2', 33, 0),
(376, 'Mañana Maria Parrado', 89, 0),
(377, 'jfkdls', 22, 0),
(378, 'jfkdlsñagh', 26, 0),
(379, 'hasierako ilara+ie', 29, 0),
(380, 'Tour de Francia', 4, 0),
(381, 'Bizikleta', 89, 0),
(382, 'Steve Jobs', 4, 0),
(383, 'Sorginetxeko trikuharria', 4, 0),
(384, 'errepasoa', 36, 0),
(385, 'Kontrola', 89, 0),
(386, 'l.27.V1', 68, 0),
(387, 'l.27.V2', 68, 0),
(388, 'l.27.V3', 68, 0),
(389, 'l.27.V4', 68, 0),
(390, 'l.27.V5', 68, 0),
(391, 'l.27.V6', 68, 0),
(392, 'l.27.V7', 68, 0),
(393, 'l.27.V8', 68, 0),
(394, 'l.27.V9', 63, 0),
(395, 'l.27.V10', 68, 0),
(396, 'l.27.V9', 68, 0),
(397, 'l.27.V11', 68, 0),
(398, 'l.27.V12', 68, 0),
(399, 'l.27.V13', 68, 0),
(400, 'l.27TEXTO', 68, 0),
(401, 'ASMAKIZUNAK', 47, 0),
(402, 'l.28.V1', 69, 0),
(403, 'l.28.V2', 69, 0),
(404, 'l.28.V3', 69, 0),
(405, 'l.28.V4', 69, 0),
(406, 'l.28.V5', 69, 0),
(407, 'l.28.V6', 69, 0),
(408, 'l.28.V7', 69, 0),
(409, 'l.28.V8', 69, 0),
(410, 'l.28.V9', 69, 0),
(411, 'l.28.V10', 69, 0),
(412, 'l.TEXTO 28', 69, 0),
(413, 'Errepasoa', 47, 0),
(414, 'Zendaya  Coleman', 89, 0),
(415, 'g', 42, 0),
(416, 'Ejercicio.29.1', 70, 0),
(417, 'Ejercicio.29.2', 70, 0),
(418, 'Ejercicio.29.3', 70, 0),
(419, 'Ejercicio.29.4', 70, 0),
(420, 'Ejercicio.29.5', 70, 0),
(421, 'Ejercicio 29.6', 70, 0),
(422, 'Ejercicio 29.7', 70, 0),
(423, 'Ejercicio 29.8', 70, 0),
(424, 'Ejercicio 29.9', 70, 0),
(425, 'Ejercicio 29.10', 70, 0),
(426, 'Ejercicio 29.11', 70, 0),
(427, 'Ejercicio 29.12', 70, 0),
(428, 'Ejercicio 30.1', 71, 0),
(429, 'Ejercicio 30.2', 71, 0),
(430, 'Ejercicio 30.3', 71, 0),
(431, 'Ejercicio 30.4', 71, 0),
(432, 'Ejercicio 30.5', 71, 0),
(433, 'Ejercicio 30.6', 71, 0),
(434, 'Ejercicio 30.7', 71, 0),
(435, 'Ejercicio 30.8', 71, 0),
(436, 'Ejercicio 30.9', 71, 0),
(437, 'Ejercicio 30.10', 71, 0),
(438, 'Ejercicio 30.11', 71, 0),
(439, 'Ejercicio 30.12', 71, 0),
(440, 'Ejercicio 30.13', 71, 0),
(441, 'Ejercicio 30.14', 71, 0),
(442, 'Ejercicio 30.15', 71, 0),
(443, 'Harriak', 89, 0),
(444, 'Eragile geologiko edo geomorfikoak', 89, 0),
(445, 'Tolesturak eta failak', 89, 0),
(446, 'Sumendiak', 89, 0),
(447, 'Lurrikarak edo Seismoak', 89, 0),
(448, 'Itsasikarak eta Tsunamiak', 89, 0),
(449, 'Sismografoa', 89, 0),
(450, 'Ejercicio 30.16', 71, 0),
(451, 'Ejercicio 30.17', 71, 0),
(452, 'Ejericicio 30.18', 71, 0),
(453, 'Ejercicio 30.19', 71, 0),
(454, 'Ejercicio 30.20', 71, 0),
(455, 'Ejercicio 30.21', 71, 0),
(456, 'Ejercicio 30.22', 71, 0),
(457, 'Ejercicio 30.22', 71, 0),
(458, 'Ejercicio 30.24', 71, 0),
(459, 'Ejercicio 30.25', 71, 0),
(460, 'Ejercicio 30.26', 71, 0),
(461, 'Unibertsoa', 89, 0),
(462, 'Big-Bang-a ', 89, 0),
(463, 'Izarrak', 89, 0),
(464, 'Argi-Urtea', 89, 0),
(465, 'Ibaia', 89, 0),
(466, 'Glaziarrak', 89, 0),
(467, 'Lurpeko UraK', 89, 0),
(468, 'Ejercicio 30.27', 71, 0),
(469, 'Ejercicio30.28', 71, 0),
(470, 'Ejercicio 30.29', 71, 0),
(471, 'Ejercicio 30.30', 71, 0),
(472, 'Ejercicio 30.31', 71, 0),
(473, 'Ejercicio 30.32', 71, 0),
(474, 'Ejercicio 30.33', 71, 0),
(475, 'Ejercicio 30.34', 71, 0),
(476, 'Ejercicio 30.35', 71, 0),
(477, 'Ejercicio 30.36', 71, 0),
(478, 'Ejercicio 30.37', 71, 0),
(479, 'Ejercicio 30.38', 71, 0),
(480, 'Adele - Rolling in the deep', 89, 0),
(481, 'Ejercicio 30.39', 71, 0),
(482, 'Ejercicio.30.40', 71, 0),
(483, 'Ejercicio 30.41', 71, 0),
(484, 'Ejercicio 30.42', 71, 0),
(485, 'Azken arnasa', 89, 0),
(486, 'Hemendik At - Eskutitza', 89, 0),
(487, 'Date a Live - Argumento', 89, 0),
(488, 'Date a Live - Personajes Principales', 89, 0),
(489, 'Date a Live - Espíritus', 89, 0),
(490, 'Date A Live - Ratatoskr - AST -', 89, 0),
(491, 'Avión', 89, 0),
(492, 'Avión Historia', 89, 0),
(493, 'Ejercicio 30.43', 71, 0),
(494, 'Ejercicio 30.44', 71, 0),
(495, 'Ejercicio 30.45', 71, 0),
(496, 'Ejercicio 30.46', 71, 0),
(497, 'Ejercicio 30.47', 71, 0),
(498, 'Ejercicio 30.47', 71, 0),
(499, 'Ejercicio 30.48', 71, 0),
(500, 'Ejercicio 30.49', 71, 0),
(501, 'Ejercicio 30.50', 71, 0),
(502, 'Ejercicio 30.51', 71, 0),
(503, 'Ejercicio 30.52', 71, 0),
(504, 'Ejercicio 30.53', 71, 0),
(505, 'Ejercicio 30.54', 71, 0),
(506, 'Ejercicio 30.55', 71, 0),
(507, 'Ejercicio 30.56', 71, 0),
(508, 'Ejercicio 30.57', 71, 0),
(509, 'Ejercicio 30.58', 71, 0),
(510, 'Ejercicio 30.59', 71, 0),
(511, 'Ejercicio 30.60', 71, 0),
(512, 'Ejercicio 30.61', 71, 0),
(513, 'Ejercicio 30.62', 71, 0),
(514, 'Ejercicio 30.63', 71, 0),
(515, 'Ejercicio 30.64', 71, 0),
(516, 'GH a tres', 19, 0),
(517, 'Uncover - Lyric', 89, 0),
(518, 'Thriller Lyrics', 89, 0),
(519, 'Over The Rainbow - Lirycs', 89, 0),
(520, 'lasaña salada', 22, 0),
(521, 'Ejercicio 30.65', 71, 0),
(522, 'Ejercicio 30.66', 71, 0),
(523, 'Ejercicio 30.67', 71, 0),
(524, 'Ejercicio 30.68', 71, 0),
(525, 'Ejercicio 30.69', 71, 0),
(526, 'Ejercicio 30.70', 71, 0),
(527, 'Ejercicio 30.71', 71, 0),
(528, 'Ejercicio 30.72', 71, 0),
(529, 'Ejercicio 30.73', 71, 0),
(530, 'Ejercicio 30.74', 71, 0),
(531, 'Ejercicio 30.75', 71, 0),
(532, 'Ejercicio 30.76', 71, 0),
(533, 'Ejercicio 30.77', 71, 0),
(534, 'Ejercicio 30.78', 71, 0),
(535, 'Ejercicio 30.79', 71, 0),
(536, 'Prueba Noviembre 1', 96, 0),
(537, 'Prueba Noviembre 2', 96, 0),
(538, 'Fila base+I+E', 40, 0),
(539, 'Motivación', 69, 0),
(540, 'Ejercicio 30.80', 71, 0),
(541, 'Ejercicio 30.81', 71, 0),
(542, 'Ejercicio 30.82', 71, 0),
(543, 'Ejercicio 30.83', 71, 0),
(544, 'Ejercicio 30.84', 71, 0),
(545, 'Ejercicio 30.85', 71, 0),
(546, 'Ejercicio 30.86', 71, 0),
(547, 'Ejercicio 30.87', 71, 0),
(548, 'Ejercicio 30.88', 71, 0),
(549, 'Fila base+I+E+U', 42, 0),
(550, 'DE+KI+JU', 42, 0),
(551, 'Ejercicio 30.89', 71, 0),
(552, 'Ejercicio 30.90', 71, 0),
(553, 'Ejercicio 30.91', 71, 0),
(554, 'Ejercicio 30.92', 71, 0),
(555, 'Ejercicio 30.93', 71, 0),
(556, 'Ejercicio 30.94', 71, 0),
(557, 'Ejercicio 30.95', 71, 0),
(558, 'Ejercicio 30.96', 71, 0),
(559, 'Ejercicio 30.97', 71, 0),
(560, 'Ejercicio 30.98', 71, 0),
(561, 'Ejercicio 30.99', 71, 0),
(562, 'Ejercicio 30.100', 71, 0),
(563, 'Txanela - Europako Mendiak', 89, 0),
(564, 'base+e+i+u+r', 43, 0),
(565, '5f% ', 96, 0),
(566, '7j/ ', 96, 0),
(567, '567 pozo ', 96, 0),
(568, '4f$ ', 96, 0),
(569, '3d· ', 96, 0),
(570, '8k(', 96, 0),
(571, 'Ejercicio 31.1', 72, 0),
(572, 'Ejercicio 31.2', 72, 0),
(573, 'Ejercicio 31.3', 72, 0),
(574, 'Ejercicio 31.4', 72, 0),
(575, 'Ejercicio 31.5', 72, 0),
(576, 'Ejercicio 31.6', 72, 0),
(577, 'Ejercicio 31.7', 72, 0),
(578, 'Ejercicio 31.8', 72, 0),
(579, 'Ejercicio 31.9', 72, 0),
(580, 'Ejercicio 31.10', 72, 0),
(581, 'Ejercicio 31.11', 72, 0),
(582, 'Ejercicio 31.12', 72, 0),
(583, 'Ejercicio 31.13', 72, 0),
(584, 'Ejercicio 31.14', 72, 0),
(585, 'Ejercicio 31.15', 72, 0),
(586, 'Ejercicio 31.16', 72, 0),
(587, 'Ejercicio 31.17', 72, 0),
(588, 'Ejercicio 31.18', 72, 0),
(589, 'Ejercicio 31.19', 72, 0),
(590, 'Ejercicio 31.20', 72, 0),
(591, 'Ejercicio 31.21', 72, 0),
(592, 'Ejercicio 31.22', 72, 0),
(593, 'Ejercicio 31.23', 72, 0),
(594, 'base+u+i+e+r+o', 55, 0),
(595, 'Ejercicio 31. 24', 72, 0),
(596, 'Ejercicio 31.25', 72, 0),
(597, 'Ejercicio 31.26', 72, 0),
(598, 'Ejercicio 31.27', 72, 0),
(599, 'base+eiuro+w', 56, 0),
(600, 'base+ieurow+p', 58, 0),
(601, 'base+eruiow+p+q', 59, 0),
(602, 'Prueba 1 DICIEMBRE', 1, 0),
(603, 'Prueba 2 DICIEMBRE', 1, 0),
(604, 'Prueba 3 DICIEMBRE', 1, 0),
(605, '29.6', 1, 0),
(606, 'base+2º-t', 61, 0),
(607, 'Cambio climático', 76, 0),
(608, 'San Fermín', 76, 0),
(609, 'Osasuna', 76, 0),
(610, 'base+2ª', 63, 0),
(611, 'base+z', 68, 0),
(612, 'practicar t', 62, 0),
(613, 'practica Z', 68, 0),
(614, 'Ejercicio 31.28', 72, 0),
(615, 'Ejercicio 31.29', 72, 0),
(616, 'Ejercicio 31.30', 72, 0),
(617, 'Ejercicio 31.31', 72, 0),
(618, 'Ejercicio 31.32', 72, 0),
(619, 'Ejercicio 31.33', 72, 0),
(620, 'Ejercicio 31.34', 72, 0),
(621, 'Ejercicio 31.35', 72, 0),
(622, 'Ejercicio 31.36', 72, 0),
(623, 'Galápagos Islands II', 73, 0),
(624, 'Arrugas', 80, 0),
(625, 'Beatles', 73, 0),
(626, 'Lenon', 81, 0),
(627, 'Groenlandés', 80, 0),
(628, 'Pirata', 73, 0),
(629, 'San Francisco', 80, 0),
(630, 'Adventure of a lifetime', 76, 0),
(631, 'carta', 81, 0),
(632, 'Ejercicio 31.36', 72, 0),
(633, 'Ejercicio 31.37', 72, 0),
(634, 'Ejercicio 31.38', 72, 0),
(635, 'Ejercicio 31.39', 72, 0),
(636, 'Ejercicio 31.40', 72, 0),
(637, 'Ejercicio 31.41', 72, 0),
(638, 'Ejercicio 31.42', 72, 0),
(639, 'Ejercicio 31.43', 72, 0),
(640, 'Ejercicio 31.44', 72, 0),
(641, 'Ejercicio 31.45', 72, 0),
(642, 'Ejercicio 31.46', 72, 0),
(643, 'Ejercicio 31.47', 72, 0),
(644, 'Ejercicio 31.48', 72, 0),
(645, 'Motivación II', 69, 0),
(646, 'Galápagos ', 73, 0),
(647, 'Beatles II', 73, 0),
(648, 'Pirata II', 73, 0),
(649, 'Cambio climático II', 76, 0),
(650, 'SF', 76, 0),
(651, 'Osasuna II', 76, 0),
(652, 'Mecano', 70, 0),
(653, 'Mecano II', 70, 0),
(654, 'palabras y+t', 63, 0),
(655, 'Che', 70, 0),
(656, 'macarena', 70, 0),
(657, 'sol', 70, 0),
(658, 'Zombie', 70, 0),
(659, 'x,z', 70, 0),
(660, '29.N', 70, 0),
(661, 'Chiste', 70, 0),
(662, '29.1', 70, 0),
(663, 'Bizitza Europa', 89, 0),
(664, 'Bizitza Europa', 89, 0),
(665, 'Bizitza Europa', 89, 0),
(666, 'Bizitza Europa', 89, 0),
(667, 'Ejercicio 31.49', 72, 0),
(668, 'Ejercicio 31.50', 72, 0),
(669, 'Ejercicio 31.51', 72, 0),
(670, 'Ejercicio 31.52', 72, 0),
(671, 'Ejercicio 31.53', 72, 0),
(672, 'Ejercicio 31.54', 72, 0),
(673, 'Ejercicio 31.55', 72, 0),
(674, 'Ejercicio 31.55', 72, 0),
(675, 'Ejercicio 31.56', 72, 0),
(676, 'Ejercicio 31.57', 72, 0),
(677, 'Ejercicio 31.58', 72, 0),
(678, 'Ejercicio 31.59', 72, 0),
(679, 'Ejercicio 31.60', 72, 0),
(680, 'Ejercicio 31.61', 72, 0),
(681, 'Ejercicio 31.61', 72, 0),
(682, 'Ejercicio 31.62', 72, 0),
(683, 'Ejercicio 31.63', 72, 0),
(684, 'Ejercicio 31.64', 72, 0),
(685, 'Ejercicio 31.65', 72, 0),
(686, 'Ejercicio 31.66', 72, 0),
(687, 'Ejercicio 31.67', 72, 0),
(688, 'refranes', 80, 0),
(689, 'refranes I', 73, 0),
(690, 'refranes II', 73, 0),
(691, 'Cambio de hora ', 73, 0),
(692, 'Ejercicio 31.68', 72, 0),
(693, 'Ejercicio 31.69', 72, 0),
(694, 'Ejercicio 31.70', 72, 0),
(695, 'Ejercicio 31.71', 72, 0),
(696, 'Ejercicio 31.72', 72, 0),
(697, 'Ejercicio 31.73', 72, 0),
(698, 'Ejercicio 31.74', 72, 0),
(699, 'Ejercicio 31.75', 72, 0),
(700, 'Ejercicio 31.76', 72, 0),
(701, 'Ejercicio 31.77', 72, 0),
(702, 'Ejercicio 31.78', 72, 0),
(703, 'Ejercicio 31.79', 72, 0),
(704, 'Ejercicio 31.80', 72, 0),
(705, 'Ejercicio 31.81', 72, 0),
(706, 'z esaldiak', 48, 0),
(707, 'Ejercicio 31.82', 72, 0),
(708, 'Ejercicio 31.83', 72, 0),
(709, 'Ejercicio 31.84', 72, 0),
(710, 'Ejercicio 31.85', 72, 0),
(711, 'Ejercicio 31.86', 72, 0),
(712, 'Ejercicio 31.87', 72, 0),
(713, 'Ejercicio 31.88', 72, 0),
(714, 'Ejercicio 31.89', 72, 0),
(715, 'Ejercicio 31.90', 72, 0),
(716, 'Ejercicio 31.91', 72, 0),
(717, 'base+2ª+z+s', 69, 0),
(718, 'Ejercicio 31.92', 72, 0),
(719, 'Ejercicio 31.93', 72, 0),
(720, 'Ejercicio 31.94', 72, 0),
(721, 'Ejercicio 31.95', 72, 0),
(722, 'Ejercicio 31.96', 72, 0),
(723, 'Ejercicio 31.96', 72, 0),
(724, 'Ejercicio 31.97', 72, 0),
(725, 'Ejercicio 31.98', 72, 0),
(726, 'Ejercicio 31.99', 72, 0),
(727, 'Ejercicio 31.100', 72, 0),
(728, 'ej_30_base+2ª+z+x+c', 71, 0),
(729, 'ej_31_base+2ª+z+x+cv', 72, 0),
(730, 'Ejercicio 32.1', 76, 0),
(731, 'Ejercicio 32.2', 76, 0),
(732, 'Ejercicio 32.3', 76, 0),
(733, 'Ejercicio 32.4', 76, 0),
(734, 'Ejercicio 32.5', 76, 0),
(735, 'Ejercicio 32.6', 76, 0),
(736, 'Ejercicio 32.7', 76, 0),
(737, 'Ejercicio 32.8', 76, 0),
(738, 'Ejercicio 32.9', 76, 0),
(739, 'Ejercicio 32.10', 76, 0),
(740, 'Ejercicio 32.11', 76, 0),
(741, 'Ejercicio 32.12', 76, 0),
(742, 'Ejercicio 32.13', 76, 0),
(743, 'Ejercicio 32.14', 76, 0),
(744, 'Ejercicio 32.15', 76, 0),
(745, 'Ejercicio 32.16', 76, 0),
(746, 'Ejercicio 32.17', 76, 0),
(747, 'Ejercicio 32.18', 76, 0),
(748, 'Ejercicio 32.19', 76, 0),
(749, 'Ejercicio 32.20', 76, 0),
(750, 'Ejercicio 32.21', 76, 0),
(751, 'Ejercicio 32.22', 76, 0),
(752, 'Ejercicio 32.23', 76, 0),
(753, 'Ejercicio 32.24', 76, 0),
(754, 'Ejercicio 32.25', 76, 0),
(755, 'Ejercicio 32.26', 76, 0),
(756, 'Ejercicio 32.27', 76, 0),
(757, 'Ejercicio 32.28', 76, 0),
(758, 'Ejercicio 32.29', 76, 0),
(759, 'ej_32_+B', 76, 0),
(760, 'Ej_43_+M', 80, 0),
(761, 'Ej_35_+N', 81, 0),
(762, 'Txano Berde', 89, 0),
(763, 'KONDAIRA1', 6, 0),
(764, 'SERIE JF', 3, 0),
(765, 'KD', 10, 0),
(766, 'JFKD - largo', 11, 0),
(767, 'LS', 13, 0),
(768, 'ÑA', 14, 0),
(769, 'LSÑA- corto', 15, 0),
(770, ' JHJ- corto', 17, 0),
(771, 'FGF-largo', 18, 0),
(772, 'JUJ- largo', 42, 0),
(773, 'FRF- largo', 43, 0),
(774, 'SERIE JURA', 44, 0),
(775, 'SERIE HURGAR', 44, 0),
(776, 'SERIE LUGAR', 55, 0),
(777, 'SERIE JYJ', 11, 0),
(778, 'SERIE HAY', 11, 0),
(779, 'SERIE FTF', 11, 0),
(780, 'SERIE FUTURA', 11, 0),
(781, 'SERIE KIK', 11, 0),
(782, 'SERIE DED', 11, 0),
(783, 'SERIE HIJA', 11, 0),
(784, 'SERIE GAJE', 11, 0),
(785, 'SERIE LUGAR', 12, 0),
(786, 'SERIE LOL', 12, 0),
(787, 'SERIE HOLA', 12, 0),
(788, 'SERIE SWS', 12, 0),
(789, 'SERIE JMJ FVF ', 12, 0),
(790, 'SERIE MALVA', 12, 0),
(791, 'SERIE JNJ', 12, 0),
(792, 'SERIE JUNTA', 12, 0),
(793, 'SERIE FBF', 12, 0),
(794, 'SERIE FABRIL', 12, 0),
(795, 'SERIE NUBE', 13, 0),
(796, 'SERIE DCD', 13, 0),
(797, 'SERIE CIUDAD', 13, 0),
(798, 'SERIE JACA', 13, 0),
(799, 'SERIE K,K', 13, 0),
(800, 'SERIE CURA', 13, 0),
(801, 'SERIE ÑPÑ', 14, 0),
(802, 'ÑA mezclado a cuatro', 14, 0),
(803, 'SERIE POQUEDAD', 14, 0),
(804, 'SERIE J', 14, 0),
(805, 'SERIE F', 14, 0),
(806, 'SERIE JUAN', 14, 0),
(807, 'SERIE ESTA', 14, 0),
(808, 'SERIE MAR', 14, 0),
(809, 'SERIE AZA', 15, 0),
(810, 'SERIE JUEZ', 15, 0),
(811, 'SERIE -GOZO', 15, 0),
(812, 'SERIE L.L', 15, 0),
(813, 'SERIE SXS', 15, 0),
(814, 'SERIE ;EXAMEN:', 15, 0),
(815, 'SERIE LÁPIZ', 15, 0),
(816, 'SERIE ÉLÈVE', 15, 0),
(817, 'nueva_prueba', 6, 0),
(818, 'fjhgfjghfjhgfjhgj', 6, 0),
(819, 'ariketa1', 6, 0),
(820, 'Ardo Gorri Naparra', 89, 0),
(821, 'Gatibu - Pailazo - Lyric', 89, 0),
(822, 'Hesian - Esaidazu - Lyric', 89, 0),
(823, 'Justin Bieber - Sorry 2 - Lyric', 89, 0),
(824, 'Red Lights - Lyric', 89, 0),
(825, 'Zenbakiak 1-5', 88, 0),
(826, 'lyric Picky - Joey Montana', 89, 0),
(827, 'Berri Txarrak - Oreka - Lyric', 89, 0),
(828, 'El Beso - El Villano - Lyric', 89, 0),
(829, 'Malu - A Prueba de ti - Lyric', 89, 0),
(830, 'Zea Mays - Negua Joanda Ta - Lyric', 89, 0),
(831, 'Vendetta - Ekainak 24 - Lyric', 89, 0),
(832, 'Hesian - Behar Zaitut - Lyric', 89, 0),
(833, 'Gatibu - Euritan Dantzan - Lyric', 89, 0),
(834, 'Txanela 5 - Argiaren Propietateak - Islapena', 4, 0),
(835, 'Txanela 5 - Argiaren Propietateak - Errefrakzioa', 4, 0),
(836, 'Txanela 5 - Argiaren Propietateak - Ostadarra', 4, 0),
(837, 'Froga', 5, 0),
(838, 'Froga', 5, 0),
(839, 'Txapelketa Berastegi', 5, 0),
(840, 'froga 1', 0, 0),
(841, 'eurre', 4, 0),
(842, 'borrar', 5, 0),
(843, 'prueba', 3, 0),
(844, 'ejercicioprueba', 3, 0),
(845, 'eaorain', 0, 0),
(846, 'LSÑA- largo', 22, 0),
(847, 'FGF-corto', 18, 0),
(848, 'FRF-largo', 43, 0),
(849, 'JHJ-largo', 17, 0),
(850, 'posición base ', 41, 0),
(851, 'posición base al revés', 41, 0),
(852, 'posición base mezclado', 41, 0),
(853, 'SDFJKL mezclado', 13, 0),
(854, 'SDF-LKJ', 0, 0),
(855, 'SDF-LKJ', 13, 0),
(856, 'Signos', 96, 0),
(857, 'Palabras con diéresis', 95, 0),
(858, 'Palabras con acento', 95, 0),
(859, 'Subguion', 94, 0),
(860, 'El guion', 94, 0),
(861, 'Palabras con guion', 94, 0),
(862, 'Los dos puntos', 93, 0),
(863, 'El punto', 93, 0),
(864, 'La coma', 92, 0),
(865, 'Palabras y comas', 92, 0),
(866, 'Punto y coma', 92, 0),
(867, 'Mayúscula Derecha', 91, 0),
(868, 'Mayúscula Derecha Nombres', 91, 0),
(869, 'Mayúscula Derecha Nombres', 91, 0),
(870, 'Mayúscula Izquierda', 90, 0),
(871, 'Mayúsculas Izquierda Nombres', 90, 0),
(872, 'J,M y N a cuatro', 82, 0),
(873, 'Frases con M y N', 90, 0),
(874, 'Frases con M y N', 82, 0),
(875, 'J y N a cuatro', 81, 0),
(876, 'J y N a cuatro', 81, 0),
(877, 'J y M a cuatro', 80, 0),
(878, 'D, F, C, V y B a cuatro', 73, 0),
(879, 'D, F, C, V y B a cuatro', 73, 0),
(880, 'D, F, C, V y B a cuatro', 73, 0),
(881, 'F y B a cuatro', 76, 0),
(882, 'F y V a cuatro', 72, 0),
(883, 'Frases con la V', 72, 0),
(884, 'D y C a cuatro', 71, 0),
(885, 'Frases con la C', 71, 0),
(886, 'kik-largo', 39, 0),
(887, 'medio+i mezcladas', 39, 0),
(888, 'medio + i palabras', 39, 0),
(889, 'ded largo', 40, 0),
(890, 'medio+e mezcladas', 1, 0),
(891, 'medio + palabras con e', 40, 0),
(892, 'palabras medio+e+i', 41, 0),
(893, 'palabras con u', 42, 0),
(894, 'Lasaña', 15, 0),
(895, 'lol largo', 55, 0),
(896, 'palabras con w', 56, 0),
(897, 'practica o+w', 57, 0),
(898, 'palabras o + w', 57, 0),
(899, 'palabras p', 58, 0),
(900, 'practicar y+t', 63, 0),
(901, 'practica sx', 69, 0),
(902, 'practica z+x', 70, 0),
(903, 'palabras z+x', 70, 0),
(904, '150 3%', 63, 0),
(905, 'lasaña', 22, 0),
(906, '200 3%', 68, 0),
(907, 'Liryc - Pusho - Te fuiste 1', 89, 0),
(908, 'Liryc - Pusho - Te fuiste 3', 89, 0),
(909, 'Liryc - Pusho - Te fuiste 2', 89, 0),
(910, 'prueba 4 noviembre vedruna', 2, 0),
(911, 'prueba 4 noviembre vedruna II 250 pulsaciones', 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_excercise_sub`
--

CREATE TABLE `tbl_excercise_sub` (
  `excercise_sub_id` int(11) NOT NULL,
  `excercise_sub_name` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `excercise_sub_content` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `excercise_sub_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `excercise_sub_max_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `position_order` int(11) NOT NULL,
  `excercise_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_group`
--

CREATE TABLE `tbl_group` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `center_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_group`
--

INSERT INTO `tbl_group` (`group_id`, `group_name`, `center_id`) VALUES
(1, 'Euskara Bikoizteko', 1),
(2, 'Aurreratuak', 1),
(5, 'Avanzados Para Duplicar', 1),
(6, 'Mecanografía Básica', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_group_professor`
--

CREATE TABLE `tbl_group_professor` (
  `user_id` int(11) NOT NULL,
  `center_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_group_professor`
--

INSERT INTO `tbl_group_professor` (`user_id`, `center_id`, `group_id`) VALUES
(10, 1, 1),
(10, 1, 2),
(11, 1, 1),
(14, 5, 6),
(14, 1, 2),
(13, 1, 1),
(14, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_group_student`
--

CREATE TABLE `tbl_group_student` (
  `user_id` int(11) NOT NULL,
  `center_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_group_student`
--

INSERT INTO `tbl_group_student` (`user_id`, `center_id`, `group_id`) VALUES
(15, 1, 1),
(16, 1, 1),
(19, 5, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson`
--

CREATE TABLE `tbl_lesson` (
  `lesson_id` int(11) NOT NULL,
  `lesson_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `position_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_lesson`
--

INSERT INTO `tbl_lesson` (`lesson_id`, `lesson_name`, `position_order`) VALUES
(1, 'Presentación', 1),
(2, 'Posición inicial', 3),
(3, 'Lección 1', 5),
(4, 'Aurkezpena', 2),
(5, 'Oinarrizko posizioa', 4),
(6, '1. irakasgaia', 6),
(7, '2. irakasgaia', 7),
(8, '3. irakasgaia', 10),
(9, '4. irakasgaia', 6),
(10, 'Lección 2', 8),
(11, 'Lección 3', 9),
(12, 'Lección 4', 6),
(13, 'Lección 5', 7),
(14, 'Lección 6', 8),
(15, 'Lección 7', 9),
(16, 'Lección 8', 10),
(17, 'Lección 9', 11),
(18, 'Lección 10', 12),
(19, 'Lección 11', 13),
(20, '5. irakasgaia', 7),
(21, '6. irakasgaia', 8),
(22, '7. irakasgaia', 9),
(23, '8. irakasgaia', 10),
(24, '9. irakasgaia', 11),
(25, '10. irakasgaia', 12),
(26, '11. irakasgaia', 13),
(27, '12. irakasgaia', 14),
(28, '13. irakasgaia', 15),
(29, '14. irakasgaia', 16),
(30, '15. irakasgaia', 17),
(31, '16. irakasgaia', 18),
(32, '17. irakasgaia', 19),
(33, '18. irakasgaia', 20),
(34, '19. irakasgaia', 21),
(35, '20. irakasgaia', 22),
(36, '21. irakasgaia', 23),
(37, '22. irakasgaia', 24),
(38, '23. irakasgaia', 25),
(39, 'Lección 12', 14),
(40, 'Lección 13', 15),
(41, 'Lección 14', 16),
(42, 'Lección 15', 17),
(43, 'Lección 16', 18),
(44, 'Lección 17', 19),
(45, '24. irakasgaia', 26),
(46, '25. irakasgaia', 27),
(47, '26. irakasgaia', 28),
(48, '27. irakasgaia', 29),
(49, '28. irakasgaia', 30),
(50, '29. irakasgaia', 31),
(51, '30. irakasgaia', 32),
(52, '31. irakasgaia', 33),
(53, '32. irakasgaia', 34),
(54, '33. irakasgaia', 35),
(55, 'Lección 18', 20),
(56, 'Lección 19', 21),
(57, 'Lección 20', 22),
(58, 'Lección 21', 23),
(59, 'Lección 22', 24),
(60, 'Lección 23', 25),
(61, 'Lección 24', 26),
(62, 'Lección 25', 27),
(63, 'Lección 26', 28),
(64, '34. irakasgaia', 36),
(65, 'Lección 27', 29),
(66, 'Lección 28', 30),
(67, 'Lección 29', 31),
(68, 'Lección 30', 32),
(69, 'Lección 31', 33),
(70, 'Lección 33', 35),
(71, '35. irakasgaia', 37),
(72, 'Lección 32', 34),
(73, '36. irakasgaia', 38),
(74, 'Lección 34', 36),
(75, 'Lección 35', 37),
(76, 'Lección 36', 38),
(77, '37. irakasgaia', 39),
(78, '38. irakasgaia', 40),
(79, '39. irakasgaia', 41),
(80, '40. irakasgaia', 42),
(81, '41. irakasgaia', 43),
(82, '42. irakasgaia', 44),
(83, '43. irakasgaia', 45),
(84, 'Lección 37', 39),
(85, 'Lección 38', 40),
(86, 'Lección 39', 41),
(87, 'Lección 40', 42),
(88, 'Lección 41', 43),
(89, 'Lección 42', 44),
(90, 'Lección 43', 45);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_detail`
--

CREATE TABLE `tbl_lesson_detail` (
  `lesson_detail_id` int(11) NOT NULL,
  `lesson_detail_content` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `lesson_time` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `lesson_max_time` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `lesson_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_dictation_student`
--

CREATE TABLE `tbl_lesson_dictation_student` (
  `lesson_dictation_student_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_exercise_student`
--

CREATE TABLE `tbl_lesson_exercise_student` (
  `lesson_dictation_student_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_group`
--

CREATE TABLE `tbl_lesson_group` (
  `lesson_group_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_group_student`
--

CREATE TABLE `tbl_lesson_group_student` (
  `lesson_group_student_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_media`
--

CREATE TABLE `tbl_lesson_media` (
  `lesson_id` int(11) NOT NULL,
  `media1` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `media2` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_student`
--

CREATE TABLE `tbl_student` (
  `user_id` int(11) NOT NULL,
  `center_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_student`
--

INSERT INTO `tbl_student` (`user_id`, `center_id`) VALUES
(15, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_nickname` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `user_name` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `user_email` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `user_password` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `user_level` varchar(3) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_nickname`, `user_name`, `user_email`, `user_password`, `user_level`) VALUES
(1, 'admin', 'Julio Iturre', 'julio.iturre@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1'),
(2, 'willans.flores', 'Willans Flores', 'willanfloresc@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1'),
(6, 'juan.perez', 'Juan Perez', 'dir@dir1.com', 'e10adc3949ba59abbe56e057f20f883e', '2'),
(7, 'rodrigo.suarez', 'Rodrigo Suarez', 'dir@dir2.com', '74c43b7ec689955c9c1517294e92500f', '2'),
(8, 'julio.ausin', 'Julio Ausin', 'dir@dir3.com', '14fa3d8304d25e374700550ff0d945b2', '2'),
(9, 'carlos.iturre', 'Carlos Iturre', 'dir@dir4.com', 'e10adc3949ba59abbe56e057f20f883e', '2'),
(10, 'alvaro.fernandez', 'Alvaro Fernandez', 'alvaroernandez@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '3'),
(11, 'erik.aznal', 'Erik Aznal', 'erikznal@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '3'),
(12, 'ibon.rodriguez', 'Ibon Rodriguez', 'ibonrodriguez@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '3'),
(13, 'iradi.galarza', 'Irati Galarza', 'iratigalarza@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '3'),
(14, 'irene.fernandez', 'Irene Fernandez', 'irenefernandez@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '3'),
(15, 'aaron.rodrigo.larrea', 'Aaron Rodrigo Larrea', 'aaron.rodrigo@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '4'),
(16, 'adhara.corretge', 'Adhara Corretge', 'adhara.corretge@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '4'),
(18, 'carlos.martinez', 'Carlos Martinez', 'carlos_martinez@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1'),
(19, 'luis.mayo', 'Luis Mayo', 'erestu@terra.es', '25f9e794323b453885f5181f1b624d0b', '4');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_additional_excercise`
--
ALTER TABLE `tbl_additional_excercise`
  ADD PRIMARY KEY (`additional_excercise_id`);

--
-- Indices de la tabla `tbl_additional_excercise_detail`
--
ALTER TABLE `tbl_additional_excercise_detail`
  ADD PRIMARY KEY (`additional_excercise_detail_id`);

--
-- Indices de la tabla `tbl_additional_excercise_group`
--
ALTER TABLE `tbl_additional_excercise_group`
  ADD PRIMARY KEY (`aditional_excercise_group_id`);

--
-- Indices de la tabla `tbl_additional_excercise_group_student`
--
ALTER TABLE `tbl_additional_excercise_group_student`
  ADD PRIMARY KEY (`aditional_excercise_group`);

--
-- Indices de la tabla `tbl_center`
--
ALTER TABLE `tbl_center`
  ADD PRIMARY KEY (`center_id`);

--
-- Indices de la tabla `tbl_dictation`
--
ALTER TABLE `tbl_dictation`
  ADD PRIMARY KEY (`dictation_id`);

--
-- Indices de la tabla `tbl_dictation_sub`
--
ALTER TABLE `tbl_dictation_sub`
  ADD PRIMARY KEY (`dictation_sub_id`);

--
-- Indices de la tabla `tbl_excercise`
--
ALTER TABLE `tbl_excercise`
  ADD PRIMARY KEY (`excercise_id`);

--
-- Indices de la tabla `tbl_excercise_sub`
--
ALTER TABLE `tbl_excercise_sub`
  ADD PRIMARY KEY (`excercise_sub_id`);

--
-- Indices de la tabla `tbl_group`
--
ALTER TABLE `tbl_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indices de la tabla `tbl_lesson`
--
ALTER TABLE `tbl_lesson`
  ADD PRIMARY KEY (`lesson_id`);

--
-- Indices de la tabla `tbl_lesson_detail`
--
ALTER TABLE `tbl_lesson_detail`
  ADD PRIMARY KEY (`lesson_detail_id`);

--
-- Indices de la tabla `tbl_lesson_dictation_student`
--
ALTER TABLE `tbl_lesson_dictation_student`
  ADD PRIMARY KEY (`lesson_dictation_student_id`);

--
-- Indices de la tabla `tbl_lesson_exercise_student`
--
ALTER TABLE `tbl_lesson_exercise_student`
  ADD PRIMARY KEY (`lesson_dictation_student_id`);

--
-- Indices de la tabla `tbl_lesson_group`
--
ALTER TABLE `tbl_lesson_group`
  ADD PRIMARY KEY (`lesson_group_id`);

--
-- Indices de la tabla `tbl_lesson_group_student`
--
ALTER TABLE `tbl_lesson_group_student`
  ADD PRIMARY KEY (`lesson_group_student_id`);

--
-- Indices de la tabla `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_additional_excercise`
--
ALTER TABLE `tbl_additional_excercise`
  MODIFY `additional_excercise_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_additional_excercise_detail`
--
ALTER TABLE `tbl_additional_excercise_detail`
  MODIFY `additional_excercise_detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_additional_excercise_group`
--
ALTER TABLE `tbl_additional_excercise_group`
  MODIFY `aditional_excercise_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_additional_excercise_group_student`
--
ALTER TABLE `tbl_additional_excercise_group_student`
  MODIFY `aditional_excercise_group` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_center`
--
ALTER TABLE `tbl_center`
  MODIFY `center_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `tbl_dictation`
--
ALTER TABLE `tbl_dictation`
  MODIFY `dictation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_dictation_sub`
--
ALTER TABLE `tbl_dictation_sub`
  MODIFY `dictation_sub_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_excercise`
--
ALTER TABLE `tbl_excercise`
  MODIFY `excercise_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=912;
--
-- AUTO_INCREMENT de la tabla `tbl_excercise_sub`
--
ALTER TABLE `tbl_excercise_sub`
  MODIFY `excercise_sub_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_group`
--
ALTER TABLE `tbl_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tbl_lesson`
--
ALTER TABLE `tbl_lesson`
  MODIFY `lesson_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT de la tabla `tbl_lesson_detail`
--
ALTER TABLE `tbl_lesson_detail`
  MODIFY `lesson_detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_lesson_dictation_student`
--
ALTER TABLE `tbl_lesson_dictation_student`
  MODIFY `lesson_dictation_student_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_lesson_exercise_student`
--
ALTER TABLE `tbl_lesson_exercise_student`
  MODIFY `lesson_dictation_student_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_lesson_group`
--
ALTER TABLE `tbl_lesson_group`
  MODIFY `lesson_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_lesson_group_student`
--
ALTER TABLE `tbl_lesson_group_student`
  MODIFY `lesson_group_student_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
