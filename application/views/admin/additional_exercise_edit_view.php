<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <!-- LISTADO DE CENTROS -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <!--<h1>Lista de Lecciones</h1>-->
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                    <form action="<?php echo base_url(); ?>admin/additional_exercise/additional_exercise_edit" method="post">
                                    	<input type="hidden" name="additional_exercise_id" value="<?php echo $ae_id ?>">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">                                                 
                                                <div class="form-group">
                                                    <label>Nombre:</label>
                                                    <input name="additional_exercise_name" type="text" class="form-control" placeholder="Ejercicio Adicionañ" value="<?php echo $additional_exercises[0]->additional_excercise_name; ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="chosen-select-single mg-b-20">
                                                    <label><strong>Idioma actual:</strong> <?php
                                                        if ($additional_exercises[0]->additional_excercise_lang == "es"){
                                                            echo "Español";
                                                        }elseif ($additional_exercises[0]->additional_excercise_lang == "eu"){
                                                            echo "Euskera";
                                                        }else{
                                                            echo "Sin asignar";
                                                        }
                                                    ?></label>
                                                    <input type="hidden" value="<?php echo $additional_exercises[0]->additional_excercise_lang; ?>" name="actual_lang">
                                                    <select data-placeholder="Asignar Idioma..." class="chosen-select" tabindex="-1" name="ae_lang">
                                                        <option value="">Asignar Idioma</option>
                                                        <option value="es">Español</option>
                                                        <option value="eu">Euskera</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="tinymce-single" style="padding-left: 0px;">
                                                    <div class="alert-title">
                                                        <h2 style="color:#333">Introduce el Ejercicio</h2>
                                                        <p style="color:#333">Introduce el ejercicio exactamente como lo digitará el estudiante. Los espacios serán intrepretados como tal.</p>
                                                    </div>
                                                    <!--<div id="summernote2">
                                                        <div class="note-editable panel-body" contenteditable="true" style="height: 200px;">
                                                            
                                                        </div>
                                                    </div>-->
                                                    <textarea name="additional_excercise_content" style="height:200px;font-size:30px;letter-spacing: 10px;display: block;background: #fff;width: 100%;padding: 20px;"><?php echo $additional_exercises[0]->additional_excercise_content; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<div class="alert-title">
													<h2 style="color:#333">Tiempo del Ejercicio (en segundos)</h2>
                                                     <div class="form-group">
														<input name="additional_excercise_time" type="text" class="form-control" style="width: 100px" value="<?php echo $additional_exercises[0]->additional_excercise_time; ?>">
													</div>
												</div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="alert-title">
                                                    <h2 style="color:#333">Tiempo máximo del Ejercicio (en segundos)</h2>
                                                     <div class="form-group">
                                                        <input name="additional_excercise_max_time" type="text" class="form-control" style="width: 100px" value="<?php echo $additional_exercises[0]->additional_excercise_max_time; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                <div class="payment-adress" style="text-align:left">
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
    </div>

<?php $this->load->view('admin/z_footer'); ?>