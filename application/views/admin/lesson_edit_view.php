<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <!-- LISTADO DE CENTROS -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <!--<h1>Lista de Lecciones</h1>-->
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                    <form action="<?php echo base_url(); ?>admin/lesson/lesson_edit" method="post" enctype="multipart/form-data">
                                    	<input type="hidden" name="lesson_id" value="<?php echo $l_id ?>">
                                    	<?php if (!empty($lessons[0]->media1)){ ?>
                                    		<input type="hidden" name="media1" value="<?php echo $lessons[0]->media1; ?>">
                                    	<?php } ?>
                                    	<?php if (!empty($lessons[0]->media2)){ ?>
                                    		<input type="hidden" name="media2" value="<?php echo $lessons[0]->media2; ?>">
                                    	<?php } ?>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group">
                                                    <label>Lección</label>
                                                    <input name="lesson_name" type="text" class="form-control" placeholder="Lección" value="<?php echo $lessons[0]->lesson_name; ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="chosen-select-single mg-b-20">
                                                    <label><strong>Idioma actual:</strong> <?php
                                                        if ($lessons[0]->lesson_lang == "es"){
                                                            echo "Español";
                                                        }elseif ($lessons[0]->lesson_lang == "eu"){
                                                            echo "Euskera";
                                                        }else{
                                                            echo "Sin asignar";
                                                        }
                                                    ?></label>
                                                    <input type="hidden" name="actual_lang" value="<?php echo $lessons[0]->lesson_lang; ?>">
                                                    <select data-placeholder="Asignar Idioma..." class="chosen-select" tabindex="-1" name="ae_lang">
                                                        <option value="">Asignar Idioma</option>
                                                        <option value="es">Español</option>
                                                        <option value="eu">Euskera</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <hr>
                                                <h5>Agregar Multimedia (OPCIONAL).</h5>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<div class="file-upload-inner ts-forms">
													<div class="input prepend-small-btn">
														<div class="file-button">
															MP4 <input type="file" name="userfile"  onchange="document.getElementById('prepend-small-btn').value = this.value;">
														</div>
														<input autocomplete="off" type="text" id="prepend-small-btn" placeholder="sin archivo">
													</div>
												</div>

												<?php if (!empty($lessons[0]->media1)){ ?>
													<div class="video-demo" style="margin-top:20px">
														<h5>Video actual.</h5>
														<p><input type="checkbox" name="delete_videomp4"> Marque esta casilla para eliminar el video</p>
														<video width="100%" controls>
															<source src="<?php echo base_url(); ?><?php echo $lessons[0]->media1;?>.mp4" type="video/mp4">
															<!--<source src="mov_bbb.ogg" type="video/ogg">-->
															Your browser does not support HTML5 video.
														</video>
													</div>
												<?php } ?>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<div class="file-upload-inner ts-forms">
													<div class="input prepend-small-btn">
														<div class="file-button">
															OGV <input type="file" name="userfile2"  onchange="document.getElementById('prepend-small-btn-2').value = this.value;">
														</div>
														<input autocomplete="off" type="text" id="prepend-small-btn-2" placeholder="sin archivo">
													</div>
												</div>
												<?php if (!empty($lessons[0]->media2)){  ?>
													<div class="video-demo" style="margin-top:20px">
														<h5>Video actual.</h5>
														<p><input type="checkbox" name="delete_videoogv"> Marque esta casilla para eliminar el video</p>
														<video width="100%" controls>
															<source src="<?php echo base_url(); ?><?php echo $lessons[0]->media2;?>.ogv" type="video/ogg">
															<!--<source src="mov_bbb.ogg" type="video/ogg">-->
															Your browser does not support HTML5 video.
														</video>
													</div>
												<?php } ?>
											</div>
										</div>
                                        
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="tinymce-single" style="padding-left: 0px;">
                                                    <div class="alert-title">
                                                        <h2 style="color:#333">Introduce el Ejercicio</h2>
                                                        <p style="color:#333">Introduce el ejercicio exactamente como lo digitará el estudiante. Los espacios serán intrepretados como tal.</p>
                                                    </div>
                                                    <!--<div id="summernote2">
                                                        <div class="note-editable panel-body" contenteditable="true" style="height: 200px;">
                                                            
                                                        </div>
                                                    </div>-->
                                                    <textarea name="lesson_content" style="height:200px;font-size:30px;letter-spacing: 10px;display: block;background: #fff;width: 100%;padding: 20px;"><?php echo $lessons[0]->lesson_detail_content; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<div class="alert-title">
													<h2 style="color:#333">Tiempo del Ejercicio (en segundos)</h2>
                                                     <div class="form-group">
														<input name="lesson_time" type="text" class="form-control" style="width: 100px" value="<?php echo $lessons[0]->lesson_time; ?>">
													</div>
												</div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="alert-title">
                                                    <h2 style="color:#333">Tiempo máximo del Ejercicio (en segundos)</h2>
                                                     <div class="form-group">
                                                        <input name="lesson_max_time" type="text" class="form-control" style="width: 100px" value="<?php echo $lessons[0]->lesson_max_time; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                <div class="payment-adress" style="text-align:left">
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
    </div>

<?php $this->load->view('admin/z_footer'); ?>