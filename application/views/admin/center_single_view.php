<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
                
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <div class="container-fluid">
            <div class="row">
            
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="product-sales-chart">
                        <h4 style="margin-bottom:20px">Datos de este curso</h4>
                        <p> <?php echo $title; ?></p>
                        <p>Director asignado: <?php echo $manager[0]->user_name; ?></p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="product-sales-chart">
                        <h4>Grupos de este Centro</h4>
                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                <style>
                                    .custom-datatable-overright table tbody tr td{vertical-align: middle;}
                                </style>
                                <table id="table" data-toggle="table" data-pagination="false" data-search="false" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
                                    data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                        <tr>
                                            <th data-field="id">#</th>
                                            <th data-field="grupos" data-editable="false">Grupo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; foreach ($groups_list as $group){ ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td>
                                                    <a class="editable-click" href="<?php echo base_url(); ?>admin/group/group_edit_view/<?php echo $group->group_id; ?>" title="<?php echo $group->group_name; ?>">
                                                        <?php echo $group->group_name; ?>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
<?php $this->load->view('admin/z_footer'); ?>
