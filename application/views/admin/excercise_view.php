<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <!-- FORM PARA CREAR UN NUEVO CENTRO -->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div id="dropzone1" class="pro-ad">
                                        <!-- LÍNEA DE FORMULARIO PARA CREAR UN SOLO CENTRO -->
                                        <!--<form action="<?php #echo base_url(); ?>admin/center/create_center" method="post">-->
                                        <a class="btn btn-primary" href="<?php echo base_url() ?>admin/excercise/create_dictation_view">
                                            <i class="fa fa-check edu-checked-pro" aria-hidden="true"></i> Nuevo Ejercicio
                                        </a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- LISTADO DE CENTROS -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Lista de Ejercicios</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                    <style>
                                        .custom-datatable-overright table tbody tr td{vertical-align: middle;}
                                        .subtitle{display: block;float: left;padding-top: 10px}
                                        .remove_filter{background:#b70038; color:#fff; padding: 3px 8px; border-radius:20px; }
                                        .langs{color:#333; text-decoration:none; margin-right: 10px;}
                                        .langs.current{background:#b70038; color:#fff; padding: 3px 8px; border-radius:20px; }
                                    </style>
                                    <div class="subtitle">
                                        <h5>
                                            <?php $with_lang = $this->uri->segment(3); ?>
                                            <a title="Español" class="langs esp <?php echo (empty($with_lang) ? "current" : ""); ?>" href="<?php echo base_url(); ?>admin/excercise">
                                                <i class="fa fa-link"></i> Español
                                            </a>
                                            <a title="Euskera" class="langs eus <?php echo (empty($with_lang) ? "" : "current"); ?>" href="<?php echo base_url(uri_string()); ?>/eu">
                                                <i class="fa fa-link"></i> Euskera
                                            </a>
                                            <?php if (!empty($get_onelesson)){ ?>
                                                <style type="text/css">.langs{display: none !important;}</style>
                                                <a title="Eliminar Filtro" class="remove_filter" href="<?php echo base_url(); ?>admin/dictation"><?php echo $subtitle; ?>: <?php foreach ($get_onelesson as $lesson) {
                                                    echo $lesson->lesson_name;
                                                } ?>&nbsp;&nbsp;&nbsp;<i class="fa fa-close"></i></a>
                                            <?php } ?>
                                        </h5>
                                    </div>
                                    <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                            <tr>
                                                <th data-field="id">#</th>
                                                <th data-field="dictation" data-editable="false">Nombre de Ejercicio</th>
                                                <th data-field="lesson" data-editable="false">Pertenece a lección</th>
                                                <th data-field="visible" data-editable="false">Visible</th>
                                                <th data-field="action">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody class="row_position">
                                            <?php $i = 1; foreach ($dictation_list as $dictation){ ?>
                                                <tr id="<?php echo $dictation->exercise_id; ?>">
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $dictation->exercise_name ; ?></td>
                                                    <td>
                                                        <?php
                                                        $lesson_belong = "SELECT * FROM tbl_lesson WHERE lesson_id = ". $dictation->lesson_id;
                                                        $query_belong = $this->db->query($lesson_belong);
                                                        if (!empty($query_belong->result())){
                                                            foreach ($query_belong->result() as $row) {
                                                        ?>
                                                            <a class="editable-click" href="<?php echo base_url(); ?>admin/lesson/lesson_single_view/<?php echo $row->lesson_id; ?>" title=""><?php echo $row->lesson_name; ?></a>
                                                            <a class="btn btn-primary" style="color: #fff;font-size: 13px;padding: 3px 5px;margin-left: 10px;" href="<?php echo base_url() ?>admin/excercise/filter_bylesson/<?php echo $dictation->exercise_id; ?>">
                                                                <i class="fa fa-filter" aria-hidden="true"></i> Filtrar por Lección
                                                            </a>
                                                        <?php
                                                            }
                                                        }else{
                                                            echo "Sin asignar";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php echo ($dictation->exercise_visible == 1) ? "Si" : "No"; ?>
                                                    </td>
                                                    <td class="datatable-ct">
                                                        <a class="btn btn-primary" style="color:#fff" href="<?php echo base_url() ?>admin/excercise/dictation_edit_view/<?php echo $dictation->exercise_id; ?>">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Ejercicio
                                                        </a>
                                                        <div id="Eliminar<?php echo $i; ?>" class="modal modal-edu-general FullColor-popup-DangerModal fade" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-close-area modal-close-df">
                                                                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <span class="educate-icon educate-danger modal-check-pro information-icon-pro"></span>
                                                                        <h2>Alerta!</h2>
                                                                        <p>Está seguro de Eliminar el Ejercicio <strong style="text-decoration: underline;"><?php echo $dictation->exercise_name; ?> y su contenido?</strong></p>
                                                                    </div>
                                                                    <div class="modal-footer danger-md">
                                                                        <a data-dismiss="modal" href="#" style="color:#fff;">Cancelar</a>
                                                                        <a href="<?php echo base_url(); ?>admin/excercise/DeleteExercise/<?php echo $dictation->exercise_id; ?>/<?php echo $dictation->exercise_content; ?>" style="color:#fff;">Eliminar</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a class="btn btn-primary" href="#" style="color:#fff;" data-toggle="modal" data-target="#Eliminar<?php echo $i; ?>"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar Ejercicio</a>
                                                    </td>
                                                </tr>
                                            <?php $i++; } ?>
                                        </tbody>
                                    </table>
                                    <script type="text/javascript">
                                        $( ".row_position" ).sortable({
                                            delay: 150,
                                            stop: function() {
                                                var selectedData = new Array();
                                                $('.row_position > tr').each(function() {
                                                    selectedData.push($(this).attr("id"));
                                                });
                                                updateOrder(selectedData);
                                            }
                                        });
                                        function updateOrder(data) {
                                            $.ajax({
                                                type:"post",
                                                url: "<?php echo base_url(); ?>admin/excercise/re_order",
                                                data:{position:data},
                                                success:function(){
                                                    //alert('your change successfully saved');
                                                }
                                            })
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
    </div>

<?php $this->load->view('admin/z_footer'); ?>