<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <!-- FORM PARA CREAR UN NUEVO CENTRO -->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div id="dropzone1" class="pro-ad">
                                        <!-- LÍNEA DE FORMULARIO PARA CREAR UN SOLO CENTRO -->
                                        <!--<form action="<?php #echo base_url(); ?>admin/center/create_center" method="post">-->
                                        
                                        <form action="<?php echo base_url(); ?>admin/user/create_user" method="post">
                                            <input type="hidden" name="user_level" value="4">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <?php if (!empty($message)){ ?>
                                                        <div class="alert alert-success alert-st-one" role="alert">
                                                            <i class="fa fa-check edu-checked-pro admin-check-pro admin-check-pro-none" aria-hidden="true"></i>
                                                            <span class="message-mg-rt message-alert-none"><?php echo $message; ?></span>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">                                                 
                                                    <div class="form-group">
                                                        <input name="user_name" type="text" class="form-control" placeholder="Nombre" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">                                                 
                                                    <div class="form-group">
                                                        <input name="user_email" type="text" class="form-control" placeholder="Email" required>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    function randomPassword(length) {
                                                        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
                                                        var pass = "";
                                                        for (var x = 0; x < length; x++) {
                                                            var i = Math.floor(Math.random() * chars.length);
                                                            pass += chars.charAt(i);
                                                        }
                                                        return pass;
                                                    }
                                                    jQuery(document).ready(function($) {
                                                        $("#generate_p").click(function(event) {
                                                            $("#user_password").val(randomPassword(8));

                                                        });
                                                    });
                                                </script>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">                                                 
                                                    <div class="form-group">
                                                        <input name="user_password" id="user_password" type="text" class="form-control" placeholder="Contraseña" required autocomplete="off">
                                                        <input type="button" class="button" value="Generar Contraseña" id="generate_p" tabindex="2">
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                    <div class="payment-adress" style="text-align: left;padding-top: 4px;">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                 <div class="col-lg-9 col-md-9 col-sm-3 col-xs-12">                                                 
                                                    <div class="alert alert-warning" role="alert">El nombre de usuario será generado automáticamente.</div>
                                                </div>
                                            </div>
                                            <!--<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="payment-adress">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                                    </div>
                                                </div>
                                            </div>-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- LISTADO DE CENTROS -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Lista de Estudiantes</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                    <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                            <tr>
                                                <th data-field="id">#</th>
                                                <th data-field="name" data-editable="false">Nombre</th>
                                                <th data-field="username" data-editable="false">Nombre de usuario</th>
                                                <th data-field="email" data-editable="false">Email</th>
                                                <th data-field="center" data-editable="false">Centro</th>
                                                <th data-field="groups" data-editable="false">Grupo</th>
                                                <th data-field="action">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; foreach ($user_list as $user){ ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $user->user_name; ?></td>
                                                    <td><?php echo $user->user_nickname; ?></td>
                                                    <td><a class="editable-click" target="_blank" href="mailto:<?php echo $user->user_email; ?>" "email me"><?php echo $user->user_email; ?></a></td>
                                                    <td>
                                                        <?php
                                                        $sql = "SELECT * FROM `tbl_center_student` as tbl_ce LEFT JOIN `tbl_center` as tbl_c ON tbl_ce.center_id = tbl_c.center_id WHERE tbl_ce.user_id = ".$user->user_id;
                                                        $query = $this->db->query($sql);
                                                        if (!empty($query->result())){
                                                        ?>
                                                            <ul>
                                                                <?php foreach ($query->result() as $row) { ?>
                                                                    <li>- <a class="editable-click"  href="<?php echo base_url() ?>admin/center/center_single_view/<?php echo $row->center_id; ?>"><?php echo $row->center_name; ?></a></li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php }else{ echo "Centro no asignado";} ?>
                                                        <!--<a href="<?php #echo base_url() ?>admin/user/user_edit/<?php #echo $user->user_id; ?>">
                                                            <i class="fa fa-plus-square"></i> Agregar Centro
                                                        </a>-->
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $sql_professor_group = "SELECT * FROM `tbl_group_student` as tbl_gp LEFT JOIN `tbl_group` as tbl_g ON tbl_gp.group_id = tbl_g.group_id WHERE tbl_gp.user_id = ".$user->user_id;
                                                        $query_professor_group = $this->db->query($sql_professor_group);
                                                        if (!empty($query_professor_group->result())){
                                                        ?>
                                                            <ul>
                                                                <?php foreach ($query_professor_group->result() as $row) { ?>
                                                                	<li>∙ <a class="editable-click" title="Ver Grupo" href="<?php echo base_url(); ?>admin/group/group_edit_view/<?php echo $row->group_id; ?>"><?php echo $row->group_name ?></a></li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php }else{ echo "Grupo no asignado";} ?>
                                                        <!--<a href="<?php #echo base_url() ?>admin/user/user_edit/<?php #echo $user->user_id; ?>">
                                                            <i class="fa fa-plus-square"></i> Agregar Grupo
                                                        </a>-->
                                                    </td>
                                                    <td class="datatable-ct">
                                                        <button type="button" class="btn btn-primary" onclick="window.location='<?php echo base_url() ?>admin/user/user_edit_view/<?php echo $user->user_id; ?>'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Usuario</button>
                                                    </td>
                                                </tr>
                                            <?php $i++; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
    </div>

<?php $this->load->view('admin/z_footer'); ?>