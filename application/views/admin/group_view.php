<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div id="dropzone1" class="pro-ad">
                                        <form action="<?php echo base_url(); ?>admin/group/create_group" method="post">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <?php if (!empty($message)){ ?>
                                                        <div class="alert alert-success alert-st-one" role="alert">
                                                            <i class="fa fa-check edu-checked-pro admin-check-pro admin-check-pro-none" aria-hidden="true"></i>
                                                            <span class="message-mg-rt message-alert-none"><?php echo $message; ?></span>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                                                 
                                                    <div class="form-group">
                                                        <input name="group_name" type="text" class="form-control" placeholder="Grupo" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="chosen-select-single mg-b-20">
                                                        <select data-placeholder="Seleccionar Centro..." class="chosen-select" tabindex="-1" name="centers">
                                                            <option value="">Seleccionar Centro</option>
                                                            <?php foreach ($centers as $center){ ?>
                                                                <option value="<?php echo $center->center_id ?>"><?php echo $center->center_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="payment-adress" style="text-align: right;">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- LISTADO DE CENTROS -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Lista de Grupos</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                    <table id="table" data-toggle="table" data-pagination="false" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                            <tr>
                                                <th data-field="id">#</th>
                                                <th data-field="center" data-editable="false">Grupo</th>
                                                <th data-field="name" data-editable="false">Centro</th>
                                                <th data-field="action">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; foreach ($groups as $group){ ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $group->group_name; ?></td>
                                                    <?php
                                                    $sql = "SELECT * FROM `tbl_center` WHERE center_id = ".$group->center_id;
                                                    $query = $this->db->query($sql);
                                                    if (!empty($query->result())){
                                                        foreach ($query->result() as $row) {
                                                    ?>
                                                            <td>
                                                                <a class="editable-click" href="<?php echo base_url() ?>admin/center/center_single_view/<?php echo $center->center_id; ?>">
                                                                <?php echo $row->center_name; ?></a>
                                                            </td>
                                                        <?php } ?>
                                                    <?php }else{ ?>
                                                            <td><div class="alert alert-warning" role="alert" style="margin-bottom:0; padding: 5px 15px;"><strong>Grupo no asignados. Edite el grupo para asignarlo a un Centro</strong></div></td>
                                                    <?php } ?>
                                                    <td class="datatable-ct">
                                                        <button type="button" class="btn btn-primary" onclick="window.location='<?php echo base_url() ?>admin/group/group_edit_view/<?php echo $group->group_id; ?>'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Grupo</button>
                                                    </td>
                                                </tr>
                                            <?php $i++; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
        
        
    </div>

<?php $this->load->view('admin/z_footer'); ?>