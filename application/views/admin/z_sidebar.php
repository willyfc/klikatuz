<div class="left-sidebar-pro">
    <nav id="sidebar" class="">
        <div class="sidebar-header" style="text-align:left;">
            <a href="<?php echo base_url(); ?>admin/dashboard"><img style="width: 150px;padding-top: 8px;padding-left: 12px;" class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
            <!--<strong><a href="<?php echo base_url(); ?>"><img src="<?php #echo base_url(); ?>/assets/img/logo/logosn.png" alt="" /></a></strong>-->
        </div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar">
            <nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    <li class="active">
                        <a class="has-arrow" href="<?php echo base_url(); ?>admin/center">
                            <span class="educate-icon educate-home icon-wrap"></span>
                            <span class="mini-click-non">CENTROS</span>
                        </a>
                    </li>
                    
                    <li class="active">
                        <a class="has-arrow" href="<?php echo base_url(); ?>admin/group">
                            <span class="educate-icon educate-professor icon-wrap"></span>
                            <span class="mini-click-non">GRUPOS</span>
                        </a>
                        <!--<ul class="submenu-angle" aria-expanded="true">
                            <li><a title="Dashboard v.1" href="index.html"><span class="mini-sub-pro">Listar Grupos</span></a></li>
                            <li><a title="Dashboard v.2" href="index-1.html"><span class="mini-sub-pro">Nuevo Grupo</span></a></li>
                        </ul>-->
                    </li>
                    
                    <li class="active">
                        <a class="has-arrow" href="#">
                            <span class="educate-icon educate-student icon-wrap"></span>
                            <span class="mini-click-non">USUARIOS</span>
                        </a>
                        <ul class="submenu-angle" aria-expanded="true">
                            <li><a title="Administradores" href="<?php echo base_url() ?>admin/user"><span class="mini-sub-pro">Administradores</span></a></li>
                            <li><a title="Administrar Directores" href="<?php echo base_url() ?>admin/user/manager"><span class="mini-sub-pro">Directores</span></a></li>
                            <li><a title="Administrar Profesores" href="<?php echo base_url() ?>admin/user/professor"><span class="mini-sub-pro">Profesores</span></a></li>
                            <li><a title="Administrar Estudiantes" href="<?php echo base_url() ?>admin/user/student"><span class="mini-sub-pro">Estudiantes</span></a></li>
                        </ul>
                    </li>
                    
                    <li class="active">
                        <a class="has-arrow" href="<?php echo base_url(); ?>admin/lesson" title="Lecciones">
                            <span class="educate-icon educate-course icon-wrap"></span>
                            <span class="mini-click-non">LECCIONES</span>
                        </a>
                    </li>

                    <li class="active">
                        <a class="has-arrow" href="<?php echo base_url(); ?>admin/dictation" title="Lecciones">
                            <span class="educate-icon educate-course icon-wrap"></span>
                            <span class="mini-click-non">DICTADOS</span>
                        </a>
                    </li>

                    <li class="active">
                        <a class="has-arrow" href="<?php echo base_url(); ?>admin/excercise" title="Ejercicios">
                            <span class="educate-icon educate-course icon-wrap"></span>
                            <span class="mini-click-non">EJERCICIOS</span>
                        </a>
                    </li>

                    <li class="active">
                        <a class="has-arrow" href="<?php echo base_url(); ?>admin/additional_exercise" title="Ejercicios">
                            <span class="educate-icon educate-course icon-wrap"></span>
                            <span class="mini-click-non">E. ADICIONALES</span>
                        </a>
                    </li>
                    
                </ul>
            </nav>
        </div>
    </nav>
</div>