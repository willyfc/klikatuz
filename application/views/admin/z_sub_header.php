<div class="header-advance-area">
    <div class="header-top-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="header-top-wraper">
                        <div class="row">
                            <!--<div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                <div class="menu-switcher-pro">
                                    <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                        <i class="educate-icon educate-nav"></i>
                                    </button>
                                </div>
                            </div>-->
                            <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                <div class="header-right-info">
                                    <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                        <li class="nav-item">
                                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                <img src="img/product/pro4.jpg" alt="" />
                                                
                                                <span class="admin-name">
                                                    <?php
                                                    $session_data = $this->session->all_userdata();
                                                    echo $session_data["user_name"];
                                                    ?>
                                                </span>
                                                <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                                            </a>
                                            <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                <li><a href="<?php echo base_url(); ?>"><span class="edu-icon edu-settings author-log-ic">Mi cuenta</span></a>
                                                </li>
                                                <li><a href="<?php echo base_url(); ?>/login/logout"><span class="edu-icon edu-locked author-log-ic">Cerrar Sesión</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu start
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li
                                    <a class="has-arrow" href="#">
                                        <span class="educate-icon educate-home icon-wrap"></span>
                                        CENTRO
                                    </a>
                                </li>

                                <li
                                    <a data-toggle="collapse" data-target="#Pagemob" href="#">
                                        <span class="educate-icon educate-professor icon-wrap"></span>
                                        <span class="mini-click-non">GRUPOS</span>
                                    </a>
                                    <ul id="Pagemob" class="collapse dropdown-header-top">
                                        <li><a title="Dashboard v.1" href="index.html">Listar Grupos</a></li>
                                        <li><a title="Dashboard v.2" href="index-1.html">Nuevo Grupo/a></li>
                                    </ul>
                                </li>

                                <li
                                    <a data-toggle="collapse" data-target="#Pagemob" href="#">
                                        <span class="educate-icon educate-student icon-wrap"></span>
                                        <span class="mini-click-non">USUARIOS</span>
                                    </a>
                                    <ul id="Pagemob" class="collapse dropdown-header-top">
                                        <li><a title="Dashboard v.1" href="index.html">Listar Usuarios</a></li>
                                        <li><a title="Dashboard v.2" href="index-1.html">Nuevo Usuario</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Mobile Menu end -->
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h3 style="margin: 0;"><?php echo $title; ?></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>