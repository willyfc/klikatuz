<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <!-- FORM PARA CREAR UN NUEVO CENTRO -->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div id="dropzone1" class="pro-ad">
                                        <!-- LÍNEA DE FORMULARIO PARA CREAR UN SOLO CENTRO -->
                                        <!--<form action="<?php #echo base_url(); ?>admin/center/create_center" method="post">-->
                                        
                                        <form action="<?php echo base_url(); ?>admin/center/create_center" method="post" autocomplete="off">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <?php if (!empty($message)){ ?>
                                                        <div class="alert alert-success alert-st-one" role="alert">
                                                            <i class="fa fa-check edu-checked-pro admin-check-pro admin-check-pro-none" aria-hidden="true"></i>
                                                            <span class="message-mg-rt message-alert-none"><?php echo $message; ?></span>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                                                 
                                                    <div class="form-group">
                                                        <input name="center_name" type="text" class="form-control" placeholder="Centro">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="chosen-select-single mg-b-20">
                                                        <select data-placeholder="Asignar Director..." class="chosen-select" tabindex="-1" name="user_id">
                                                            <option value="">Asignar Director</option>
                                                            <?php foreach ($manager_list as $manager){ ?>
                                                                <option value="<?php echo $manager->user_id; ?>"><?php echo $manager->user_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="payment-adress" style="text-align: right;">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- LISTADO DE CENTROS -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Lista de Centros</h1>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                    <style>
                                        .custom-datatable-overright table tbody tr td{vertical-align: middle;}
                                    </style>
                                    <table id="table" data-toggle="table" data-pagination="false" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                            <tr>
                                                <th data-field="id">#</th>
                                                <th data-field="center" data-editable="false">Centro</th>
                                                <th data-field="name" data-editable="false">Director</th>
                                                <th data-field="grupos" data-editable="false">Grupos</th>
                                                <!--<th data-field="nro_estudiantes" data-editable="false">Total de Estudiantes</th>-->
                                                <th data-field="action">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; foreach ($center_list as $center){ ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $center->center_name; ?></td>
                                                    
                                                    <!-- MANAGER ASIGNADO -->
                                                    <td>
                                                        <?php
                                                        $sql = "SELECT * FROM `tbl_user` WHERE user_id = ".$center->manager_id;
                                                        $query = $this->db->query($sql);
                                                        if (!empty($query->result())){
                                                            foreach ($query->result() as $row) {
                                                            ?>
                                                                <?php echo $row->user_name ?>
                                                            <?php } ?>
                                                        <?php }else{ ?>
                                                                <div class="alert alert-warning" role="alert" style="margin-bottom:0; padding: 5px 15px;"><strong>Sin Director asignado.</strong> Edite el centro para asignar un Director</div>
                                                        <?php } ?>
                                                    </td>
                                                    
                                                    <!-- GRUPOS ASIGNADOS -->
                                                    <td>
                                                        <ul>
                                                        <?php
                                                            $sql_groups = "SELECT * FROM `tbl_group` WHERE center_id = ".$center->center_id;
                                                            $query_groups = $this->db->query($sql_groups);
                                                            if (!empty($query_groups->result())){
                                                                foreach ($query_groups->result() as $row) {
                                                                ?>
                                                            <li>∙ <a class="editable-click" title="Ver Grupo" href="<?php echo base_url(); ?>admin/group/group_edit_view/<?php echo $row->group_id; ?>"><?php echo $row->group_name ?></a></li>
                                                                <?php } ?>
                                                            <?php }else{ ?>
                                                                    <div class="alert alert-warning" role="alert" style="margin-bottom:0; padding: 5px 15px;"><strong>Sin Grupos asignados. Edite el centro para asignar Grupos</strong></div>
                                                            <?php } ?>
                                                        </ul>
                                                    </td>
                                                    
                                                    <!-- NRO DE ESTUDIANTES -->
                                                    <!--<td>
                                                        <?php
                                                        #$sql_students = "SELECT * FROM `tbl_student` WHERE center_id = ".$center->center_id;
                                                        #$query_students = $this->db->query($sql_students);
                                                        ?>
                                                        <strong><?php #echo $query_students->num_rows(); ?> </strong>Estudiante(s)                                               
                                                    </td>-->
                                                    <td class="datatable-ct">
                                                        <div style="text-align:center;">
                                                            <button title="Editar Centro" type="button" class="btn btn-primary" onclick="window.location='<?php echo base_url() ?>admin/center/center_edit_view/<?php echo $center->center_id; ?>'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                            
                                                            <button title="Ver Centro" type="button" class="btn btn-primary" onclick="window.location='<?php echo base_url() ?>admin/center/center_single_view/<?php echo $center->center_id; ?>'"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php $i++; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
    </div>

<?php $this->load->view('admin/z_footer'); ?>
