<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div id="dropzone1" class="pro-ad">
                                        <form action="<?php echo base_url(); ?>admin/group/group_edit" method="post">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <?php if (!empty($message)){ ?>
                                                        <div class="alert alert-success alert-st-one" role="alert">
                                                            <i class="fa fa-check edu-checked-pro admin-check-pro admin-check-pro-none" aria-hidden="true"></i>
                                                            <span class="message-mg-rt message-alert-none"><?php echo $message; ?></span>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                                                 
                                                    <div class="form-group">
                                                        <?php foreach ($groups as $group){ ?>
                                                            <input name="group_name" type="text" class="form-control" placeholder="Grupo" value="<?php echo $group->group_name; ?>" required>
                                                            <input type="hidden" name="group_id" value="<?php echo $group->group_id; ?>">
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <?php
                                                        $sql = "SELECT * FROM `tbl_center` WHERE center_id = ".$group->center_id;
                                                        $query = $this->db->query($sql);
                                                        if (!empty($query->result())){
                                                            foreach ($query->result() as $row) {
                                                            ?>
                                                                <div class="alert alert-warning" role="alert">
                                                                    <strong>Asignado a:</strong> <?php echo $row->center_name ?><br>
                                                                    Puede cambiar el Director Asignado en el siguiente campo. En caso de no modificarlo se mantendrá el ya asignado.
                                                                    <input type="hidden" name="center_actual_id" value="<?php echo $group->center_id; ?>">
                                                                </div>
                                                            <?php } ?>
                                                        <?php }else{ ?>
                                                                <div class="alert alert-warning" role="alert">Este Centro no está asignado a ningún centro. Puede asignarlo en el siguiente campo.</div>
                                                        <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                                    <div class="chosen-select-single mg-b-20">
                                                        <select data-placeholder="Seleccionar Centro..." class="chosen-select" tabindex="-1" name="center_new_id">
                                                            <option value="">Seleccionar Centro</option>
                                                            <?php foreach ($centers as $center){ ?>
                                                                <option value="<?php echo $center->center_id ?>"><?php echo $center->center_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                                    <div class="payment-adress" style="text-align: right;">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Asignaciones</h1>
                                </div>
                            </div>
                            
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                    <style>
                                        .custom-datatable-overright table tbody tr td{vertical-align: middle;}
                                    </style>
                                    <div class="row">
	                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		                                    <table id="table" data-toggle="table" data-pagination="false" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="false"
		                                        data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="false" data-toolbar="#toolbar">
		                                        <thead>
		                                            <tr>
		                                                <th data-field="lecciones" data-editable="false">Lecciones</th>
		                                            </tr>
		                                        </thead>
		                                        <tbody>
		                                            <?php
													$sql_asignaciones = "SELECT * FROM `tbl_lesson_group` LEFT JOIN tbl_lesson ON tbl_lesson_group.lesson_id = tbl_lesson.lesson_id WHERE tbl_lesson_group.group_id =".$group->group_id;
													$query_asignaciones = $this->db->query($sql_asignaciones);
													foreach ($query_asignaciones->result() as $row_asignaciones) { ?>
		                                                <tr>
		                                                    <td>
		                                                        <?php echo $row_asignaciones->lesson_name; ?>
		                                                    </td>
		                                                </tr>
		                                            <?php } ?>
		                                        </tbody>
		                                    </table>
		                                </div>
		                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		                                    <table id="table" data-toggle="table" data-pagination="false" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="false"
		                                        data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="false" data-toolbar="#toolbar">
		                                        <thead>
		                                            <tr>
		                                                <th data-field="ejercicios_ad" data-editable="false">Ejercicios Adicionales</th>
		                                            </tr>
		                                        </thead>
		                                        <tbody>                                            
		                                            <tr>
		                                                <?php
		                                                $sql_asignaciones_ea = "SELECT * FROM `tbl_additional_excercise_group` LEFT JOIN tbl_additional_excercise ON tbl_additional_excercise_group.aditional_excercise_id = tbl_additional_excercise.additional_excercise_id WHERE tbl_additional_excercise_group.group_id =".$group->group_id;
														$query_asignaciones_ea = $this->db->query($sql_asignaciones_ea);
														foreach ($query_asignaciones_ea->result() as $row_asignaciones_ea) {
														?>
		                                                    <td>
		                                                        <?php echo $row_asignaciones_ea->additional_excercise_name; ?>
		                                                    </td>
		                                                <?php } ?>
		                                            </tr>
		                                        </tbody>
		                                    </table>
		                                </div>
		                            </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Profesores</h1>
                                </div>
                            </div>
                            
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                    <style>
                                        .custom-datatable-overright table tbody tr td{vertical-align: middle;}
                                        .add_group_p{display: block; float: left;    padding-top: 10px;}
                                    </style>

									<?php
									foreach ($professors as $professor){
										$professors_ids[] = $professor->user_id;
									}
									?>
                                    <form class="add_group_p" action="<?php echo base_url(); ?>admin/group/add_group_professor" method="post">
                                    	<?php foreach ($groups as $group){ ?>
											<input type="hidden" name="group_id" value="<?php echo $group->group_id ?>">
										<?php } ?>
										<input type="hidden" name="center_id" value="<?php echo $row->center_id;  ?>">
										<div class="form-group" style="float: left;width: 190px;">
											<div class="chosen-select-single mg-b-20">
												<select data-placeholder="Asignar Grupo" class="chosen-select" tabindex="-1" name="asignar_grupo" required>
													<option value="">Asignar Profesor</option>
													<?php foreach ($professors_nolist as $p_nolist){ ?>
														<?php if (!in_array($p_nolist->user_id, $professors_ids)){ ?>
															<option value="<?php echo $p_nolist->user_id; ?>"><?php echo $p_nolist->user_name; ?></option>
														<?php } ?>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group" style="float: left;margin-left: 10px;">
											<div class="payment-adress" style="text-align: right;">
												<button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
											</div>
										</div>
									</form>

                                    <table id="table" data-toggle="table" data-pagination="false" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                            <tr>
                                                <th data-field="id">#</th>
                                                <th data-field="nom_profesor" data-editable="false">Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; foreach ($professors as $professor){ ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td>
                                                        <?php echo $professor->user_name; ?>
                                                    </td>
                                                </tr>
                                            <?php $i++; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Estudiantes</h1>
                                </div>
                            </div>
                            
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                    <style>
                                        .custom-datatable-overright table tbody tr td{vertical-align: middle;}
                                    </style>
                                    <?php
                                    foreach ($students as $student){
                                    	$students_ids[] = $student->user_id;
                                    }
                                    ?>
                                    <form class="add_group_p" action="<?php echo base_url(); ?>admin/group/add_group_student" method="post">
                                    	<?php foreach ($groups as $group){ ?>
											<input type="hidden" name="group_id" value="<?php echo $group->group_id ?>">
										<?php } ?>
										<input type="hidden" name="center_id" value="<?php echo $row->center_id;  ?>">
										<div class="form-group" style="float: left;width: 190px;">
											<div class="chosen-select-single mg-b-20">
												<select data-placeholder="Asignar Grupo" class="chosen-select" tabindex="-1" name="asignar_grupo" required>
													<option value="">Asignar Estudiante</option>
													<?php foreach ($students_nolist as $s_nolist){ ?>
														<?php if (!in_array($s_nolist->user_id, $students_ids)){ ?>
															<option value="<?php echo $s_nolist->user_id; ?>"><?php echo $s_nolist->user_name; ?></option>
														<?php } ?>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group" style="float: left;margin-left: 10px;">
											<div class="payment-adress" style="text-align: right;">
												<button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
											</div>
										</div>
									</form>
                                    <table id="table" data-toggle="table" data-pagination="false" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                            <tr>
                                                <th data-field="id">#</th>
                                                <th data-field="nom_estudiante" data-editable="false">Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; foreach ($students as $student){ ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td>
                                                        <?php echo $student->user_name; ?>
                                                    </td>
                                                </tr>
                                            <?php $i++; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        
    </div>

<?php $this->load->view('admin/z_footer'); ?>