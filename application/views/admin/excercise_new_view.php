<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <!-- LISTADO DE CENTROS -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <form action="<?php echo base_url(); ?>admin/excercise/create_dictation" method="post">
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <div class="sparkline13-list">
                                <div class="sparkline13-hd">
                                    <div class="main-sparkline13-hd">
                                        <!--<h1>Lista de Lecciones</h1>-->
                                    </div>
                                </div>
                                <div class="sparkline13-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="exercise_name" type="text" class="form-control" placeholder="Nombre del Dictado">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="chosen-select-single mg-b-20">
                                                    <select data-placeholder="Asignar a Lección..." class="chosen-select" tabindex="-1" name="lesson_id">
                                                        <option value="">Asignar Lección</option>
                                                        <?php foreach ($lessons as $lesson){ ?>
                                                            <option value="<?php echo $lesson->lesson_id; ?>"><?php echo $lesson->lesson_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="chosen-select-single mg-b-20">
                                                    <select data-placeholder="Asignar a Tipo..." class="chosen-select" tabindex="-1" name="type_id">
                                                        <option value="">Asignar a Tipo</option>
                                                        <?php foreach ($exercise_types as $exercise_type){ ?>
                                                            <option value="<?php echo $exercise_type->excercise_type_id; ?>"><?php echo $exercise_type->excercise_type_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <label>Idioma:</label>
                                                <div class="chosen-select-single mg-b-20">
                                                    <select data-placeholder="Asignar Idioma..." class="chosen-select" tabindex="-1" name="exercise_lang">
                                                        <option value="">Asignar Idioma</option>
                                                        <option value="es">Español</option>
                                                        <option value="eu">Euskera</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <label>Tiempo mínimo (minutos)</label>
                                                <input name="exercise_time" type="text" class="form-control">
                                                <br>
                                                <label>Tiempo máximo (minutos)</label>
                                                <input name="exercise_max_time" type="text" class="form-control">
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <label>Repeticiones</label>
                                                <input name="exercise_repetition" type="text" class="form-control">
                                                <br>
                                                <label style="display: block;">Visible para los alumnos?</label>
                                                <input name="exercise_visible" type="checkbox"> &nbsp;Marque la casilla para hacer visible el dictado.
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <label style="display: block;">Corregir?</label>
                                                <input name="exercise_correct" type="checkbox"> &nbsp;Marque la casilla para permitir corregir el dictado.
                                                <br>
                                                <label style="display: block;margin-top: 13px">Errores permitidos</label>
                                                <input name="exercise_max_errors" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="sparkline13-list">
                                <div class="sparkline13-hd">
                                    <div class="main-sparkline13-hd">
                                        <!--<h1>Lista de Lecciones</h1>-->
                                    </div>
                                </div>
                                <div class="sparkline13-graph">
                                    <label>Texto</label>
                                    <textarea class="form-control" name="exercise_content" style="height:350px;font-size:30px; letter-spacing: 10px;display: block;background: #fff;width: 100%;padding: 20px;"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <br>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="sparkline13-list">
                                <div class="sparkline13-hd">
                                    <div class="main-sparkline13-hd">
                                        <div class="payment-adress" style="text-align:center">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar nuevo Dictado</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
    </div>

<?php $this->load->view('admin/z_footer'); ?>