<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <!-- FORM PARA CREAR UN NUEVO CENTRO -->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div id="dropzone1" class="pro-ad">
                                        <!-- LÍNEA DE FORMULARIO PARA CREAR UN SOLO CENTRO -->
                                        <!--<form action="<?php #echo base_url(); ?>admin/center/create_center" method="post">-->
                                        <?php foreach ($users as $user){ ?>
                                            
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <form action="<?php echo base_url(); ?>admin/user/user_edit" method="post">
                                                            <input type="hidden" name="user_actual_id" value="<?php echo $user->user_id ?>">
                                                            <input type="hidden" name="user_actual_level" value="<?php echo $user->user_level ?>">
                                                            <div class="form-group">
                                                                <input name="user_name" type="text" class="form-control" placeholder="Nombre" value="<?php echo $user->user_name; ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <input name="user_nickname" type="text" class="form-control" placeholder="Nombre de Usuario" value="<?php echo $user->user_nickname; ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <input name="user_email" type="text" class="form-control" placeholder="Email" value="<?php echo $user->user_email; ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="alert alert-warning" role="alert">Si no desea modificar la contraseña dejé el campo en blanco.</div>
                                                                <input name="user_password" type="text" class="form-control" placeholder="Contraseña">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="payment-adress" style="text-align: right;">
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <form action="<?php echo base_url(); ?>admin/user/user_rol_edit" method="post">
                                                            <input type="hidden" name="user_actual_id" value="<?php echo $user->user_id ?>">
                                                            <input type="hidden" name="user_actual_level" value="<?php echo $user->user_level ?>">
                                                            <div class="form-group">
                                                                <div class="alert alert-warning" role="alert">El Rol actual de este usuario es de <strong><?php echo $rol ?></strong>, puede modificarlo en el siguiente campo<br><strong style="text-decoration: underline;">SE RECOMIENDA NO HACERLO</strong></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="chosen-select-single mg-b-20">
                                                                    <select data-placeholder="Modificar rol de usuario.." class="chosen-select" tabindex="-1" name="user_level_new" required>
                                                                        <option value="">Modificar rol de usuario</option>
                                                                        <option value="1">Administrador</option>
                                                                        <option value="2">Director</option>
                                                                        <option value="3">Profesor</option>
                                                                        <option value="4">Estudiante</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="payment-adress" style="text-align: right;">
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            
                                        <?php } ?>                                            
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!--   SI ES PROFESOR    -->
        <?php if ($user->user_level == 3){ ?>
            <div class="data-table-area mg-b-15">
                <div class="container-fluid">
                    <div class="row">

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="sparkline13-list">
								<div class="sparkline13-hd">
									<div class="main-sparkline13-hd">
										<h1>Lista de Centros</h1>
									</div>
								</div>

								<div class="sparkline13-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                        <table id="table" class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th data-field="id">#</th>
                                                    <th data-field="center" data-editable="false">Centro(s)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($centers_professors as $centers_professor){ ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td style="padding-right: 20px !important;">
                                                            <?php
                                                            if (!empty($centers_professor)){
                                                            	echo $centers_professor->center_name;
                                                            }else{ ?>
                                                            Centro no asignado.
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                	<?php
                                                	$centes_ids[] = $centers_professor->center_id;
                                                	$i++;
                                            	}
                                            	?>
                                            </tbody>
                                        </table>
                                        <form action="<?php echo base_url(); ?>admin/user/add_center_professor" method="post">
                                            <input type="hidden" name="user_actual_id" value="<?php echo $user->user_id ?>">
                                            <div class="form-group">
                                                <div class="chosen-select-single mg-b-20">
                                                    <select data-placeholder="Asignar Centro" class="chosen-select" tabindex="-1" name="asignar_centro" required>
                                                        <option value="">Asignar Centro</option>
                                                        <?php
														$sql_centers = "SELECT * FROM `tbl_center`";
														$query_center = $this->db->query($sql_centers);
														?>
														<?php foreach ($query_center->result() as $center){ ?>
															<?php if (!in_array($center->center_id, $centes_ids)){ ?>
																<option value="<?php echo $center->center_id; ?>"><?php echo $center->center_name; ?></option>
															<?php } ?>
														<?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="payment-adress" style="text-align: right;">
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

							</div>
						</div>


                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="sparkline13-list">
                                <div class="sparkline13-hd">
                                    <div class="main-sparkline13-hd">
                                        <h1>Lista de Grupos por Centros</h1>
                                    </div>
                                </div>
                                <div class="sparkline13-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                        <table id="table" class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th data-field="id">#</th>
                                                    <th data-field="center" data-editable="false">Centro(s)</th>
                                                    <th data-field="grupo" data-editable="false">Grupo(s)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $center_validate = $centers_professors[0]->center_id; ?>
                                                <?php if (!empty($center_validate)){ ?>
                                                    <?php $i = 1; foreach ($centers_professors as $centers_professor){ ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td style="padding-right: 20px !important;">
                                                                <?php echo $centers_professor->center_name; ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $sql_professor_group = "SELECT * FROM `tbl_group_professor` as tbl_gp LEFT JOIN `tbl_group` as tbl_g ON tbl_gp.group_id = tbl_g.group_id WHERE tbl_gp.user_id = ".$user->user_id." AND tbl_gp.center_id = ".$centers_professor->center_id;
                                                                $query_professor_group = $this->db->query($sql_professor_group);
                                                                #echo "<pre>"; print_r($query_student_group->result()); echo "</pre>";
                                                                if (!empty($query_professor_group->result())){
                                                                ?>
                                                                    <ul style="margin-bottom: 20px">
                                                                        <?php foreach ($query_professor_group->result() as $row) { ?>
                                                                            <li>- <?php echo $row->group_name; ?></li>
                                                                            <?php $groups_ids[] = $row->group_id; ?>
                                                                        <?php } ?>
                                                                    </ul>
                                                                    <form action="<?php echo base_url(); ?>admin/user/add_group_professor" method="post">
                                                                        <input type="hidden" name="user_actual_id" value="<?php echo $user->user_id ?>">
                                                                        <input type="hidden" name="user_center_id" value="<?php echo $centers_professor->center_id ?>">
                                                                        <div class="form-group">
                                                                            <div class="chosen-select-single mg-b-20">
                                                                                <select data-placeholder="Asignar Grupo" class="chosen-select" tabindex="-1" name="asignar_grupo" required>
                                                                                    <option value="">Asignar Grupo</option>
                                                                                    <?php
                                                                                    $sql_groups = "SELECT * FROM `tbl_group` WHERE center_id = ".$centers_professor->center_id;
                                                                                    $query_groups = $this->db->query($sql_groups);
                                                                                    ?>
                                                                                    <?php foreach ($query_groups->result() as $group){ ?>
                                                                                    	<?php if (!in_array($group->group_id, $groups_ids)){ ?>
                                                                                        	<option value="<?php echo $group->group_id; ?>"><?php echo $group->group_name; ?></option>
                                                                                    	<?php } ?>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="payment-adress" style="text-align: right;">
                                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                <?php }else{ ?>
                                                                    <?php if (!empty($centers_professor->center_id )){ ?>    
                                                                        Grupo no asignado.<br>
                                                                        <form action="<?php echo base_url(); ?>admin/user/add_group_professor" method="post">
                                                                            <input type="hidden" name="user_actual_id" value="<?php echo $user->user_id ?>">
                                                                            <input type="hidden" name="user_center_id" value="<?php echo $centers_professor->center_id ?>">
                                                                            <div class="form-group">
                                                                                <div class="chosen-select-single mg-b-20">
                                                                                    <select data-placeholder="Asignar Grupo" class="chosen-select" tabindex="-1" name="asignar_grupo" required>
                                                                                        <option value="">Asignar Grupo</option>
                                                                                        <?php
                                                                                        $sql_groups = "SELECT * FROM `tbl_group` WHERE center_id = ".$centers_professor->center_id ;
                                                                                        $query_groups = $this->db->query($sql_groups);
                                                                                        ?>
                                                                                        <?php foreach ($query_groups->result() as $group){ ?>
                                                                                            <option value="<?php echo $group->group_id; ?>"><?php echo $group->group_name; ?></option>
                                                                                        <?php } ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="payment-adress" style="text-align: right;">
                                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                    <?php $i++; } ?>
                                                    <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>                    
                    </div>
                </div>
            </div>
        <?php } ?>

        
        <!--   SI ES ESTUDIANTE    -->
        <?php if ($user->user_level == 4){ ?>
            <div class="data-table-area mg-b-15">
                <div class="container-fluid">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="sparkline13-list">
                                <div class="sparkline13-hd">
                                    <div class="main-sparkline13-hd">
                                        <h1>Centro y Grupos</h1>
                                    </div>
                                </div>
                                <div class="sparkline13-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                        <table id="table" class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th data-field="id">#</th>
                                                    <th data-field="center" data-editable="false">Centro</th>
                                                    <th data-field="grupo" data-editable="false">Grupo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1; foreach ($centers_professors as $centers_professor){ ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td style="padding-right: 20px !important;">
                                                            <?php
                                                            $sql = "SELECT * FROM `tbl_center_student` as tbl_ce LEFT JOIN `tbl_center` as tbl_c ON tbl_ce.center_id = tbl_c.center_id WHERE tbl_ce.user_id = ".$user->user_id;
                                                            $query = $this->db->query($sql);
                                                            if (!empty($query->result())){
                                                            ?>
                                                                <ul>
                                                                    <?php foreach ($query->result() as $row) { ?>
                                                                        <?php $center_id = $row->center_id; ?>
                                                                        <li>- <?php echo $row->center_name; ?></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            <?php }else{ ?>
                                                            Centro no asignado.<br><br>
                                                                <form action="<?php echo base_url(); ?>admin/user/add_center_student" method="post">
                                                                    <input type="hidden" name="user_actual_id" value="<?php echo $user->user_id ?>">
                                                                    <div class="form-group">
                                                                        <div class="chosen-select-single mg-b-20">
                                                                            <select data-placeholder="Asignar Centro" class="chosen-select" tabindex="-1" name="asignar_centro" required>
                                                                                <option value="">Asignar Centro</option>
                                                                                <?php foreach ($centers as $center){ ?>
                                                                                    <option value="<?php echo $center->center_id; ?><"><?php echo $center->center_name; ?></option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="payment-adress" style="text-align: right;">
                                                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            <?php } ?>
                                                            <!--<a href="<?php #echo base_url() ?>admin/user/user_edit/<?php #echo $user->user_id; ?>">
                                                                <i class="fa fa-plus-square"></i> Agregar Centro
                                                            </a>-->
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $sql_student_group = "SELECT * FROM `tbl_group_student` as tbl_gp LEFT JOIN `tbl_group` as tbl_g ON tbl_gp.group_id = tbl_g.group_id WHERE tbl_gp.user_id = ".$user->user_id;
                                                            $query_student_group = $this->db->query($sql_student_group);
                                                            if (!empty($query_student_group->result())){
                                                            ?>
                                                                <ul style="margin-bottom: 20px">
                                                                    <?php foreach ($query_student_group->result() as $row) { ?>
                                                                        <li>- <?php echo $row->group_name; ?></li>
                                                                        <?php $groups_ids_std[] = $row->group_id; ?>
                                                                    <?php } ?>
                                                                </ul>
                                                                <form action="<?php echo base_url(); ?>admin/user/add_group_student" method="post">
                                                                    <input type="hidden" name="user_actual_id" value="<?php echo $user->user_id ?>">
                                                                    <input type="hidden" name="user_center_id" value="<?php echo $center_id ?>">
                                                                    <div class="form-group">
                                                                        <div class="chosen-select-single mg-b-20">
                                                                            <select data-placeholder="Asignar Grupo" class="chosen-select" tabindex="-1" name="asignar_grupo" required>
                                                                                <option value="">Asignar Grupo</option>
                                                                                <?php
                                                                                $sql_groups = "SELECT * FROM `tbl_group` WHERE center_id = ".$center_id;
                                                                                $query_groups = $this->db->query($sql_groups);
                                                                                ?>
                                                                                <?php foreach ($query_groups->result() as $group){ ?>
                                                                                	<?php if (!in_array($group->group_id, $groups_ids_std)){ ?>
                                                                                 	   <option value="<?php echo $group->group_id; ?>"><?php echo $group->group_name; ?></option>
                                                                                 	<?php } ?>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="payment-adress" style="text-align: right;">
                                                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            <?php }else{ ?>
                                                                <?php if (!empty($center_id)){ ?>    
                                                                    Grupo no asignado.<br>
                                                                    <form action="<?php echo base_url(); ?>admin/user/add_group_student" method="post">
                                                                        <input type="hidden" name="user_actual_id" value="<?php echo $user->user_id ?>">
                                                                        <input type="hidden" name="user_center_id" value="<?php echo $center_id ?>">
                                                                        <div class="form-group">
                                                                            <div class="chosen-select-single mg-b-20">
                                                                                <select data-placeholder="Asignar Grupo" class="chosen-select" tabindex="-1" name="asignar_grupo" required>
                                                                                    <option value="">Asignar Grupo</option>
                                                                                    <?php
                                                                                    $sql_groups = "SELECT * FROM `tbl_group` WHERE center_id = ".$center_id;
                                                                                    $query_groups = $this->db->query($sql_groups);
                                                                                    ?>
                                                                                    <?php foreach ($query_groups->result() as $group){ ?>
                                                                                        <option value="<?php echo $group->group_id; ?>"><?php echo $group->group_name; ?></option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="payment-adress" style="text-align: right;">
                                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                <?php } ?>v>
                                                                </form>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php $i++; } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>                    
                    </div>
                </div>
            </div>
        <?php } ?>
        
    </div>

<?php $this->load->view('admin/z_footer'); ?>