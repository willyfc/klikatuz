<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>

		<div class="single-pro-review-area mt-t-30 mg-b-15">
			<div class="container-fluid">
		        <div class="row">
		        	<form action="<?php echo base_url(); ?>/admin/lesson/filter_by_center" method="post">
		        	   	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			        		<h4>Seleccionar Centro</h4>
			        	</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="chosen-select-single mg-b-20">
								<select data-placeholder="Seleccionar Centro..." class="chosen-select" tabindex="-1" name="center_select">
									<option value="">Seleccionar Centro</option>
									<?php foreach ($center_list as $center){ ?>
										<option value="<?php echo $center->center_id; ?>"><?php echo $center->center_name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
							<div class="payment-adress" style="text-align: left;">
								<button type="submit" class="btn btn-primary waves-effect waves-light">Cargar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<?php if (isset($groups_list)){ ?>
        <!-- LISTADO DE CENTROS -->
	        <div class="data-table-area mg-b-15">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
	                        <div class="sparkline13-list">
	                            <div class="sparkline13-hd">
	                                <div class="main-sparkline13-hd">
	                                    <h1>Lista de Grupos - Centro: <?php echo $get_center_byid[0]->center_name; ?></h1>
	                                </div>
	                            </div>
	                            <div class="sparkline13-graph">
	                                <div class="datatable-dashv1-list custom-datatable-overright">
	                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
	                                    <style>
	                                        .custom-datatable-overright table tbody tr td{vertical-align: middle;}
	                                    </style>
	                                    <table id="table" data-toggle="table" data-pagination="false" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
	                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
	                                        <thead>
	                                            <tr>
	                                                <th data-field="id">#</th>
	                                                <th data-field="group" data-editable="false">Grupo</th>
	                                                <th data-field="action">Acciones</th>
	                                            </tr>
	                                        </thead>
	                                        <tbody>
	                                            <?php $i = 1; foreach ($groups_list as $group){ ?>
	                                                <tr>
	                                                    <td><?php echo $i; ?></td>
	                                                    <td><?php echo $group->group_name; ?></td>
	                                                    <td class="datatable-ct">
	                                                        <div style="text-align:center;">
	                                                            <button title="Editar Centro" type="button" class="btn btn-primary" onclick="window.location='<?php echo base_url() ?>admin/center/center_edit_view/<?php echo $center->center_id; ?>'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
	                                                            
	                                                            <button title="Ver Centro" type="button" class="btn btn-primary" onclick="window.location='<?php echo base_url() ?>admin/center/center_single_view/<?php echo $center->center_id; ?>'"><i class="fa fa-eye" aria-hidden="true"></i></button>
	                                                        </div>
	                                                    </td>
	                                                </tr>
	                                            <?php $i++; } ?>
	                                        </tbody>
	                                    </table>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
        <!-- Static Table End -->
    	<?php } ?>
    </div>

<?php $this->load->view('admin/z_footer'); ?>
