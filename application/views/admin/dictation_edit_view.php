<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>

<!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <!-- LISTADO DE CENTROS -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                	<form action="<?php echo base_url(); ?>admin/dictation/dictation_edit" method="post">
	                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
	                        <div class="sparkline13-list">
	                            <div class="sparkline13-hd">
	                                <div class="main-sparkline13-hd">
	                                    <!--<h1>Lista de Lecciones</h1>-->
	                                </div>
	                            </div>
	                            <div class="sparkline13-graph">
	                                <div class="datatable-dashv1-list custom-datatable-overright">
	                                    <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
                                    	<input type="hidden" name="dictation_id" value="<?php echo $d_id ?>">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input name="exercise_name" type="text" class="form-control" placeholder="Nombre del Dictado" value="<?php echo $dictations[0]->exercise_name; ?>">
                                                </div>
                                            </div>
                                        </div>
                                       	<div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            	<div class="alert alert-warning" role="alert">
                                            		<strong>Lección Asignada:</strong>
                                            		<?php
														$lesson_belong = "SELECT tbl_lesson.lesson_id, tbl_lesson.lesson_name FROM tbl_exercise LEFT JOIN tbl_lesson ON tbl_exercise.lesson_id = tbl_lesson.lesson_id WHERE tbl_lesson.lesson_id = ". $dictations[0]->lesson_id." Limit 1";
														$query_belong = $this->db->query($lesson_belong);
														if (!empty($query_belong->result())){
															foreach ($query_belong->result() as $row) {
                                                        ?>
															<a class="editable-click" href="<?php echo base_url(); ?>admin/lesson/lesson_single_view/<?php echo $row->lesson_id; ?>" title=""><?php echo $row->lesson_name; ?></a><br>
                                                        <?php
															}
														}else{
															echo "Sin asignar<br>";
														}
														?>
														Puede cambiar la asignación en el siguiente campo, de lo contrario se mantendrá el ya existente.
                                                    <input type="hidden" name="lesson_actual_id" value="<?php echo $dictations[0]->lesson_id; ?>">
                                                </div>
                                                <div class="chosen-select-single mg-b-20">
                                                    <select data-placeholder="Asignar a Lección..." class="chosen-select" tabindex="-1" name="lesson_id">
                                                        <option value="">Asignar a Lección</option>
                                                        <?php foreach ($lessons as $lesson){ ?>
                                                            <option value="<?php echo $lesson->lesson_id; ?>"><?php echo $lesson->lesson_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        
										<div class="row">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<label>Idioma: <span style="font-weight: normal;"><?php echo ($dictations[0]->exercise_lang == "es") ? "Español" : "Euskera"; ?></span></label>
												<input type="hidden" name="actual_exercise_lang" value="<?php echo $dictations[0]->exercise_lang; ?>">
												<div class="chosen-select-single mg-b-20">
													<select data-placeholder="Asignar Idioma..." class="chosen-select" tabindex="-1" name="exercise_lang">
														<option value="">Asignar Idioma</option>
														<option value="es">Español</option>
														<option value="eu">Euskera</option>
													</select>
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<label>Tiempo mínimo (minutos)</label>
												<input name="exercise_time" type="text" class="form-control" value="<?php echo $dictations[0]->exercise_time; ?>">
												<br>
												<label>Tiempo máximo (minutos)</label>
												<input name="exercise_max_time" type="text" class="form-control" value="<?php echo $dictations[0]->exercise_max_time; ?>">
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<label>Repeticiones</label>
												<input name="exercise_repetition" type="text" class="form-control" value="<?php echo $dictations[0]->exercise_repetition; ?>">
												<br>
												<label style="display: block;">Visible para los alumnos?</label>
												<input name="exercise_visible" type="checkbox" <?php echo ($dictations[0]->exercise_visible == 1) ? "checked" : ""; ?>> &nbsp;Marque la casilla para hacer visible el dictado.
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<label style="display: block;">Corregir?</label>
												<input name="exercise_correct" type="checkbox" <?php echo ($dictations[0]->exercise_correct == 1) ? "checked" : ""; ?>> &nbsp;Marque la casilla para permitir corregir el dictado.
												<br>
												<label style="display: block;margin-top: 13px">Errores permitidos</label>
												<input name="exercise_max_errors" type="text" class="form-control" value="<?php echo $dictations[0]->exercise_max_errors; ?>">
											</div>
										</div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
							<div class="sparkline13-list">
								<div class="sparkline13-hd">
									<div class="main-sparkline13-hd">
										<!--<h1>Lista de Lecciones</h1>-->
									</div>
								</div>
								<div class="sparkline13-graph">
									<label>Texto</label>
									<?php
									$exercise_content = "SELECT tbl_exercise_content.exercise_content_id,tbl_exercise_content.exercise_content_text FROM `tbl_exercise_content` LEFT JOIN tbl_exercise ON tbl_exercise_content.exercise_content_id = tbl_exercise.exercise_id WHERE tbl_exercise_content.exercise_content_id = '". $dictations[0]->exercise_content."' AND tbl_exercise_content.exercise_content_lang = '".$dictations[0]->exercise_lang."' Limit 1";
									$query_content = $this->db->query($exercise_content);
									if (!empty($query_content->result())){
										foreach ($query_content->result() as $rows) {
									?>
										<input type="hidden" name="exercise_content_id" value="<?php echo $rows->exercise_content_id ?>">
										<textarea class="form-control" name="exercise_content_text" style="height:350px;font-size:30px; letter-spacing: 10px;display: block;background: #fff;width: 100%;padding: 20px;"><?php echo $rows->exercise_content_text; ?></textarea>
									<?php
										}
									}else{ ?>
										<input type="hidden" name="exercise_content_id" value="<?php echo $rows->exercise_content_id ?>">
										<textarea class="form-control" name="exercise_content_text" style="height:350px;font-size:30px; letter-spacing: 10px;display: block;background: #fff;width: 100%;padding: 20px;"></textarea>
									<?php } ?>
								</div>
	                    	</div>
	                	</div>

						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<br>
							</div>
						</div>

	                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="sparkline13-list">
								<div class="sparkline13-hd">
									<div class="main-sparkline13-hd">
										<div class="payment-adress" style="text-align:center">
											<button type="submit" class="btn btn-primary waves-effect waves-light">Actualizar Dictado</button>
										</div>
									</div>
								</div>
							</div>
						</div>

                	</form>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
    </div>

<?php $this->load->view('admin/z_footer'); ?>