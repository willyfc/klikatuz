<!-- jquery
		============================================ -->
    
    <!-- bootstrap JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/metisMenu/metisMenu-active.js"></script>
    <!-- touchspin JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/touchspin/touchspin-active.js"></script>
    <!-- colorpicker JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/colorpicker/jquery.spectrum.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/colorpicker/color-picker-active.js"></script>
    <!-- data table JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/data-table/bootstrap-table.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/data-table/tableExport.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/data-table/data-table-active.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/data-table/bootstrap-table-editable.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/data-table/bootstrap-editable.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/data-table/bootstrap-table-resizable.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/data-table/colResizable-1.5.source.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/data-table/bootstrap-table-export.js"></script>
    <!--  editable JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/editable/jquery.mockjax.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/editable/mock-active.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/editable/select2.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/editable/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/editable/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/editable/bootstrap-editable.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/editable/xediable-active.js"></script>
    <!-- chosen JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/chosen/chosen.jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chosen/chosen-active.js"></script>
    <!-- select2 JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/select2/select2.full.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/select2/select2-active.js"></script>
    <!-- Chart JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/chart/jquery.peity.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/peity/peity-active.js"></script>
    <!-- summernote JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/summernote/summernote-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/tab.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html>