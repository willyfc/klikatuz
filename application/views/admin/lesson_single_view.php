<?php $this->load->view('admin/z_header'); ?>

<?php $this->load->view('admin/z_sidebar'); ?>
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo base_url(); ?>/assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
                
        <?php $this->load->view('admin/z_sub_header'); ?>
        
        <div class="container-fluid">
            <div class="row">
            
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-sales-chart">
						<h4 style="margin-bottom:20px">Lección: <?php echo $lesson[0]->lesson_name; ?></h4>
						<hr>
						<?php if (!empty($lesson[0]->media1) || !empty($lesson[0]->media2)){ ?>
							<h5>Multimedia</h5>
							<video width="100%" controls>
								<source src="<?php echo base_url(); ?><?php echo $lesson[0]->media2;?>.ogv" type="video/ogg">
								<!--<source src="<?php #echo base_url(); ?><?php #echo $lesson[0]->media1;?>.mp4" type="video/mp4">-->
								Your browser does not support HTML5 video.
							</video>
							<hr>
						<?php } ?>

						<?php #if (!empty($lesson[0]->lesson_detail_content)){ ?>
							<!--<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<h5 style=" margin-top: 30px;">Ejercicio</h5>
									<div class="tinymce-single" style="padding-left: 0px;">
										<div style="width: 100%;font-size:30px;letter-spacing: 10px;padding: 20px;"><?php #echo $lesson[0]->lesson_detail_content; ?></div>
									</div>
								</div>
							</div>
							<hr>-->
						<?php #}else{
							#echo "<strong>Esta lección no contiene ningún Ejercicio.</strong>";
							#echo "<hr>";
						#} ?>

						<?php if (!empty($lesson[0]->lesson_time)){ ?>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<h5 style=" margin-top: 30px;">Tiempo para el Ejercicio: <span style="font-weight: normal;"><?php echo $lesson[0]->lesson_time; ?> segundos</span></h5>
									<h5 style=" margin-top: 30px;">Tiempo máximo para el Ejercicio: <span style="font-weight: normal;">
										<?php echo (!empty($lesson[0]->lesson_max_time)) ? $lesson[0]->lesson_max_time." segundos" : "No asignado";  ?>
									</span></h5>
								</div>
							</div>
						<?php } ?>
                    </div>
                </div>
			</div>
			<div class="row" style="margin-top:30px">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="product-sales-chart">
                        <h4>Grupos asignados a esta lección</h4>
                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <!--<div id="toolbar"><select class="form-control dt-tb"><option value="">Export Basic</option><option value="all">Export All</option><option value="selected">Export Selected</option></select></div>-->
								<style>
								.custom-datatable-overright table tbody tr td{vertical-align: middle;}
								.add_group_p{display: block; float: left;padding-top: 10px;}
								</style>
								<form class="add_group_p" action="<?php echo base_url(); ?>admin/lesson/add_lesson_group" method="post">
									<input type="hidden" name="lesson_id" value="<?php echo $lesson[0]->lesson_id; ?>">
									<div class="form-group" style="float: left;width: 280px;">
										<div class="chosen-select-single mg-b-20">
											<select data-placeholder="Asignar Grupo" class="chosen-select" tabindex="-1" name="asignar_grupo" required>
												<option value="">Asignar Grupo</option>
												<?php foreach ($groups_list as $group_list){ ?>
													<option value="<?php echo $group_list->group_id; ?>"><?php echo $group_list->group_name; ?>&nbsp;&nbsp;/&nbsp;&nbsp;<?php echo $group_list->center_name; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="form-group" style="float: left;margin-left: 10px;">
										<div class="payment-adress" style="text-align: right;">
											<button type="submit" class="btn btn-primary waves-effect waves-light">Guardar</button>
										</div>
									</div>
								</form>
                                <table id="table" data-toggle="table" data-pagination="false" data-search="false" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
                                    data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                        <tr>
                                            <th data-field="id">#</th>
                                            <th data-field="grupos" data-editable="false">Grupo</th>
                                            <th data-field="centro" data-editable="false">Centro</th>
                                            <th data-field="nro" data-editable="false"># de Estudiantes Asignados</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										$sql_groups = "SELECT * FROM `tbl_lesson_group` LEFT JOIN tbl_group ON tbl_lesson_group.group_id = tbl_group.group_id  WHERE tbl_lesson_group.lesson_id = ".$l_id;
										$query_groups = $this->db->query($sql_groups);
										if (!empty($query_groups->result())){
											$i = 1;
											foreach ($query_groups->result() as $row) {
                                        ?>
												<tr>
													<td><?php echo $i; ?></td>
													<td>
														<a class="editable-click" href="<?php echo base_url(); ?>admin/group/group_edit_view/<?php echo $row->group_id; ?>" title="<?php echo $row->group_name; ?>">
															<?php echo $row->group_name; ?>
														</a>
													</td>
													<td>
														<?php
														$sql_center = "SELECT * FROM `tbl_group` LEFT JOIN tbl_center ON tbl_group.center_id = tbl_center.center_id WHERE tbl_group.group_id =".$row->group_id;
														$query_centers = $this->db->query($sql_center);
														foreach ($query_centers->result() as $query_center) {
														?>
															<?php echo $query_center->center_name; ?>
														<?php } ?>
													</td>
													<td>
														<?php
														$sql_count = "SELECT COUNT(*) as nro_students FROM `tbl_lesson_group_student` WHERE group_id = ".$row->group_id." AND lesson_id =".$l_id;
														$query_counts = $this->db->query($sql_count);
														foreach ($query_counts->result() as $query_count) {
															echo $query_count->nro_students;
														}
														
														?>
													</td>
												</tr>
                                        	<?php $i++; } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="product-sales-chart">
						<h4>Dictados asignados a este lección</h4>
						<div class="sparkline13-graph">
							<div class="datatable-dashv1-list custom-datatable-overright">

								<table id="table" data-toggle="table" data-pagination="false" data-search="false" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
                                    data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                        <tr>
                                            <th data-field="id">#</th>
                                            <th data-field="dictado" data-editable="false">Dictado</th>
                                            <th data-field="Idioma" data-editable="false">Idioma</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										$sql_dictation = "SELECT tbl_exercise.exercise_id,tbl_exercise.exercise_name,tbl_exercise.exercise_lang FROM tbl_exercise LEFT JOIN tbl_lesson ON tbl_exercise.lesson_id = tbl_lesson.lesson_id WHERE tbl_exercise.excercise_type_id = 4 AND tbl_lesson.lesson_id = ".$l_id." ORDER BY tbl_exercise.position_order ASC";
										$query_dictation = $this->db->query($sql_dictation);
										if (!empty($query_dictation->result())){
											$i = 1;
											foreach ($query_dictation->result() as $row) {
                                        ?>
												<tr>
													<td><?php echo $i; ?></td>
													<td>
														<a class="editable-click" href="<?php echo base_url(); ?>admin/dictation/dictation_edit_view/<?php echo $row->exercise_id; ?>" title="<?php echo $row->exercise_name; ?>">
															<?php echo $row->exercise_name; ?>
														</a>
													</td>
													<td>
														<?php if ($row->exercise_lang == "es"){echo "Español";}else{echo "Euskera";} ?>
													</td>
												</tr>
                                        	<?php $i++; } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="product-sales-chart">
						<h4>Ejercicios asignados a este lección</h4>
						<div class="sparkline13-graph">
							<div class="datatable-dashv1-list custom-datatable-overright">

								<table id="table" data-toggle="table" data-pagination="false" data-search="false" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="true"
                                    data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                        <tr>
                                            <th data-field="id">#</th>
                                            <th data-field="ejercicio" data-editable="false">Ejercicio</th>
                                            <th data-field="Idioma" data-editable="false">Idioma</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										$sql_exercise = "SELECT tbl_exercise.exercise_id,tbl_exercise.exercise_name,tbl_exercise.exercise_lang FROM tbl_exercise LEFT JOIN tbl_lesson ON tbl_exercise.lesson_id = tbl_lesson.lesson_id WHERE tbl_exercise.excercise_type_id = 1 AND tbl_lesson.lesson_id = ".$l_id." ORDER BY tbl_exercise.position_order ASC";
										$query_exercise = $this->db->query($sql_exercise);
										if (!empty($query_exercise->result())){
											$i = 1;
											foreach ($query_exercise->result() as $row) {
                                        ?>
												<tr>
													<td><?php echo $i; ?></td>
													<td>
														<a class="editable-click" href="<?php echo base_url(); ?>admin/exercise/exercise_edit_view/<?php echo $row->exercise_id; ?>" title="<?php echo $row->exercise_name; ?>">
															<?php echo $row->exercise_name; ?>
														</a>
													</td>
													<td>
														<?php if ($row->exercise_lang == "es"){echo "Español";}else{echo "Euskera";} ?>
													</td>
												</tr>
                                        	<?php $i++; } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
							</div>
						</div>
					</div>
				</div>
                
            </div>
        </div>
        
    </div>
<?php $this->load->view('admin/z_footer'); ?>
