<?php
class Center_model extends CI_Model{
    /*
     * TTODOS LOS CENTROS
     */
    public function get_center() {
        $this->db->select("*");
        $this->db->from('tbl_center');
        $query = $this->db->get();
        return $query->result();
    }
    public function create_center($data){
        $this->db->insert('tbl_center',$data);
        return $this->db->insert_id();
    }
    function update_center($id, $data){
        $this->db->where('center_id', $id);
        $this->db->update('tbl_center', $data);
    }
    
    /*
     * Listado de Todos los Directores
     */
    function get_managers() {
        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('user_level', '2');
        $query = $this->db->get();
        return $query->result();
    }
    /*
     * Obtener un solo director
     */
    function get_manager($center_id) {
        $this->db->select("tbl_user.user_name");
        $this->db->from('tbl_center');
        $this->db->join('tbl_user', 'tbl_center.manager_id = tbl_user.user_id', 'left');
        $this->db->where('tbl_center.center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    
    /*
     * Un centro
     */
    function get_one_center($center_id){
        $this->db->select("*");
        $this->db->from('tbl_center');        
        $this->db->where('center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Grupos que pertenecen a un centro
     * Utilizada en vista de edición de un centro
     */
    function get_center_groups($center_id){
        $this->db->select("*");
        $this->db->from('tbl_group');        
        $this->db->where('center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    
}
