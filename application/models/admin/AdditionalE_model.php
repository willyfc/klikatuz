<?php
class AdditionalE_model extends CI_Model{
    
    public function get_ae() {
        $this->db->select("*");
        $this->db->from('tbl_additional_excercise');
        $this->db->where('additional_excercise_lang', "es");
        $this->db->order_by('position_order', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function eu() {
        $this->db->select("*");
        $this->db->from('tbl_additional_excercise');
        $this->db->where('additional_excercise_lang', "eu");
        $this->db->order_by('position_order', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * TTODOS LOS CENTROS
     */
    public function get_center() {
        $this->db->select("*");
        $this->db->from('tbl_center');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_groups() {
        $this->db->select("*");
        $this->db->from('tbl_group');
        $query = $this->db->get();
        return $query->result();
    }
    public function create_additional_excersice($data){
        $this->db->insert('tbl_additional_excercise',$data);
        return $this->db->insert_id();
    }
    public function create_additional_excersice_detail($data){
        $this->db->insert('tbl_additional_excercise_detail',$data);
        return $this->db->insert_id();
    }
    function update_center($id, $data){
        $this->db->where('center_id', $id);
        $this->db->update('tbl_center', $data);
    }
    
    /*
     * Listado de Todos los Directores
     */
    function get_managers() {
        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('user_level', '2');
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Un centro
     */
    function get_one_center($center_id){
        $this->db->select("*");
        $this->db->from('tbl_center');        
        $this->db->where('center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Grupos que pertenecen a un centro
     * Utilizada en vista de edición de un centro
     */
    function get_center_groups($center_id){
        $this->db->select("*");
        $this->db->from('tbl_group');        
        $this->db->where('center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }

	function get_one_additional_exercise($additional_exercise_id){
		$this->db->select("*");
		$this->db->from('tbl_additional_excercise');
		$this->db->join('tbl_additional_excercise_detail', 'tbl_additional_excercise.additional_excercise_id = tbl_additional_excercise_detail.additional_excercise_id', 'left');
		$this->db->where('tbl_additional_excercise.additional_excercise_id', $additional_exercise_id);
		$query = $this->db->get();
		return $query->result();
    }
    public function update_additional_exercise($data, $id){
		$this->db->where('additional_excercise_id', $id);
		$this->db->update('tbl_additional_excercise', $data);
    }
    public function update_additional_exercise_detail($data, $id){
    	$this->db->where('additional_excercise_id ', $id);
		$this->db->update('tbl_additional_excercise_detail', $data);
    }
    ////////////////////
    public function add_additional_excercise_group($data){
		$this->db->insert('tbl_additional_excercise_group',$data);
		return $this->db->insert_id();
	}
	public function additional_excercise_group_student($data){
		$this->db->insert('tbl_additional_excercise_group_student',$data);
		return $this->db->insert_id();
	}

	function get_additional_excercise_nolist($additional_excercise_id){
		$this->db->select("*");
		$this->db->from('tbl_additional_excercise');
		$this->db->join('tbl_additional_excercise_group', 'tbl_additional_excercise.additional_excercise_id = tbl_additional_excercise_group.aditional_excercise_id', 'left');
		$this->db->join('tbl_group', 'tbl_additional_excercise_group.group_id = tbl_group.group_id', 'left');
		$this->db->where('tbl_additional_excercise.additional_excercise_id', $additional_excercise_id);
		$query = $this->db->get();
		return $query->result();
	}

    public function re_order($i, $v){
        $this->db->set('position_order', $i, FALSE);
        $this->db->where('additional_excercise_id', $v);
        $this->db->update('tbl_additional_excercise');
    }

    public function get_groups_bycenter() {
        $this->db->select("*");
        $this->db->from('tbl_group');
        $this->db->join('tbl_center', 'tbl_group.center_id = tbl_center.center_id', 'left');
        $query = $this->db->get();
        return $query->result();
    }
	function DeleteExercise($id){
		$this->db->delete('tbl_additional_excercise', array('additional_excercise_id' => $id));
	}
	function DeleteExercise_Detail($id){
		$this->db->delete('tbl_additional_excercise_detail', array('additional_excercise_id' => $id));
	}
	function DeleteExercise_Groups($id){
		$this->db->delete('tbl_additional_excercise_group', array('aditional_excercise_id' => $id));
	}
	function DeleteExercise_Student($id){
		$this->db->delete('tbl_additional_excercise_group_student', array('aditional_excercise_id' => $id));
	}
}