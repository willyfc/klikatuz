<?php
class Group_model extends CI_Model{
    /*
     * TTODOS LOS GRUPOS
     */
    public function get_groups() {
        $this->db->select("*");
        $this->db->from('tbl_group');
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * CARGAR SELECT'S PARA CARGAR CENTROS
     */
    public function get_all_center() {
        $this->db->select("*");
        $this->db->from('tbl_center');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function create_group($data){
        $this->db->insert('tbl_group',$data);
        return $this->db->insert_id();
    }
    
    function update_group($id, $data){
        $this->db->where('group_id', $id);
        $this->db->update('tbl_group', $data);
    }
    
    /*
     * Un grupo
     */
    function get_one_group($group_id){
        $this->db->select("*");
        $this->db->from('tbl_group');        
        $this->db->where('group_id', $group_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Obtener profesores
     */
    function get_professors($group_id){
        $this->db->select("*");
        $this->db->from('tbl_group_professor');
        $this->db->join('tbl_user', 'tbl_group_professor.user_id = tbl_user.user_id', 'left');
        $this->db->where('tbl_group_professor.group_id', $group_id);
        $query = $this->db->get();
        return $query->result();
    }
    /*
     * Obtener estudiantes
     */
    function get_students($group_id){
        $this->db->select("*");
        $this->db->from('tbl_group_student');
        $this->db->join('tbl_user', 'tbl_group_student.user_id = tbl_user.user_id', 'left');
        $this->db->where('tbl_group_student.group_id', $group_id);
        $query = $this->db->get();
        return $query->result();
    }

    /*
    * FUNCIONES PARA AGREGAR PROFESORES DESDE LA VISTA DE EDITAR GRUPOS
    */
	public function add_group_professor($data){
		$this->db->insert('tbl_group_professor',$data);
		return $this->db->insert_id();
	}
	function get_professor_nolist($center_id){
		$this->db->select("tbl_user.user_id, tbl_user.user_name");
		$this->db->from('tbl_user');
		$this->db->join('tbl_center_professor', 'tbl_user.user_id = tbl_center_professor.user_id', 'left');
		$this->db->where('tbl_center_professor.center_id', $center_id);
		$query = $this->db->get();
		return $query->result();
	}

    public function add_group_student($data){
        $this->db->insert('tbl_group_student',$data);
        return $this->db->insert_id();
    }
    function get_student_nolist($center_id){
        $this->db->select("tbl_user.user_id, tbl_user.user_name");
        $this->db->from('tbl_user');
        $this->db->join('tbl_center_student', 'tbl_user.user_id = tbl_center_student.user_id', 'left');
        $this->db->where('tbl_center_student.center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * PARA GRUPOS A LOS QUE PERTENECE UN GRUPO
     */
    /*public function get_center($center_id) {
        $this->db->select("*");
        $this->db->from('tbl_center');
        $this->db->where('center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }*/
}