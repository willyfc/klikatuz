<?php
class Dictation_model extends CI_Model{
    
    public function get_dictation($lang) {
    	if (empty($lang)){
    		$lang = "es";
    	}else{
    		$lang = $lang;
    	}
        $this->db->select("*");
        $this->db->from('tbl_exercise');
        $this->db->where('excercise_type_id', 4);
        $this->db->where('exercise_lang', $lang);
        $this->db->order_by('position_order', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_dictationByLesson($lesson_id, $lang = "") {
    	if (empty($lang)){
    		$lang = "es";
    	}else{
    		$lang = "eu";
    	}
        $this->db->select("*");
        $this->db->from('tbl_exercise');
        $this->db->where('excercise_type_id', 4);
        $this->db->where('exercise_lang', $lang);
        $this->db->where('lesson_id', $lesson_id);
        $this->db->order_by('position_order', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }    
    	public function get_onelesson($lesson_id){
    		$this->db->select("lesson_name");
			$this->db->from('tbl_lesson');
			$this->db->where('lesson_id', $lesson_id);
			$this->db->where('lesson_lang', "es");
			$query = $this->db->get();
			return $query->result();
    	}

    public function create_dictation($data){
        $this->db->insert('tbl_exercise',$data);
        return $this->db->insert_id();
    }
    public function create_dictation_content($data){
        $this->db->insert('tbl_exercise_content',$data);
        return $this->db->insert_id();
    }
    function get_lessons(){
        $this->db->select("*");
        $this->db->from('tbl_lesson');
        $query = $this->db->get();
        return $query->result();
    }
    function get_exercise_type(){
        $this->db->select("*");
        $this->db->from('tbl_exercise_type');
        $query = $this->db->get();
        return $query->result();
    }

	function get_one_dictation($dictation_id){
		$this->db->select("*");
		$this->db->from('tbl_exercise');
		$this->db->where('tbl_exercise.exercise_id', $dictation_id);
        $this->db->order_by('tbl_exercise.position_order ', 'ASC');
		$query = $this->db->get();
		return $query->result();
    }

    //EDITAR EJERCICIOS
    public function update_dictation($data, $id){
		$this->db->where('exercise_id', $id);
		$this->db->update('tbl_exercise', $data);
    }
    function get_NameDictation($dictation_id){
		$this->db->select("exercise_name");
		$this->db->from('tbl_exercise');
		$this->db->where('tbl_exercise.exercise_id', $dictation_id);
		$query = $this->db->get();
		return $query->result();
    }
    public function update_dictation_content($data, $id){
		$this->db->where('exercise_content_id', $id);
		$this->db->update('tbl_exercise_content', $data);
    }

    ////////////////////
    public function add_lesson_group($data){
		$this->db->insert('tbl_lesson_group',$data);
		return $this->db->insert_id();
	}
	public function lesson_group_student($data){
		$this->db->insert('tbl_lesson_group_student',$data);
		return $this->db->insert_id();
	}
    public function re_order($i, $v){
        $this->db->set('position_order', $i, FALSE);
        $this->db->where('exercise_id', $v);
        $this->db->update('tbl_exercise');
    }

    function DeleteExercise($id){
        $this->db->delete('tbl_exercise', array('exercise_id' => $id));
    }
    function DeleteExercise_Detail($id){
        $this->db->delete('tbl_exercise_content', array('exercise_content_id' => $id));
    }
}