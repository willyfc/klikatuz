<?php
class User_model extends CI_Model{
    /*
     * 
     * MODELO PARA TODOS LOS TIPOS DE USUARIOS
     * 
     */
    public function get_admins() {
        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('user_level', '1');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_managers() {
        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('user_level', '2');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_professors() {
        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('user_level', '3');
        $query = $this->db->get();
        return $query->result();
    }
    	public function get_professors_by_center($center_id) {
    		if ($center_id == "todos"){
    			$this->db->select("*");
				$this->db->from('tbl_user');
				$this->db->where('user_level', '3');
				$query = $this->db->get();
				return $query->result();	
    		}elseif ($center_id == "sin_asignar") {
    			$this->db->select("tbl_user.user_id, tbl_user.user_name, tbl_user.user_nickname, tbl_user.user_email FROM tbl_user left JOIN tbl_center_professor ON tbl_user.user_id = tbl_center_professor.user_id WHERE tbl_user.user_level = 3 AND (tbl_center_professor.user_id IS NULL OR tbl_center_professor.user_id = '')", FALSE);
				$query = $this->db->get();
				return $query->result();
    		}else{
				$this->db->select("*");
				$this->db->from('tbl_user');
				$this->db->join('tbl_center_professor', 'tbl_user.user_id = tbl_center_professor.user_id', 'left');
				$this->db->where('tbl_center_professor.center_id', $center_id);
				$query = $this->db->get();
				return $query->result();
			}
		}

    public function get_students() {
        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('user_level', '4');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function create_user($data){
        $this->db->insert('tbl_user',$data);
        return $this->db->insert_id();
    }
    
    /*
     * Obtener un usuario sin distincion de rol
     */
    public function get_user($user_id) {
        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }
    /*
     * Para la vista de edicion de un usuario
     */
    public function get_user_edit($user_id, $level) {
        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('user_level', $level);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }
    /*
     * Actualizar campos comunes y rol de usuario
     */
    function update_user($user_id, $data){
        $this->db->where('user_id', $user_id);
        $this->db->update('tbl_user', $data);
    }
    
    /********************************************************************/
    /*
     * PARA ACTUALIZAR CENTROS Y GRUPOS
     * PARA FILTRO DE USUARIOS
     */
    function get_centers(){
        $this->db->select("*");
        $this->db->from('tbl_center');
        $query = $this->db->get();
        return $query->result();
    }
    function get_professor_center($user_id){
        $this->db->select("*");
        $this->db->from('tbl_user as u');
        $this->db->join('tbl_center_professor as cp', 'u.user_id = cp.user_id', 'left');
        $this->db->join('tbl_center as c', 'cp.center_id = c.center_id', 'left');
        $this->db->where('u.user_id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * PARA ACTUALIZAR CENTROS Y GRUPOS
     */
    
    /********************************************************************/
    
    /*
     * Un centro
    */
    function update_center($id, $data){
        $this->db->where('center_id', $id);
        $this->db->update('tbl_center', $data);
    }
    function get_one_center($center_id){
        $this->db->select("*");
        $this->db->from('tbl_center');        
        $this->db->where('center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Grupos que pertenecen a un centro
     * Utilizada en vista de edición de un centro
     */
    function get_center_groups($center_id){
        $this->db->select("*");
        $this->db->from('tbl_group');        
        $this->db->where('center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
    * PARA AGREGAR CENTROS Y GRUPOS A UN ESTUDIANTE
    */
    public function add_center_student($data){
        $this->db->insert('tbl_center_student',$data);
        return $this->db->insert_id();
    }
    public function add_group_student($data){
        $this->db->insert('tbl_group_student',$data);
        return $this->db->insert_id();
    }
    /*
     * PARA AGREGAR CENTROS Y GRUPOS A UN ESTUDIANTE
     */
    
    /*
    * PARA AGREGAR CENTROS Y GRUPOS A UN PROFESOR
    */
    public function add_center_professor($data){
        $this->db->insert('tbl_center_professor',$data);
        return $this->db->insert_id();
    }
    public function add_group_professor($data){
        $this->db->insert('tbl_group_professor',$data);
        return $this->db->insert_id();
    }
    /*
     * PARA AGREGAR CENTROS Y GRUPOS A UN PROFESOR
     */
    
}