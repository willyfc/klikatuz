<?php
class Lesson_model extends CI_Model{
    
    public function get_lesson() {
        $this->db->select("*");
        $this->db->from('tbl_lesson');
        $this->db->order_by('position_order', 'ASC');
        $this->db->where('lesson_lang', 'es');
        $query = $this->db->get();
        return $query->result();
    }
    public function eu() {
        $this->db->select("*");
        $this->db->from('tbl_lesson');
        $this->db->order_by('position_order', 'ASC');
        $this->db->where('lesson_lang', 'eu');
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * TTODOS LOS CENTROS
     */
    public function get_center() {
        $this->db->select("*");
        $this->db->from('tbl_center');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_groups() {
        $this->db->select("*");
        $this->db->from('tbl_group');
        $query = $this->db->get();
        return $query->result();
    }
    public function create_lesson($data){
        $this->db->insert('tbl_lesson',$data);
        return $this->db->insert_id();
    }
    public function create_lesson_detail($data){
        $this->db->insert('tbl_lesson_detail',$data);
        return $this->db->insert_id();
    }
    public function insert_video($data){
        $this->db->insert('tbl_lesson_media ',$data);
        return $this->db->insert_id();
    }
    function update_center($id, $data){
        $this->db->where('center_id', $id);
        $this->db->update('tbl_center', $data);
    }
    
    /*
     * Listado de Todos los Directores
     */
    function get_managers() {
        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('user_level', '2');
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Un centro
     */
    function get_one_center($center_id){
        $this->db->select("*");
        $this->db->from('tbl_center');        
        $this->db->where('center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Grupos que pertenecen a un centro
     * Utilizada en vista de edición de un centro
     */
    function get_center_groups($center_id){
        $this->db->select("*");
        $this->db->from('tbl_group');        
        $this->db->where('center_id', $center_id);
        $query = $this->db->get();
        return $query->result();
    }

	function get_one_lesson($lesson_id){
		$this->db->select("*");
		$this->db->from('tbl_lesson');
		$this->db->join('tbl_lesson_detail', 'tbl_lesson.lesson_id = tbl_lesson_detail.lesson_id', 'left');
		$this->db->join('tbl_lesson_media', 'tbl_lesson.lesson_id = tbl_lesson_media.lesson_id', 'left');
		$this->db->where('tbl_lesson.lesson_id', $lesson_id);
		$query = $this->db->get();
		return $query->result();
    }
    public function update_lesson($data, $id){
		$this->db->where('lesson_id', $id);
		$this->db->update('tbl_lesson', $data);
    }
    public function update_lesson_detail($data, $id){
    	$this->db->where('lesson_id', $id);
		$this->db->update('tbl_lesson_detail', $data);
    }
    public function update_video1($data, $id){
    	$this->db->where('lesson_id', $id);
		$this->db->update('tbl_lesson_media', $data);
    }
    public function update_video2($data, $id){
    	$this->db->where('lesson_id', $id);
		$this->db->update('tbl_lesson_media', $data);
    }

    ////////////////////
    public function add_lesson_group($data){
		$this->db->insert('tbl_lesson_group',$data);
		return $this->db->insert_id();
	}
	public function lesson_group_student($data){
		$this->db->insert('tbl_lesson_group_student',$data);
		return $this->db->insert_id();
	}
    public function re_order($i, $v){
        $this->db->set('position_order', $i, FALSE);
        $this->db->where('lesson_id', $v);
        $this->db->update('tbl_lesson');
    }

    public function get_groups_bycenter() {
        $this->db->select("*");
        $this->db->from('tbl_group');
        $this->db->join('tbl_center', 'tbl_group.center_id = tbl_center.center_id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    function DeleteLesson($id){
        $this->db->delete('tbl_lesson', array('lesson_id' => $id));
    }
    function DeleteLesson_ad($id){
        $lesson_belong = "SELECT exercise_id,exercise_content FROM tbl_exercise WHERE lesson_id = ". $id;
        $query_belong = $this->db->query($lesson_belong);
        if (!empty($query_belong->result())){
            foreach ($query_belong->result() as $row) {
                $this->db->delete('tbl_exercise_content', array('exercise_content_id' => $row->exercise_content));
            }
        }
        $this->db->delete('tbl_exercise', array('lesson_id' => $id));
    }
}