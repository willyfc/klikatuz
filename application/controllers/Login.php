<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('login_model');
    }
    public function index(){
        $this->load->view('login');
    }
    function auth(){
        $username    = $this->input->post('username',TRUE);
        $password = md5($this->input->post('password',TRUE));
        $validate = $this->login_model->validate($username,$password);
        if($validate->num_rows() > 0){
            $data  = $validate->row_array();
            $user_id  = $data['user_id'];
            $user_nickname  = $data['user_nickname'];
            $email = $data['user_email'];
            $user_name = $data['user_name'];
            $level = $data['user_level'];
            $sesdata = array(
                'user_id' => $user_id,
                'user_nickname' => $user_nickname,
                'email'     => $email,
                'user_name' => $user_name,
                'level'     => $level,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($sesdata);
            // access login for admin
            if($level === '1'){
                redirect('admin/dashboard');
            }else{
                redirect('courses/dashboard');
            }
        }else{
            echo $this->session->set_flashdata('msg','Username or Password is Wrong');
            redirect('login');
        }
    }
    function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }
}
