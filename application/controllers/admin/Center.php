<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Center extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('admin/Center_model', 'CModel');
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
    }
    function index(){
        if($this->session->userdata('level')==='1'){
            $get_center = $this->CModel->get_center();
            $get_managers = $this->CModel->get_managers();
            $data['title'] = 'Centros';
            $data['center_list'] = $get_center;
            $data['manager_list'] = $get_managers;
            $this->load->view('admin/center_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    
    function create_center(){
        $this->form_validation->set_rules('center_name', 'center_name', 'trim');
        $this->form_validation->set_rules('user_id', 'user_id', 'trim');
        //echo $this->input->post('user_id');exit();
        if ($this->form_validation->run() == FALSE) {

        }else{
            $data_insert = array('center_name' => $this->input->post('center_name'), 'manager_id' => $this->input->post('user_id'));
            $get_center = $this->CModel->get_center();
            
            $this->CModel->create_center($data_insert);
            $data['message'] = 'Centro creado correctamente';
            
            $data['title'] = 'Centros';
            $data['center_list'] = $get_center;
            redirect('admin/center');
            //$this->load->view('admin/center_view', $data);
        }
    }
    
    /*
     * VISTA PARA EDITAR UN CENTRO
     */
    function center_edit_view(){
        $center_id = $this->uri->segment(4);
        if($this->session->userdata('level')==='1'){
            $get_center = $this->CModel->get_one_center($center_id);
            $data['title'] = 'Centros - Editar';
            $data['center_list'] = $get_center;
            $get_managers = $this->CModel->get_managers();
            $data['manager_list'] = $get_managers;
            $get_center_groups = $this->CModel->get_center_groups($center_id);
            $data['groups_list'] = $get_center_groups;
            $this->load->view('admin/center_edit_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    function center_edit(){
        $this->form_validation->set_rules('center_id', 'center_id', 'trim');
        $this->form_validation->set_rules('center_name', 'center_name', 'trim');
        $this->form_validation->set_rules('user_actual_id', 'user_actual_id', 'trim');
        $this->form_validation->set_rules('user_new_id', 'user_new_id', 'trim');
        
        $center_id = $this->input->post('center_id');
        if($this->session->userdata('level')==='1'){
            if (empty($this->input->post('user_new_id'))){
                $data_update = array('center_name' => $this->input->post('center_name'), 'manager_id' => $this->input->post('user_actual_id'));
            }else{
                $data_update = array('center_name' => $this->input->post('center_name'), 'manager_id' => $this->input->post('user_new_id'));
            }
            $this->CModel->update_center($center_id, $data_update);
            redirect('admin/center/center_edit_view/'.$center_id);
        }else{
            echo "Access Denied";
        }
    }
    /*
    * FUNCION PARA CARGAR UN SOLO CENTRO CON TODOS SUS DATOS
    */
    function center_single_view(){
        $center_id = $this->uri->segment(4);
        if($this->session->userdata('level')==='1'){
            $get_center = $this->CModel->get_one_center($center_id);
            $data['title'] = "Centro: ".$get_center[0]->center_name;
            $data['center_list'] = $get_center;
            $get_manager = $this->CModel->get_manager($center_id);
            $data['manager'] = $get_manager;
            $get_center_groups = $this->CModel->get_center_groups($center_id);
            $data['groups_list'] = $get_center_groups;
            $this->load->view('admin/center_single_view', $data);
        }else{
            echo "Access Denied";
        }
    }

    /*
     * FUNCIÓN PARA MANEJAR UN SOLO CENTRO
     */
    /*function create_center_unique(){
        $this->form_validation->set_rules('center_name', 'center_name', 'trim');
        if ($this->form_validation->run() == FALSE) {

        }else{
            $data_insert = array('center_name' => $this->input->post('center_name'),);
            $get_center = $this->CModel->get_center();
            if (!empty($get_center)){
                $center_id = $get_center[0]->center_id;
                $this->CModel->update_center($center_id, $data_insert);                
                $get_center_up = $this->CModel->get_center();
                $center_name = $get_center_up[0]->center_name;
                $data['message'] = 'Centro actualizado correctamente';
            }else{
                $this->CModel->create_center($data_insert);
                $data['message'] = 'Centro creado correctamente';
            }
            $data['title'] = 'Centro';
            $data['center'] = $center_name;
            $this->load->view('admin/center_view', $data);
        }
    }*/
}
