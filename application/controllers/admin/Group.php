<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Group extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('admin/Group_model', 'GModel');
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
    }
    function index(){
        if($this->session->userdata('level')==='1'){
            $get_group = $this->GModel->get_groups();
            $get_all_center = $this->GModel->get_all_center();
            $data['title'] = 'Grupos';
            $data['groups'] = $get_group;
            $data['centers'] = $get_all_center;
            $this->load->view('admin/group_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    
    function create_group(){
        $this->form_validation->set_rules('group_name', 'group_name', 'trim');
        $this->form_validation->set_rules('centers', 'centers', 'trim');
        if ($this->form_validation->run() == FALSE) {

        }else{
            $data_insert = array('group_name' => $this->input->post('group_name'), 'center_id' => $this->input->post('centers'));
            $get_group = $this->GModel->get_groups();
            $get_all_center = $this->GModel->get_all_center();
            
            $this->GModel->create_group($data_insert);
            $data['message'] = 'Grupo creado correctamente';
            
            $data['title'] = 'Grupos';
            $data['groups'] = $get_group;
            $data['centers'] = $get_all_center;
            redirect('admin/group');
        }
    }
    
    /*
     * VISTA PARA EDITAR UN GRUPO
     */
    function group_edit_view(){
        $group_id = $this->uri->segment(4);
        if($this->session->userdata('level')==='1'){
            $get_group = $this->GModel->get_one_group($group_id);
            $data['title'] = 'Grupos - Editar';
            $data['groups'] = $get_group;
            $get_all_center = $this->GModel->get_all_center();
            $data['centers'] = $get_all_center;
            
            $get_student_nolist = $this->GModel->get_student_nolist($group_id);
            $data['students_nolist'] = $get_student_nolist;

            $get_professors_nolist = $this->GModel->get_professor_nolist($group_id);
            $data['professors_nolist'] = $get_professors_nolist;

            $get_professors = $this->GModel->get_professors($group_id);
            $data['professors'] = $get_professors;
            $get_students = $this->GModel->get_students($group_id);
            $data['students'] = $get_students;
            $this->load->view('admin/group_edit_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    
    function group_edit(){
        $this->form_validation->set_rules('group_id', 'group_id', 'trim');
        $this->form_validation->set_rules('group_name', 'group_name', 'trim');
        $this->form_validation->set_rules('center_actual_id', 'center_actual_id', 'trim');
        $this->form_validation->set_rules('center_new_id', 'center_new_id', 'trim');
        
        $group_id = $this->input->post('group_id');
        if($this->session->userdata('level')==='1'){
            if (empty($this->input->post('center_new_id'))){
                $data_update = array('group_name' => $this->input->post('group_name'), 'center_id' => $this->input->post('center_actual_id'));
            }else{
                $data_update = array('group_name' => $this->input->post('group_name'), 'center_id' => $this->input->post('center_new_id'));
            }
            $this->GModel->update_group($group_id, $data_update);
            redirect('admin/group/group_edit_view/'.$group_id);
        }else{
            echo "Access Denied";
        }
    }

    /*
    * FUNCION PARA AGREGAR PROFESORES A UN GRUPO AL EDITARLO
    * Los profesres listados deben pertenecer al mismo centro que el grupo
    */
    public function add_group_professor(){
        $this->form_validation->set_rules('asignar_grupo', 'asignar_grupo', 'trim');
        
        $group_id = $this->input->post('group_id');
        $center_id = $this->input->post('center_id');
        if($this->session->userdata('level')==='1'){
            $data_insert = array('user_id' => $this->input->post('asignar_grupo'),'center_id' => $center_id, 'group_id' => $group_id);
            $this->GModel->add_group_professor($data_insert);
            redirect('admin/group/group_edit_view/'.$group_id);
        }else{
            echo "Access Denied";
        }
    }

    public function add_group_student(){
        $this->form_validation->set_rules('asignar_grupo', 'asignar_grupo', 'trim');
        
        $group_id = $this->input->post('group_id');
        $center_id = $this->input->post('center_id');
        if($this->session->userdata('level')==='1'){
            $data_insert = array('user_id' => $this->input->post('asignar_grupo'),'center_id' => $center_id, 'group_id' => $group_id);
            $this->GModel->add_group_student($data_insert);
            redirect('admin/group/group_edit_view/'.$group_id);
        }else{
            echo "Access Denied";
        }
    }
    
}