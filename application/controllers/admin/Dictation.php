<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dictation extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('admin/Dictation_model', 'DModel');
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
    }
    function index(){
    	$lang="es";
        if($this->session->userdata('level')==='1'){
            $data['title'] = 'Dictados';
            $get_dictation = $this->DModel->get_dictation($lang);
            $data['dictation_list'] = $get_dictation;
            $this->load->view('admin/dictation_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    function eu(){
    	$lang = "eu";
        if($this->session->userdata('level')==='1'){
            $data['title'] = 'Dictados';
            $get_dictation = $this->DModel->get_dictation($lang);
            $data['dictation_list'] = $get_dictation;
            $data['eu_active'] = "eu_active";
            $this->load->view('admin/dictation_view', $data);
        }else{
            echo "Access Denied";
        }
    }
	function filter_bylesson(){
		$lesson_id = $this->uri->segment(4);
		$lang = $this->uri->segment(5);
		if($this->session->userdata('level')==='1'){
			$data['title'] = 'Dictados';
			$get_dictation = $this->DModel->get_dictationByLesson($lesson_id);
			$get_onelesson = $this->DModel->get_onelesson($lesson_id, $lang);
			$data['dictation_list'] = $get_dictation;
			$data['get_onelesson'] = $get_onelesson;
			$data['subtitle'] = "Dictados de la lección: ";
			$this->load->view('admin/dictation_view', $data);
		}else{
			echo "Access Denied";
		}
	}
    
    function create_dictation_view(){
		if($this->session->userdata('level')==='1'){
			$data['title'] = 'Dictados - Nuevo Dictado';
			$get_lessons = $this->DModel->get_lessons();
			$data['lessons'] = $get_lessons;
			$get_exercise_type = $this->DModel->get_exercise_type();
			$data['exercise_types'] = $get_exercise_type;
			$this->load->view('admin/dictation_new_view', $data);
        }else{
            echo "Access Denied";
        }
    }

	function create_dictation(){
		$this->form_validation->set_rules('exercise_name', 'exercise_name', 'trim');
		$this->form_validation->set_rules('exercise_content', 'exercise_content', 'trim');

		$exercise_name = $this->input->post('exercise_name');
		$lesson_id = $this->input->post('lesson_id');
				
		$type_id = $this->input->post('type_id');
		$exercise_time = $this->input->post('exercise_time');
		$exercise_max_time = $this->input->post('exercise_max_time');
		$exercise_repetition = $this->input->post('exercise_repetition');
		$exercise_visible = $this->input->post('exercise_visible');
			if ($exercise_visible == "on"){ $exercise_visible = 1; }else{ $exercise_visible = 0; }
		$exercise_correct = $this->input->post('exercise_correct');
			if ($exercise_correct == "on"){ $exercise_correct = 1; }else{ $exercise_correct = 0; }
		$exercise_max_errors = $this->input->post('exercise_max_errors');
		$exercise_content = $this->input->post('exercise_content');

		$exercise_lang = $this->input->post('exercise_lang');
				
		$data_insert = array(
			'exercise_name' => $exercise_name,
			'exercise_time' => $exercise_time,
			'exercise_max_time' => $exercise_max_time,
			'exercise_repetition' => $exercise_repetition,
			'exercise_correct' => $exercise_correct,
			'exercise_visible' => $exercise_visible,
			'exercise_max_errors' => $exercise_max_errors,
			'exercise_lang' => $exercise_lang,
			'excercise_type_id' => $type_id,
			'lesson_id' => $lesson_id
		);
		$last_exercise = $this->DModel->create_dictation($data_insert);

		$data_insert_content = array('exercise_content_name' => $exercise_name, 'exercise_content_text' => $exercise_content, 'exercise_content_lang' => $exercise_lang);
		$last_insert_content = $this->DModel->create_dictation_content($data_insert_content);

		$data_update_content_id = array('exercise_content' => $last_insert_content);
		$this->DModel->update_dictation($data_update_content_id,$last_exercise);		

		$data['title'] = 'Dictados - Nuevo Dictado';
		redirect('admin/dictation/dictation_edit_view/'.$last_exercise);
	}

	function dictation_edit_view(){
		$dictation_id = $this->uri->segment(4);
		if($this->session->userdata('level')==='1'){
			$get_one_dictation = $this->DModel->get_one_dictation($dictation_id);
			$data['title'] = 'Dictados - Editar Dictado';
			$data['dictations'] = $get_one_dictation;
			$data['d_id'] = $dictation_id;
			$get_lessons = $this->DModel->get_lessons();
			$data['lessons'] = $get_lessons;
			$this->load->view('admin/dictation_edit_view', $data);
		}else{
			echo "Access Denied";
		}
    }

    function dictation_edit(){
		if($this->session->userdata('level')==='1'){
			$dictation_id = $this->input->post('dictation_id');
			$exercise_content_id = $this->input->post('exercise_content_id');

			$exercise_name = $this->input->post('exercise_name');
			$lesson_actual_id = $this->input->post('lesson_actual_id');
			$lesson_new = $this->input->post('lesson_id');
			//Verifica si la lección cambió
			if (empty($lesson_new)){ $lesson_id = $lesson_actual_id; }else{ $lesson_id = $lesson_new; }
			
			$exercise_time = $this->input->post('exercise_time');
			$exercise_max_time = $this->input->post('exercise_max_time');
			$exercise_repetition = $this->input->post('exercise_repetition');
			$exercise_visible = $this->input->post('exercise_visible');
				if ($exercise_visible == "on"){ $exercise_visible = 1; }else{ $exercise_visible = 0; }
			$exercise_correct = $this->input->post('exercise_correct');
				if ($exercise_correct == "on"){ $exercise_correct = 1; }else{ $exercise_correct = 0; }
			$exercise_max_errors = $this->input->post('exercise_max_errors');
			$exercise_content_text = $this->input->post('exercise_content_text');

			$exercise_lang_new = $this->input->post('exercise_lang');
			$actual_exercise_lang = $this->input->post('actual_exercise_lang');
			//Verifica si el idioma cambió
			if (empty($exercise_lang_new)){ $exercise_lang = $actual_exercise_lang; }else{ $exercise_lang = $exercise_lang_new; }
			
			$data_update = array(
				'exercise_name' => $exercise_name,
				'exercise_time' => $exercise_time,
				'exercise_max_time' => $exercise_max_time,
				'exercise_repetition' => $exercise_repetition,
				'exercise_correct' => $exercise_correct,
				'exercise_visible' => $exercise_visible,
				'exercise_max_errors' => $exercise_max_errors,
				'exercise_lang' => $exercise_lang,
				'lesson_id' => $lesson_id
			);
			$this->DModel->update_dictation($data_update, $dictation_id);

			$data_update_content = array( 'exercise_content_text' => $exercise_content_text );
			$this->DModel->update_dictation_content($data_update_content, $exercise_content_id);

			redirect('admin/dictation/dictation_edit_view/'.$dictation_id);
		}else{
			echo "Access Denied";
		}
    }

    ///////////////////////////////////////
    
    public function dictation_single_view(){
    	$lesson_id = $this->uri->segment(4);
		if($this->session->userdata('level')==='1'){
			$get_one_lesson = $this->DModel->get_one_lesson($lesson_id);
			$get_groups = $this->DModel->get_groups();
			$data['title'] = 'Lecciones';
			$data['lesson'] = $get_one_lesson;
			$data['groups_list'] = $get_groups;
			$data['l_id'] = $lesson_id;
			$this->load->view('admin/lesson_single_view', $data);
		}else{
			echo "Access Denied";
		}
    }

    public function add_lesson_dictation_group(){
		$group_id = $this->input->post('asignar_grupo');
		$lesson_id = $this->input->post('lesson_id');
        
		if($this->session->userdata('level')==='1'){
			$data_insert = array('group_id' => $group_id, 'lesson_id' => $lesson_id);
            $this->DModel->add_lesson_group($data_insert);

            $sql_students = "SELECT * FROM `tbl_user` LEFT JOIN tbl_group_student ON tbl_user.user_id = tbl_group_student.user_id  WHERE tbl_user.user_level = 4 AND tbl_group_student.group_id = ".$group_id ;
			$query_students = $this->db->query($sql_students);
			if (!empty($query_students->result())){
				foreach ($query_students->result() as $row) {
					$data_insert_std = array('lesson_id' => $lesson_id, 'group_id' => $group_id, 'user_id' => $row->user_id, 'state' => 0 );
					$this->DModel->lesson_group_student($data_insert_std);
				}
			}

            redirect('admin/lesson/lesson_single_view/'.$lesson_id);
        }else{
            echo "Access Denied";
        }
    }

    public function delete_block(){
    	$dictation_id = $this->input->post('bloque');
    	if($this->session->userdata('level')==='1'){
			$get_one_lesson = $this->DModel->delete_block($dictation_id);
		}else{
			echo "Access Denied";
		}
    }

    function create_dictation_edit(){
    	$dictation_id = $this->input->post('dictation_id');
		$i = 1;
		$dictation_name = $this->input->post('nombre_ejercicio');
		$dictation_content = $this->input->post('ejercicio');
		$dictado_t1 = $this->input->post('dictado_t1');
		$dictado_t2 = $this->input->post('dictado_t2');
		for($i=0;$i<sizeof($dictation_name);$i++) {
			$data_insert_sub = array('dictation_sub_name' => $dictation_name[$i], 'dictation_sub_content' => $dictation_content[$i], 'dictation_sub_time' => $dictado_t1[$i], 'dictation_sub_max_time' => $dictado_t2[$i], 'dictation_id' => $dictation_id );
			$this->DModel->create_dictation_sub($data_insert_sub);
		}

		redirect('admin/dictation/dictation_edit_view/'.$dictation_id);
	}

	function re_order(){
	   	$position = $this->input->post('position');
	   	$i = 1;
	   	foreach($position as $k=>$v){
	   		$this->DModel->re_order($i, $v);
	   		$i++;
	   	}
	}

	function re_order_sub(){
	   	$position = $this->input->post('position');
	   	$i = 1;
	   	foreach($position as $k=>$v){
	   		$this->DModel->re_order_sub($i, $v);
	   		$i++;
	   	}
	}

	function DeleteExercise(){
		$exercise_id = $this->uri->segment(4);
		$exercise_content = $this->uri->segment(5);
		if($this->session->userdata('level')==='1'){
			$this->DModel->DeleteExercise($exercise_id);
			$this->DModel->DeleteExercise_Detail($exercise_content);
			redirect('admin/dictation');
		}
	}

}
