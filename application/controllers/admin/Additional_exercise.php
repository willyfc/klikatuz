<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Additional_exercise extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('admin/AdditionalE_model', 'AEModel');
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
    }
    function index(){
        if($this->session->userdata('level')==='1'){
            $data['title'] = 'Ejercicios adicionales';
            $get_ae = $this->AEModel->get_ae();
            $data['ae_list'] = $get_ae;
            $this->load->view('admin/additional_exercise_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    function eu(){
        if($this->session->userdata('level')==='1'){
            $data['title'] = 'Ejercicios adicionales';
            $get_ae = $this->AEModel->eu();
            $data['ae_list'] = $get_ae;
            $this->load->view('admin/additional_exercise_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    
    function create_additional_exercise_view(){
		if($this->session->userdata('level')==='1'){
			$get_center = $this->AEModel->get_center();
			$data['title'] = 'Ejercicios Adicionales - Nuevo Ejercicio';
			$this->load->view('admin/additional_exercise_new_view', $data);
        }else{
            echo "Access Denied";
        }
    }

	function create_additional_exercise(){
		$this->form_validation->set_rules('ae_name', 'ae_name', 'trim');
		$this->form_validation->set_rules('ae_content', 'ae_content', 'trim');

		///////

		$data_insert = array('additional_excercise_name' => $this->input->post('ae_name'), 'additional_excercise_lang' => $this->input->post('ae_lang'));
		$last_additional_excersice = $this->AEModel->create_additional_excersice($data_insert);

		$data_insert_2 = array('additional_excercise_content ' => $this->input->post('ae_content'), 'additional_excercise_time ' => $this->input->post('ae_time'), 'additional_excercise_max_time' => $this->input->post('ae_max_time'), 'additional_excercise_id ' => $last_additional_excersice,);
		$this->AEModel->create_additional_excersice_detail($data_insert_2);

		$data['message'] = 'Lección creada correctamente';

		$data['title'] = 'Ejercicios Adicionales - Nueva Ejercicio';
		redirect('admin/additional_exercise');
	}

	function additional_exercise_edit_view(){
		$additional_exercise_id = $this->uri->segment(4);
		if($this->session->userdata('level')==='1'){
			$get_one_additional_exercise = $this->AEModel->get_one_additional_exercise($additional_exercise_id);
			$data['title'] = 'Ejercicios Adicionales - Editar Ejercicio';
			$data['additional_exercises'] = $get_one_additional_exercise;
			$data['ae_id'] = $additional_exercise_id;
			$this->load->view('admin/additional_exercise_edit_view', $data);
		}else{
			echo "Access Denied";
		}
    }

    function additional_exercise_edit(){
		$additional_excercise_id = $this->input->post('additional_exercise_id');
		$additional_excercise_name = $this->input->post('additional_exercise_name');

		$additional_excercise_lang_new = $this->input->post('ae_lang');
		$additional_excercise_lang_actual = $this->input->post('actual_lang');
		if (empty($additional_excercise_lang_new)){
			$additional_excercise_lang = $additional_excercise_lang_actual;
		}else{
			$additional_excercise_lang = $additional_excercise_lang_new;
		}

		$additional_excercise_content = $this->input->post('additional_excercise_content');
		$additional_excercise_time = $this->input->post('additional_excercise_time');
		$additional_excercise_max_time = $this->input->post('additional_excercise_max_time');
		if($this->session->userdata('level')==='1'){
			$data_update_lesson = array('additional_excercise_name' => $additional_excercise_name,'additional_excercise_lang' => $additional_excercise_lang );
			$this->AEModel->update_additional_exercise($data_update_lesson, $additional_excercise_id);
			
			$data_update_detail = array('additional_excercise_content' => $additional_excercise_content, 'additional_excercise_time' => $additional_excercise_time, 'additional_excercise_max_time' => $additional_excercise_max_time);
			$this->AEModel->update_additional_exercise_detail($data_update_detail, $additional_excercise_id);
						
			redirect('admin/additional_exercise/additional_exercise_edit_view/'.$additional_excercise_id);
		}else{
			echo "Access Denied";
		}
    }

    ///////////////////////////////////////
    //additional_exercise
    public function additional_exercise_single_view(){
    	$additional_exercise_id = $this->uri->segment(4);
		if($this->session->userdata('level')==='1'){
			$get_one_additional_exercise = $this->AEModel->get_one_additional_exercise($additional_exercise_id);
			$get_groups_bycenter = $this->AEModel->get_groups_bycenter();
			$data['title'] = 'Ejercicios Adicionales';
			$data['additional_exercise'] = $get_one_additional_exercise;

			$get_additional_excercise_nolist = $this->AEModel->get_additional_excercise_nolist($additional_exercise_id);
            $data['additional_excercise_nolist'] = $get_additional_excercise_nolist;

			$data['groups_list'] = $get_groups_bycenter;
			$data['ae_id'] = $additional_exercise_id;
			$this->load->view('admin/additional_exercise_single_view', $data);
		}else{
			echo "Access Denied";
		}
    }

    public function add_additional_exercise_group(){
		$group_id = $this->input->post('asignar_grupo');
		$additional_excercise_id = $this->input->post('additional_excercise_id');
        
		if($this->session->userdata('level')==='1'){
			$data_insert = array('group_id' => $group_id, 'aditional_excercise_id' => $additional_excercise_id);
            $this->AEModel->add_additional_excercise_group($data_insert);

            $sql_students = "SELECT * FROM `tbl_user` LEFT JOIN tbl_group_student ON tbl_user.user_id = tbl_group_student.user_id  WHERE tbl_user.user_level = 4 AND tbl_group_student.group_id = ".$group_id ;
			$query_students = $this->db->query($sql_students);
			if (!empty($query_students->result())){
				foreach ($query_students->result() as $row) {
					$data_insert_std = array('aditional_excercise_id ' => $additional_excercise_id, 'group_id' => $group_id, 'user_id' => $row->user_id, 'state' => 0 );
					$this->AEModel->additional_excercise_group_student($data_insert_std);
				}
			}

            redirect('admin/additional_exercise/additional_exercise_single_view/'.$additional_excercise_id);
        }else{
            echo "Access Denied";
        }
    }

    function re_order(){
	   	$position = $this->input->post('position');
	   	$i = 1;
	   	foreach($position as $k=>$v){
	   		$this->AEModel->re_order($i, $v);
	   		$i++;
	   	}
	}

	function DeleteExercise(){
		$additional_exercise_id = $this->uri->segment(4);
		if($this->session->userdata('level')==='1'){
			$this->AEModel->DeleteExercise($additional_exercise_id);
			$this->AEModel->DeleteExercise_Detail($additional_exercise_id);
			$this->AEModel->DeleteExercise_Groups($additional_exercise_id);
			$this->AEModel->DeleteExercise_Student($additional_exercise_id);
			redirect('admin/additional_exercise');
		}
	}

}