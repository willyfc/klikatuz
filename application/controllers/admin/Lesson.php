<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lesson extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('admin/Lesson_model', 'LModel');
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
    }
    function index(){
    	$message = $this->uri->segment(4);
        if($this->session->userdata('level')==='1'){
            $data['title'] = 'Lecciones';
            $get_lesson = $this->LModel->get_lesson();
            $data['lesson_list'] = $get_lesson;
            if ($message == 1){
            	$data["eliminado"] = "Contenido Eliminido";
            }
            $this->load->view('admin/lesson_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    function eu(){
        if($this->session->userdata('level')==='1'){
            $data['title'] = 'Lecciones';
            $get_lesson = $this->LModel->eu();
            $data['lesson_list'] = $get_lesson;
            $this->load->view('admin/lesson_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    
    function create_lesson_view(){
		if($this->session->userdata('level')==='1'){
			$get_center = $this->LModel->get_center();
			$data['title'] = 'Lecciones - Nueva Lección';
			$this->load->view('admin/lesson_new_view', $data);
        }else{
            echo "Access Denied";
        }
    }

	function create_lesson(){
		$this->form_validation->set_rules('lesson_name', 'lesson_name', 'trim');
		$this->form_validation->set_rules('lesson_content', 'lesson_content', 'trim');

		$rand_name = rand(1262055681,1262055681);
		$video_name = date("Y-m-dH_i_s",$rand_name);
		$config['upload_path'] = './uploads/videos';
		$config['allowed_types'] = '*';
		$config['max_size'] = 100000000;
		$config['overwrite'] = FALSE;
		$config['remove_spaces'] = TRUE;
		$config['file_name'] = $video_name;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('userfile')){
			var_dump($this->upload->display_errors());
		}else{
			$data = array('upload_data' => $this->upload->data());
		}

		///////
		$rand_name2 = rand(1262055681,1262055681);
		$video_name2 = date("Y-m-dH_i_s",$rand_name2);
		$config2['upload_path'] = './uploads/videos';
		$config2['allowed_types'] = '*';
		$config2['max_size'] = 100000000;
		$config2['overwrite'] = FALSE;
		$config2['remove_spaces'] = TRUE;
		$config2['file_name'] = $video_name2;
		$this->load->library('upload', $config2);
		if ( ! $this->upload->do_upload('userfile2')){
			var_dump($this->upload->display_errors());
		}else{
			$data = array('upload_data' => $this->upload->data());
		}

		///////

		$data_insert = array('lesson_name' => $this->input->post('lesson_name'), 'lesson_lang' => $this->input->post('lesson_lang'));
		$last_lesson = $this->LModel->create_lesson($data_insert);

		$data_insert_2 = array('lesson_detail_content' => $this->input->post('lesson_content'), 'lesson_id' => $last_lesson, 'lesson_time' => $this->input->post('lesson_time'), 'lesson_max_time' => $this->input->post('lesson_max_time'));
		$this->LModel->create_lesson_detail($data_insert_2);

		$data_insert_3 = array('lesson_id' => $last_lesson, 'media1' => 'uploads/videos/'.$video_name, 'media2' => 'uploads/videos/'.$video_name2);
		$this->LModel->insert_video($data_insert_3);

		$data['message'] = 'Lección creada correctamente';

		$data['title'] = 'Lecciones - Nueva Lección';
		redirect('admin/lesson');
	}

	function lesson_edit_view(){
		$lesson_id = $this->uri->segment(4);
		if($this->session->userdata('level')==='1'){
			$get_one_lesson = $this->LModel->get_one_lesson($lesson_id);
			$data['title'] = 'Lecciones - Editar Lección';
			$data['lessons'] = $get_one_lesson;
			$data['l_id'] = $lesson_id;
			$this->load->view('admin/lesson_edit_view', $data);
		}else{
			echo "Access Denied";
		}
    }

    function lesson_edit(){
		$lesson_id = $this->input->post('lesson_id');
		$lesson_name = $this->input->post('lesson_name');

		$lesson_lang_new = $this->input->post('ae_lang');
		$lesson_lang_actual = $this->input->post('actual_lang');
		if (empty($lesson_lang_new)){
			$lesson_lang = $lesson_lang_actual;
		}else{
			$lesson_lang = $lesson_lang_new;
		}

		$lesson_content = $this->input->post('lesson_content');
		$media1 = $this->input->post('media1');
		$media2 = $this->input->post('media2');
		$userfile = $this->input->post('userfile');
		$lesson_time = $this->input->post('lesson_time');
		$lesson_max_time = $this->input->post('lesson_max_time');
		if($this->session->userdata('level')==='1'){
			$data_update_lesson = array('lesson_name' => $lesson_name, 'lesson_lang' => $lesson_lang);
			$this->LModel->update_lesson($data_update_lesson, $lesson_id);
			
			$data_update_detail = array('lesson_detail_content' => $lesson_content, 'lesson_time' => $lesson_time, 'lesson_max_time' => $lesson_max_time);
			$this->LModel->update_lesson_detail($data_update_detail, $lesson_id);
			
			$rand_name = rand(1262055681,1262055681);
			$video_name = date("Y-m-dH_i_s",$rand_name);
			$config['upload_path'] = './uploads/videos';
			$config['allowed_types'] = '*';
			$config['max_size'] = 100000000;
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$config['file_name'] = $video_name;
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('userfile')){
				
			}else{
				$data = array('upload_data' => $this->upload->data());
				$data_update_media1 = array('media1' => 'uploads/videos/'.$video_name);
				$this->LModel->update_video1($data_update_media1, $lesson_id);
			}

			///////
			$rand_name2 = rand(1262055681,1262055681);
			$video_name2 = date("Y-m-dH_i_s",$rand_name2);
			$config2['upload_path'] = './uploads/videos';
			$config2['allowed_types'] = '*';
			$config2['max_size'] = 100000000;
			$config2['overwrite'] = FALSE;
			$config2['remove_spaces'] = TRUE;
			$config2['file_name'] = $video_name2;
			$this->load->library('upload', $config2);
			if ( ! $this->upload->do_upload('userfile2')){
				
			}else{
				$data = array('upload_data' => $this->upload->data());
				$data_update_media2 = array('media2' => 'uploads/videos/'.$video_name2);
				$this->LModel->update_video2($data_update_media2, $lesson_id);
			}

			if ($this->input->post('delete_videomp4')){
				$data_del_media1 = array('media1' => '');
				$this->LModel->update_video1($data_del_media1, $lesson_id);	
			}
			if ($this->input->post('delete_videoogv')){
				$data_del_media2 = array('media2' => '');
				$this->LModel->update_video2($data_del_media2, $lesson_id);
			}
			
			redirect('admin/lesson/lesson_edit_view/'.$lesson_id);
		}else{
			echo "Access Denied";
		}
    }

    ///////////////////////////////////////
    
    public function lesson_single_view(){
    	$lesson_id = $this->uri->segment(4);
		if($this->session->userdata('level')==='1'){
			$get_one_lesson = $this->LModel->get_one_lesson($lesson_id);
			$get_groups_bycenter = $this->LModel->get_groups_bycenter();
			$data['title'] = 'Lecciones';
			$data['lesson'] = $get_one_lesson;
			$data['groups_list'] = $get_groups_bycenter;
			$data['l_id'] = $lesson_id;
			$this->load->view('admin/lesson_single_view', $data);
		}else{
			echo "Access Denied";
		}
    }

    public function add_lesson_group(){
		$group_id = $this->input->post('asignar_grupo');
		$lesson_id = $this->input->post('lesson_id');
        
		if($this->session->userdata('level')==='1'){
			$data_insert = array('group_id' => $group_id, 'lesson_id' => $lesson_id);
            $this->LModel->add_lesson_group($data_insert);

            $sql_students = "SELECT * FROM `tbl_user` LEFT JOIN tbl_group_student ON tbl_user.user_id = tbl_group_student.user_id  WHERE tbl_user.user_level = 4 AND tbl_group_student.group_id = ".$group_id ;
			$query_students = $this->db->query($sql_students);
			if (!empty($query_students->result())){
				foreach ($query_students->result() as $row) {
					$data_insert_std = array('lesson_id' => $lesson_id, 'group_id' => $group_id, 'user_id' => $row->user_id, 'state' => 0 );
					$this->LModel->lesson_group_student($data_insert_std);
				}
			}

            redirect('admin/lesson/lesson_single_view/'.$lesson_id);
        }else{
            echo "Access Denied";
        }
    }
    function re_order(){
	   	$position = $this->input->post('position');
	   	$i = 1;
	   	foreach($position as $k=>$v){
	   		$this->LModel->re_order($i, $v);
	   		$i++;
	   	}
	}

	function DeleteLesson(){
		$lesson_id = $this->uri->segment(4);
		if($this->session->userdata('level')==='1'){
			$this->LModel->DeleteLesson($lesson_id);
			redirect('admin/lesson');
		}
	}

	function DeleteLesson_ad(){
		$lesson_id = $this->uri->segment(4);
		if($this->session->userdata('level')==='1'){
			$this->LModel->DeleteLesson($lesson_id);
			$this->LModel->DeleteLesson_ad($lesson_id);
			redirect('admin/lesson');
		}
	}
}