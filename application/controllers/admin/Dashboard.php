<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
    }
    
    function index(){
        //Allowing akses to admin only
        if($this->session->userdata('level')==='1'){
            $data['title'] = 'Escritorio';
            $this->load->view('admin/dashboard_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    
    function staff(){
        //Allowing akses to staff only
        if($this->session->userdata('level')==='2'){
            $this->load->view('courses/dashboard_view');
        }else{
            echo "Access Denied";
        }
    }
}