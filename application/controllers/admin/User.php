<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->library('email');
        $this->load->model('admin/User_model', 'UModel');
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
    }
    /*
     * CARGAR VISTA DE ADMINISTRADORES
     */
    function index(){
        if($this->session->userdata('level')==='1'){
            $get_user = $this->UModel->get_admins();
            $data['title'] = 'Usuarios - Administradores';
            $data['user_list'] = $get_user;
            $this->load->view('admin/user_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    
    /*
     * CARGAR VISTA DE DIRECTORES
     */
    function manager(){
        if($this->session->userdata('level')==='1'){
            $get_user = $this->UModel->get_managers();
            $data['title'] = 'Usuarios - Directores';
            $data['user_list'] = $get_user;
            $this->load->view('admin/user_manager_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    
    /*
     * CARGAR VISTA DE PROFESORES
     * FUNCIONA PARA TAMBIEN PARA EL FILTRO POR CENTROS
     */
    function professor(){
        if($this->session->userdata('level')==='1'){
        	$filter_p = $this->input->post('filtro_profesor');
        	if (empty($filter_p)){
        		$get_user = $this->UModel->get_professors();
				$get_centers = $this->UModel->get_centers();
				$data['title'] = 'Usuarios - Profesores';
				$data['user_list'] = $get_user;
				$data['centers_list'] = $get_centers;
				$this->load->view('admin/user_professor_view', $data);
        	}else{
				$get_user = $this->UModel->get_professors_by_center($filter_p);
				$get_centers = $this->UModel->get_centers();
				$data['title'] = 'Usuarios - Profesores';
				$data['user_list'] = $get_user;
				$data['centers_list'] = $get_centers;
				$this->load->view('admin/user_professor_view', $data);
			}
        }else{
            echo "Access Denied";
        }
    }
    
    /*
     * CARGAR VISTA DE ESTUDIANTES
     */
    function student(){
        if($this->session->userdata('level')==='1'){
            $get_user = $this->UModel->get_students();
            $data['title'] = 'Usuarios - Estudiantes';
            $data['user_list'] = $get_user;
            $this->load->view('admin/user_student_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    /*
    * FUNCIÓN QUE CREA CUALQUIER TIPO DE USUARIO
    * SE VERIFICA EL CAMPO user_level PARA ASIGNAR EL TIPO DE USUARIO
    * SE CREAR EL USER NICK NAME PARA PARA LOGIN A PARTIR DEL user_name
    */
    function create_user(){
        $this->form_validation->set_rules('user_name', 'user_name', 'trim');
        $this->form_validation->set_rules('user_email', 'user_email', 'trim');
        $this->form_validation->set_rules('user_password', 'user_password', 'trim');
        $this->form_validation->set_rules('user_level', 'user_level', 'trim');
        if ($this->form_validation->run() == FALSE) {
        }else{
            
            $pass_f = md5($this->input->post('user_password'));
            $usernickname = strtolower(str_replace(' ', '.', $this->input->post('user_name')));
            $data_insert = array('user_name' => $this->input->post('user_name'),'user_nickname' => $usernickname, 'user_email' => $this->input->post('user_email'), 'user_password' => $pass_f, 'user_level' => $this->input->post('user_level'),);
            $get_user = $this->UModel->get_admins();            
            $this->UModel->create_user($data_insert);
            $data['message'] = 'Usuario creado correctamente';            
            $data['title'] = 'Usuarios - Administradores';
            $data['user_list'] = $get_user;
            switch ($this->input->post('user_level')) {
                case 1: redirect('admin/user');break;
                case 2: redirect('admin/user/manager');break;
                case 3: redirect('admin/user/professor');break;
                case 4: redirect('admin/user/student');break;
            }
        }
    }
    
    /*
     * VISTA PARA EDITAR CUALQUIER USUARIO
     */
    function user_edit_view(){
        $user_id = $this->uri->segment(4);
        if($this->session->userdata('level')==='1'){
            $get_one_user = $this->UModel->get_user($user_id);
            $user_level = $get_one_user[0]->user_level;
            
            $get_user_edit = $this->UModel->get_user_edit($user_id, $user_level);
            switch ($user_level) {
                case 1: $data['title'] = 'Administrador - Editar'; $data['rol'] = 'Administrador';break;
                case 2: $data['title'] = 'Director - Editar';$data['rol'] = 'Director';break;
                case 3: $data['title'] = 'Profesor - Editar';$data['rol'] = 'Profesor';break;
                case 4: $data['title'] = 'Estudiante - Editar';$data['rol'] = 'Estudiante';break;
            }            
            $data['users'] = $get_user_edit;
            $get_professor_center = $this->UModel->get_professor_center($user_id);
                $data['centers_professors'] = $get_professor_center;
            $get_centers = $this->UModel->get_centers();
                $data['centers'] = $get_centers;

            $data['u_id'] = $user_id ;
            $this->load->view('admin/user_edit_view', $data);
        }else{
            echo "Access Denied";
        }
    }
    /*
     * FUNCIÓN PARA EDITAR CUALQUIER USUARIO
     */
    public function user_edit(){
        $this->form_validation->set_rules('user_name', 'user_name', 'trim');
        $this->form_validation->set_rules('user_nickname', 'user_nickname', 'trim');
        $this->form_validation->set_rules('user_email', 'user_email', 'trim');
        $this->form_validation->set_rules('user_password', 'user_password', 'trim');
        
        $user_id = $this->input->post('user_actual_id');
        if($this->session->userdata('level')==='1'){
            if (!empty($this->input->post('user_password'))){
                $u_pass = md5($this->input->post('user_password'));
                $data_update = array('user_name' => $this->input->post('user_name'),'user_nickname' => $this->input->post('user_nickname'), 'user_email' => $this->input->post('user_email'), 'user_password' => $u_pass);
            }else{
                $data_update = array('user_name' => $this->input->post('user_name'),'user_nickname' => $this->input->post('user_nickname'), 'user_email' => $this->input->post('user_email'));
            }
            $this->UModel->update_user($user_id, $data_update);
            redirect('admin/user/user_edit_view/'.$user_id);
        }else{
            echo "Access Denied";
        }
    }
    /*
     * FUNCIÓN PARA EDITAR EL ROL CUALQUIER USUARIO
     */
    public function user_rol_edit(){
        $this->form_validation->set_rules('user_level_new', 'user_level_new', 'trim');
        
        $user_id = $this->input->post('user_actual_id');
        if($this->session->userdata('level')==='1'){
            $data_update = array('user_level' => $this->input->post('user_level_new'));
            
            $this->UModel->update_user($user_id, $data_update);
            redirect('admin/user/user_edit_view/'.$user_id);
        }else{
            echo "Access Denied";
        }
    }
    
    /*
    * PARA AGREGAR CENTROS Y GRUPOS A UN ESTUDIANTE
    */
    public function add_center_student(){
        $this->form_validation->set_rules('asignar_centro', 'asignar_centro', 'trim');
        
        $user_id = $this->input->post('user_actual_id');
        if($this->session->userdata('level')==='1'){
            $data_insert = array('user_id' => $user_id,'center_id' => $this->input->post('asignar_centro'));
            $this->UModel->add_center_student($data_insert);
            redirect('admin/user/user_edit_view/'.$user_id);
        }else{
            echo "Access Denied";
        }
    }
    /*
     * FUNCIÓN PARA AGREGAR UN ESTUDIANTE A UN GRUPO
     */
    public function add_group_student(){
        $this->form_validation->set_rules('asignar_grupo', 'asignar_grupo', 'trim');
        
        $user_id = $this->input->post('user_actual_id');
        $center_id = $this->input->post('user_center_id');
        if($this->session->userdata('level')==='1'){
            $data_insert = array('user_id' => $user_id,'center_id' => $center_id, 'group_id' => $this->input->post('asignar_grupo'));
            $this->UModel->add_group_student($data_insert);
            redirect('admin/user/user_edit_view/'.$user_id);
        }else{
            echo "Access Denied";
        }
    }
    /*
    * PARA AGREGAR CENTROS Y GRUPOS A UN ESTUDIANTE
    */
    
    /*
    * PARA AGREGAR CENTROS Y GRUPOS A UN PROFESORES
    */
    public function add_center_professor(){
        $this->form_validation->set_rules('asignar_centro', 'asignar_centro', 'trim');
        
        $user_id = $this->input->post('user_actual_id');
        if($this->session->userdata('level')==='1'){
            $data_insert_center = array('user_id' => $user_id,'center_id' => $this->input->post('asignar_centro'));
            $this->UModel->add_center_professor($data_insert_center);
            redirect('admin/user/user_edit_view/'.$user_id);
        }else{
            echo "Access Denied";
        }
    }
    public function add_group_professor(){
        $this->form_validation->set_rules('asignar_grupo', 'asignar_grupo', 'trim');
        
        $user_id = $this->input->post('user_actual_id');
        $center_id = $this->input->post('user_center_id');
        if($this->session->userdata('level')==='1'){
            $data_insert = array('user_id' => $user_id,'center_id' => $center_id, 'group_id' => $this->input->post('asignar_grupo'));
            $this->UModel->add_group_professor($data_insert);
            redirect('admin/user/user_edit_view/'.$user_id);
        }else{
            echo "Access Denied";
        }
    }
}
