-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 20-03-2019 a las 06:18:36
-- Versión del servidor: 8.0.15
-- Versión de PHP: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `klikatuz`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_center`
--

CREATE TABLE `tbl_center` (
  `center_id` int(11) NOT NULL,
  `center_name` varchar(30) NOT NULL,
  `manager_id` int(11) NOT NULL COMMENT 'Viene desde la tabla usuario y se obtiene con la sesión'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `tbl_center`
--

INSERT INTO `tbl_center` (`center_id`, `center_name`, `manager_id`) VALUES
(1, 'La Salle', 7),
(2, 'Centro 2', 8),
(3, 'Centro 3', 6),
(4, 'Centro 4', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_center_professor`
--

CREATE TABLE `tbl_center_professor` (
  `user_id` int(11) NOT NULL,
  `center_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_center_professor`
--

INSERT INTO `tbl_center_professor` (`user_id`, `center_id`) VALUES
(10, 1),
(11, 1),
(10, 2),
(10, 3),
(12, 1),
(13, 1),
(12, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_center_student`
--

CREATE TABLE `tbl_center_student` (
  `user_id` int(11) NOT NULL,
  `center_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_center_student`
--

INSERT INTO `tbl_center_student` (`user_id`, `center_id`) VALUES
(15, 1),
(16, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_dictation`
--

CREATE TABLE `tbl_dictation` (
  `dictation_id` int(11) NOT NULL,
  `dictation_name` text COLLATE utf8_spanish_ci NOT NULL,
  `lesson_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_dictation`
--

INSERT INTO `tbl_dictation` (`dictation_id`, `dictation_name`, `lesson_id`) VALUES
(1, 'Dictado 1', 2),
(2, 'Dictado 2', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_dictation_sub`
--

CREATE TABLE `tbl_dictation_sub` (
  `dictation_sub_id` int(11) NOT NULL,
  `dictation_sub_name` text COLLATE utf8_spanish_ci NOT NULL,
  `dictation_sub_content` text COLLATE utf8_spanish_ci NOT NULL,
  `dictation_sub_time` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `dictation_sub_max_time` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `dictation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_dictation_sub`
--

INSERT INTO `tbl_dictation_sub` (`dictation_sub_id`, `dictation_sub_name`, `dictation_sub_content`, `dictation_sub_time`, `dictation_sub_max_time`, `dictation_id`) VALUES
(1, 'Ejercicio 2', 'zzzzz', '22', '30', 1),
(2, 'Ejercicio 2', 'zzzzz', '22', '30', 1),
(3, 'Ejercicio 1 - D2', 'fffffffffffffffffffffffffffffffffffffffffffffffff', '20', '20', 2),
(4, 'Ejercicio 2 - D2', 'jj', '22', '22', 2),
(5, 'Ejercicio 3 - D2', 'zzz', '21', '21', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_excercise`
--

CREATE TABLE `tbl_excercise` (
  `excercise_id` int(11) NOT NULL,
  `excercise_name` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `lesson_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_excercise`
--

INSERT INTO `tbl_excercise` (`excercise_id`, `excercise_name`, `lesson_id`) VALUES
(1, 'Ejercicio 1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_excercise_sub`
--

CREATE TABLE `tbl_excercise_sub` (
  `excercise_sub_id` int(11) NOT NULL,
  `excercise_sub_name` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `excercise_sub_content` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `excercise_sub_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `excercise_sub_max_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `excercise_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_excercise_sub`
--

INSERT INTO `tbl_excercise_sub` (`excercise_sub_id`, `excercise_sub_name`, `excercise_sub_content`, `excercise_sub_time`, `excercise_sub_max_time`, `excercise_id`) VALUES
(1, 'Blque 1', 'jjjj', '10', '15', 1),
(2, 'Blque 2', 'ffff', '10', '15', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_group`
--

CREATE TABLE `tbl_group` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(30) NOT NULL,
  `center_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `tbl_group`
--

INSERT INTO `tbl_group` (`group_id`, `group_name`, `center_id`) VALUES
(1, 'Euskara Bikoizteko', 1),
(2, 'Aurreratuak', 1),
(5, 'Avanzados Para Duplicar', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_group_professor`
--

CREATE TABLE `tbl_group_professor` (
  `user_id` int(11) NOT NULL,
  `center_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_group_professor`
--

INSERT INTO `tbl_group_professor` (`user_id`, `center_id`, `group_id`) VALUES
(10, 1, 1),
(10, 1, 2),
(11, 1, 1),
(12, 1, 1),
(12, 1, 2),
(12, 1, 5),
(13, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_group_student`
--

CREATE TABLE `tbl_group_student` (
  `user_id` int(11) NOT NULL,
  `center_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_group_student`
--

INSERT INTO `tbl_group_student` (`user_id`, `center_id`, `group_id`) VALUES
(15, 1, 1),
(15, 1, 2),
(16, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson`
--

CREATE TABLE `tbl_lesson` (
  `lesson_id` int(11) NOT NULL,
  `lesson_name` varchar(40) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_lesson`
--

INSERT INTO `tbl_lesson` (`lesson_id`, `lesson_name`) VALUES
(1, 'Leccion 1'),
(2, 'asdfasdf'),
(3, 'Leccion 2'),
(4, 'Leccion 3'),
(5, 'sssss'),
(6, 'sdfsdfsd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_detail`
--

CREATE TABLE `tbl_lesson_detail` (
  `lesson_detail_id` int(11) NOT NULL,
  `lesson_detail_content` text COLLATE utf8_spanish_ci NOT NULL,
  `lesson_time` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `lesson_max_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `lesson_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_lesson_detail`
--

INSERT INTO `tbl_lesson_detail` (`lesson_detail_id`, `lesson_detail_content`, `lesson_time`, `lesson_max_time`, `lesson_id`) VALUES
(1, 'rwe wqe rqwer qwer qwer qwerq wer qwerqwer', '30', '', 1),
(2, 'asdf asdf sadf asdf saf sda', '20', '', 2),
(3, '', '10', '', 3),
(4, '', '3', '', 4),
(5, '', '', '', 5),
(6, 'sdfasdf asdfsadfsadfasdf asd asdf', '24', '22', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_dictation_student`
--

CREATE TABLE `tbl_lesson_dictation_student` (
  `lesson_dictation_student_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_exercise_student`
--

CREATE TABLE `tbl_lesson_exercise_student` (
  `lesson_dictation_student_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_group`
--

CREATE TABLE `tbl_lesson_group` (
  `lesson_group_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_lesson_group`
--

INSERT INTO `tbl_lesson_group` (`lesson_group_id`, `group_id`, `lesson_id`) VALUES
(1, 1, 1),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_group_student`
--

CREATE TABLE `tbl_lesson_group_student` (
  `lesson_group_student_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_lesson_group_student`
--

INSERT INTO `tbl_lesson_group_student` (`lesson_group_student_id`, `lesson_id`, `group_id`, `user_id`, `state`) VALUES
(1, 1, 1, 15, 0),
(2, 1, 1, 16, 0),
(3, 1, 2, 15, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_lesson_media`
--

CREATE TABLE `tbl_lesson_media` (
  `lesson_id` int(11) NOT NULL,
  `media1` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `media2` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_lesson_media`
--

INSERT INTO `tbl_lesson_media` (`lesson_id`, `media1`, `media2`) VALUES
(6, 'uploads/videos/2009-12-2903_01_21', 'uploads/videos/2009-12-2903_01_21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_student`
--

CREATE TABLE `tbl_student` (
  `user_id` int(11) NOT NULL,
  `center_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_student`
--

INSERT INTO `tbl_student` (`user_id`, `center_id`) VALUES
(15, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_nickname` varchar(20) DEFAULT NULL,
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_email` varchar(60) DEFAULT NULL,
  `user_password` varchar(40) DEFAULT NULL,
  `user_level` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_nickname`, `user_name`, `user_email`, `user_password`, `user_level`) VALUES
(1, 'admin', 'Julio Iturre', 'julio.iturre@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1'),
(2, 'willans.flores', 'Willans Flores', 'willanfloresc@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1'),
(6, 'juan.perez', 'Juan Perez', 'dir@dir1.com', 'e10adc3949ba59abbe56e057f20f883e', '2'),
(7, 'rodrigo.suarez', 'Rodrigo Suarez', 'dir@dir2.com', '74c43b7ec689955c9c1517294e92500f', '2'),
(8, 'julio.ausin', 'Julio Ausin', 'dir@dir3.com', '14fa3d8304d25e374700550ff0d945b2', '2'),
(9, 'carlos.iturre', 'Carlos Iturre', 'dir@dir4.com', 'e10adc3949ba59abbe56e057f20f883e', '2'),
(10, 'alvaro.fernandez', 'Alvaro Fernandez', 'alvaroernandez@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '3'),
(11, 'erik.aznal', 'Erik Aznal', 'erikznal@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '3'),
(12, 'ibon.rodriguez', 'Ibon Rodriguez', 'ibonrodriguez@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '3'),
(13, 'iradi.galarza', 'Irati Galarza', 'iratigalarza@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '3'),
(14, 'irene.fernandez', 'Irene Fernandez', 'irenefernandez@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '3'),
(15, 'aaron.rodrigo.larrea', 'Aaron Rodrigo Larrea', 'aaron.rodrigo@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '4'),
(16, 'adhara.corretge', 'Adhara Corretge', 'adhara.corretge@klikatuz.com', 'e10adc3949ba59abbe56e057f20f883e', '4'),
(18, 'carlos.martinez', 'Carlos Martinez', 'carlos_martinez@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_center`
--
ALTER TABLE `tbl_center`
  ADD PRIMARY KEY (`center_id`);

--
-- Indices de la tabla `tbl_dictation`
--
ALTER TABLE `tbl_dictation`
  ADD PRIMARY KEY (`dictation_id`);

--
-- Indices de la tabla `tbl_dictation_sub`
--
ALTER TABLE `tbl_dictation_sub`
  ADD PRIMARY KEY (`dictation_sub_id`);

--
-- Indices de la tabla `tbl_excercise`
--
ALTER TABLE `tbl_excercise`
  ADD PRIMARY KEY (`excercise_id`);

--
-- Indices de la tabla `tbl_excercise_sub`
--
ALTER TABLE `tbl_excercise_sub`
  ADD PRIMARY KEY (`excercise_sub_id`);

--
-- Indices de la tabla `tbl_group`
--
ALTER TABLE `tbl_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indices de la tabla `tbl_lesson`
--
ALTER TABLE `tbl_lesson`
  ADD PRIMARY KEY (`lesson_id`);

--
-- Indices de la tabla `tbl_lesson_detail`
--
ALTER TABLE `tbl_lesson_detail`
  ADD PRIMARY KEY (`lesson_detail_id`);

--
-- Indices de la tabla `tbl_lesson_dictation_student`
--
ALTER TABLE `tbl_lesson_dictation_student`
  ADD PRIMARY KEY (`lesson_dictation_student_id`);

--
-- Indices de la tabla `tbl_lesson_exercise_student`
--
ALTER TABLE `tbl_lesson_exercise_student`
  ADD PRIMARY KEY (`lesson_dictation_student_id`);

--
-- Indices de la tabla `tbl_lesson_group`
--
ALTER TABLE `tbl_lesson_group`
  ADD PRIMARY KEY (`lesson_group_id`);

--
-- Indices de la tabla `tbl_lesson_group_student`
--
ALTER TABLE `tbl_lesson_group_student`
  ADD PRIMARY KEY (`lesson_group_student_id`);

--
-- Indices de la tabla `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_center`
--
ALTER TABLE `tbl_center`
  MODIFY `center_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_dictation`
--
ALTER TABLE `tbl_dictation`
  MODIFY `dictation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_dictation_sub`
--
ALTER TABLE `tbl_dictation_sub`
  MODIFY `dictation_sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tbl_excercise`
--
ALTER TABLE `tbl_excercise`
  MODIFY `excercise_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_excercise_sub`
--
ALTER TABLE `tbl_excercise_sub`
  MODIFY `excercise_sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_group`
--
ALTER TABLE `tbl_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tbl_lesson`
--
ALTER TABLE `tbl_lesson`
  MODIFY `lesson_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tbl_lesson_detail`
--
ALTER TABLE `tbl_lesson_detail`
  MODIFY `lesson_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tbl_lesson_dictation_student`
--
ALTER TABLE `tbl_lesson_dictation_student`
  MODIFY `lesson_dictation_student_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_lesson_exercise_student`
--
ALTER TABLE `tbl_lesson_exercise_student`
  MODIFY `lesson_dictation_student_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_lesson_group`
--
ALTER TABLE `tbl_lesson_group`
  MODIFY `lesson_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_lesson_group_student`
--
ALTER TABLE `tbl_lesson_group_student`
  MODIFY `lesson_group_student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
